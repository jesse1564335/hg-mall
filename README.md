# hg-mall

#### 介绍
基于Hyperf 高性能、高灵活性的协程框架的商城项目。

#### 项目分支说明
* master 分支目前代码未同步
* dev-normal默认分支


#### 软件架构
采用consul/nacos为服务中心，多个微服务通过不同的jsonrpc端口号把不同的service（服务）注册到服务中心，
这样可以使所有有权限可以连接到服务中心的服务器都可以调到这些微服务。

![架构](微服务架构.png)


#### 安装教程

1.  docker pull nacos/nacos-server
2.  docker  run --name nacos -d -p 8849:8848 --privileged=true --restart=always -e JVM_XMS=256m -e JVM_XMX=256m -e MODE=standalone -e PREFER_HOST_MODE=hostname nacos/nacos-server:latest

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
