import {export_json_to_excel} from "../Export2Excel";

export function format(time, format, timeDate=null) {
  var t = time;
  if(!timeDate) {
    t = new Date(time)
  }
  var tf = function (i) { return (i < 10 ? '0' : '') + i };
  return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
    switch (a) {
      case 'yyyy':
        return tf(t.getFullYear());
        break;
      case 'MM':
        return tf(t.getMonth() + 1);
        break;
      case 'mm':
        return tf(t.getMinutes());
        break;
      case 'dd':
        return tf(t.getDate());
        break;
      case 'HH':
        return tf(t.getHours());
        break;
      case 'ss':
        return tf(t.getSeconds());
        break;
    }
  })
}

/**
 * 计算出相差天数
 * @param secondSub
 */
export function formatTotalDateSub (secondSub) {
  var days = Math.floor(secondSub / (24 * 3600));     // 计算出小时数
  var leave1 = secondSub % (24*3600) ;                // 计算天数后剩余的毫秒数
  var hours = Math.floor(leave1 / 3600);              // 计算相差分钟数
  var leave2 = leave1 % (3600);                       // 计算小时数后剩余的毫秒数
  var minutes = Math.floor(leave2 / 60);              // 计算相差秒数
  var leave3 = leave2 % 60;                           // 计算分钟数后剩余的毫秒数
  var seconds = Math.round(leave3);
  return days + "天" + hours + "时" + minutes + "分" + seconds + '秒';
}


/**
 * @Author: 何志祥
 * @Date: 2021-11-15 14:55:53
 * @Description: 获取当前时间前后N天日期
 */
 export const getDateStr = (AddDayCount, type='begin') => {
  let dd = new Date();
  dd.setDate(dd.getDate() + AddDayCount); //获取AddDayCount天后的日期
  let y = dd.getFullYear();
  let m =
      dd.getMonth() + 1 < 10 ? '0' + (dd.getMonth() + 1) : dd.getMonth() + 1; //获取当前月份的日期，不足10补0
  let d = dd.getDate() < 10 ? '0' + dd.getDate() : dd.getDate(); //获取当前几号，不足10补0
  if(type === 'begin') {
    return y + '/' + m + '/' + d + ' 00:00:00';
  }else {
    return y + '/' + m + '/' + d + ' 23:59:59';
  }
};
/**
 * @Author: 何志祥
 * @Date: 2021-11-20 15:14:54
 * @Description: 导出 excel 表格(前端实现)
 * @param {*} tHeader 表头 - 数组(比如: ['名字', '年龄'])
 * @param {*} filterVal 表头对应取值的字段 - 数组(比如: ['name', 'age'])
 * @param {*} filename  导出文件名
 * @param {*} data  数据
 * @param {*} isAutoWidth 是否自适应宽度
 */
export const exportExcel = (tHeader, filterVal, data, filename, isAutoWidth = true) => {
  require.ensure([], () => {
    const { export_json_to_excel } = require('../Export2Excel');
    const tempData = data.map(v => filterVal.map(j => v[j]));
    export_json_to_excel(tHeader, tempData, filename, isAutoWidth);
  });
}
/**
 * @Author: 毛诚杰
 * @Date: 2021-1-19 15:14:54
 * @Description: 控制表格高度
 * @param {*} that 页面对象this
 * @param {*} heightName 表格高度值key
 * @param {*} element  需要控制高度元素以上的元素
 */
export const setTableHeight = (that, heightName, element) => {
  that.$nextTick(() => {
    // 通过set方法复制，直接复制可能出现无法触发现象
    that.$set(that, heightName, document.body.clientHeight < 740 ? '425px':'calc(100% - ' + (element.clientHeight + 57) + 'px)')
  })
  // window.onresize = () => {
  //   return (() => {
  //     console.log(element.clientHeight)
  //     // 通过set方法复制，直接复制可能出现无法触发现象
  //     that.$set(that, heightName, document.body.clientHeight < 740 ? '425px':'calc(100% - ' + (element.clientHeight + 57) + 'px)')
  //   })();
  // };
}
