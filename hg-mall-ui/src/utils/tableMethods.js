/*
 * @Author: 何志祥
 * @Date: 2021-12-09 14:53:07
 * @LastEditors: 何志祥
 * @LastEditTime: 2022-02-24 21:13:28
 * @Description: 表格有关公共方法封装
 */

/**
* @Author: 何志祥
* @Date: 2021-12-09 14:33:52
* @Description: 表格数据为空显示 --
* @param {*} row
* @param {*} column
* @param {*} cellValue 当前值
*/
export const emptyJudge = (row, column, cellValue) => {
   if (!cellValue) {
       return '--';
   }
   return cellValue;
}

 
