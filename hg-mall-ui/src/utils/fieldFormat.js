/*
 * @Author: 何志祥
 * @Date: 2021-11-03 19:35:27
 * @LastEditors: 何志祥
 * @LastEditTime: 2022-04-20 13:55:28
 * @Description: 各字段格式化
 */

/**
 * @Author: 毛诚杰
 * @Date: 2021-03-02 19:46:48
 * @Description: 通用是否过滤
 * type='filter' 代表是筛选
 */
export const isOrNo = (value, type = 'filter') => {
  if (value && value != 0) {
    return '是';
  } else {
    return '否';
  }
}

/**
 * @Author: 毛诚杰
 * @Date: 2021-03-02 19:46:48
 * @Description: 通用是否过滤,三种状态
 * type='filter' 代表是筛选
 */
 export const isNullOrNo = (value, type = 'filter') => {
  if (value === false) {
    return '否';
  } else if(value === null) {
    return '--'
  } else {
    return '是';
  }
}

/**
 * @Author: 何志祥
 * @Date: 2022-04-01 11:22:11
 * @Description: 金额保留两位小数
 */
export const moneyTwoPlaces = (value) => {
  return (value / 100).toFixed(2)
}

/**
 * @Author: 毛诚杰
 * @Date: 2021-03-02 19:46:48
 * @Description: 通用金额转化，分转元保留两位小数
 * type='filter' 代表是筛选
 */
 export const moneyCount = (value, type = 'filter') => {
  if (value) {
    return (parseFloat(value) / 100).toFixed(2);
  } else {
    return '--';
  }
}


/**
 * @Author: 何志祥
 * @Date: 2021-11-03 19:46:48
 * @Description: 订单状态
 * type='filter' 代表是筛选
 * 1-待付款, 2-已完成，3-已失效, 4-审核不通过, 5-审核中, 6-退款申请中, 7-退款中, 8-已退款, 9-已到期，10-待签约 11-待签约 12-已升级 13-未知支付结果 14-大额支付支付中
 */
export const orderStatusFormat = (value, type = 'filter') => {
  const obj = [
    {
      key: 1,
      value: '待付款'
    },
    {
      key: 2,
      value: '已完成'
    },
    {
      key: 3,
      value: '已失效'
    },
    {
      key: 4,
      value: '审核不通过'
    },
    {
      key: 5,
      value: '审核中'
    },
    {
      key: 6,
      value: '退款申请中'
    },
    {
      key: 7,
      value: '退款中'
    },
    {
      key: 8,
      value: '已退款'
    },
    {
      key: 9,
      value: '已到期'
    },
    {
      key: 10,
      value: '线下待签约'
    },
    {
      key: 11,
      value: '待签约 '
    },
    {
      key: 12,
      value: '已升级'
    },
    {
      key: 13,
      value: '未知支付结果'
    },
    {
      key: 14,
      value: '大额支付中'
    }
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 何志祥
 * @Date: 2021-11-03 19:46:48
 * @Description: 支付方式
 *  type='filter' 代表是筛选
 * 0-微信支付 WECHATPAY(0), // 1-支付宝支付 ALIPAY(1), // 9-积分支付 BPPAY(9), // 7-IAPPAY IAPPAY(7), // 5-线下支付 OFFLINE(5); // 8-赠送FREEPAY(8) // 6- 分享SHAREPAY(6) // 2-银联支付UNIONPAY(2) // 3-线下赠送OFFLINE_GIFT(3) // 4-签到SIGN(4) // 10-快钱BILLPAY(10) // 11-大额支付LARGEAMOUNT(11)
 */
export const payChannelFormat = (value, type = 'filter') => {
  const obj = [{
      key: 0,
      value: '微信支付'
    },
    {
      key: 1,
      value: '支付宝支付'
    },
    {
      key: 2,
      value: '银联支付'
    },
    {
      key: 3,
      value: '线下赠送'
    },
    {
      key: 4,
      value: '签到'
    },
    {
      key: 5,
      value: '线下支付'
    },
    {
      key: 6,
      value: '分享'
    },
    {
      key: 7,
      value: 'IAPPAY'
    },
    {
      key: 8,
      value: '赠送'
    },
    {
      key: 9,
      value: '积分支付'
    },
    {
      key: 10,
      value: '快钱'
    },
    {
      key: 11,
      value: '大额支付'
    },
    {
      key: 12,
      value: '抖音支付'
    }
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 何志祥
 * @Date: 2021-11-03 19:46:48
 * @Description: 产品版本
 * type='filter' 代表是筛选
 * 0-终身 1-周版 2-月版 3-季版 4-半年版 5-年版 6：两年版 7-其他 9-天版 10-三年版 11-四年版 12-五年版
 */
export const priceConfigFormat = (value, type = 'filter') => {
  const obj = [{
      key: 0,
      value: '终身'
    },
    {
      key: 1,
      value: '周版'
    },
    {
      key: 2,
      value: '月版'
    },
    {
      key: 3,
      value: '季版'
    },
    {
      key: 4,
      value: '半年版'
    },
    {
      key: 5,
      value: '年版'
    },
    {
      key: 6,
      value: '两年版'
    },
    {
      key: 7,
      value: '其他'
    },
    {
      key: 9,
      value: '天版'
    },
    {
      key: 10,
      value: '三年版'
    },
    {
      key: 11,
      value: '四年版'
    },
    {
      key: 12,
      value: '五年版'
    }
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}


/**
 * @Author: 何志祥
 * @Date: 2021-11-03 19:46:48
 * @Description: 是否大额支付
 * type='filter' 代表是筛选
 * 0-否 1-是
 */
export const isBigpayFormat = (value, type = 'filter') => {
  const obj = [{
    key: 0,
    value: '否'
  }, {
    key: 1,
    value: '是'
  }]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 何志祥
 * @Date: 2021-11-03 19:46:48
 * @Description: 是否最后大额支付
 * type='filter' 代表是筛选
 * 0-否 1-是
 */
export const isLastpayFormat = (value, type = 'filter') => {
  const obj = [{
    key: 0,
    value: '否'
  }, {
    key: 1,
    value: '是'
  }]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 何志祥
 * @Date: 2021-11-03 19:46:48
 * @Description: 订单来源
 * type='filter' 代表是筛选
 * 1：股掌柜（爱智投）
 */
export const orderApplicationFormat = (value, type = 'filter') => {

  const obj = [{
    key: 1,
    value: '股掌柜（爱智投）'
  }]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}


/**
 * @Author: 何志祥
 * @Date: 2021-11-03 19:46:48
 * @Description: 订单类型
 * type='filter' 代表是筛选
 * 0:正常单 1：续费单
 */
export const renewOrderFormat = (value, type = 'filter') => {
  const obj = [{
    key: 0,
    value: '正常单'
  }, {
    key: 1,
    value: '续费单'
  }]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}


/**
 * @Author: 何志祥
 * @Date: 2021-11-03 19:46:48
 * @Description: 客户风险等级
 * type='filter' 代表是筛选
 */
 export const userRiskTypeFormat = (value, type = 'filter') => {
  const obj = [{
    key: 1,
    value: '保守型'
  }, {
    key: 2,
    value: '谨慎型'
  }, {
    key: 3,
    value: '稳健型'
  }, {
    key: 4,
    value: '积极型'
  }, {
    key: 5,
    value: '激进型'
  }]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}


/**
 * @Author: 何志祥
 * @Date: 2021-11-03 19:46:48
 * @Description: 产品风险等级
 * type='filter' 代表是筛选
 */
 export const productLevelFormat = (value, type = 'filter') => {
  const obj = [{
    key: 'R1',
    value: '低风险'
  }, {
    key: 'R2',
    value: '中低风险'
  }, {
    key: 'R3',
    value: '中风险'
  }, {
    key: 'R4',
    value: '中高风险'
  }, {
    key: 'R5',
    value: '高风险'
  }]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 毛诚杰
 * @Date: 2021-11-03 19:46:48
 * @Description: 订单审核状态
 * type='filter' 代表是筛选
 */
export const auditStatusFormat = (value, type = 'filter') => {
  const obj = [
    { key: 0, value: '待申请'},
    { key: 1, value: '待审核'},
    { key: 2, value: '审核不通过'},
    { key: 3, value: '审核通过'}
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 毛诚杰
 * @Date: 2021-12-27 19:46:48
 * @Description: 订单状态
 * type='filter' 代表是筛选
 * 1-待付款, 2-已完成，3-已失效, 4-审核不通过, 5-审核中, 6-退款申请中, 7-退款中, 8-已关闭, 9-已到期，10-待签约 11-支付完成 12-已升级 13-未知支付结果 14-付款中
 */
export const applyOrderStatusFormat = (value, type = 'filter') => {
  const obj = [
    {
      key: 2,
      value: '已完成'
    },
    {
      key: 3,
      value: '已失效'
    },
    {
      key: 4,
      value: '审核不通过'
    },
    {
      key: 5,
      value: '审核中'
    },
    {
      key: 6,
      value: '退款申请中'
    },
    {
      key: 7,
      value: '退款中'
    },
    {
      key: 8,
      value: '已退款'
    },
    {
      key: 9,
      value: '已关闭'
    },
    {
      key: 10,
      value: '线下待签约'
    },
    {
      key: 11,
      value: '支付完成'
    },
    {
      key: 12,
      value: '已升级'
    },
    {
      key: 13,
      value: '未知支付结果'
    },
    {
      key: 14,
      value: '付款中'
    }
  ];

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj;
  }
};

/**
 * @Author: 毛诚杰
 * @Date: 2021-1-05 19:46:48
 * @Description: 敏感词记录类型
 * type='filter' 代表是筛选
 */
export const sensitiveWordsTypeFormat = (value, type = 'filter') => {
  const obj = [
    { key: 1, value: '客户' },
    { key: 2, value: '营销'}
  ]
  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 毛诚杰
 * @Date: 2021-1-05 19:46:48
 * @Description: 敏感词记录是否成交
 * type='filter' 代表是筛选
 */
export const sensitiveWordsIsDealFormat = (row, type = 'filter') => {
  if (row.isAggregation) {
    return '-';
  } else if (row.isDeal) {
    return '是';
  } else {
    return '否';
  }
}

/**
 * @Author: 毛诚杰
 * @Date: 2021-1-05 19:46:48
 * @Description: 敏感词记录状态
 * type='filter' 代表是筛选
 */
export const sensitiveWordsStatusFormat = (value, type = 'filter') => {
  const obj = [
    { key: 1, value: '未处理' },
    { key: 2, value: '已处理-无违规'},
    { key: 3, value: '已处理-有违规'}
  ]
  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 何志祥
 * @Date: 2022-04-01 10:46:48
 * @Description: 优惠券使用状态
 * type='filter' 代表是筛选
 */
 export const discountUseStatus = (value, type = 'filter') => {
  const obj = [
    { key: 1, value: '未使用' },
    { key: 2, value: '已使用' },
    { key: 3, value: '已过期' }
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 何志祥
 * @Date: 2022-04-01 10:50:48
 * @Description: 优惠券使用状态
 * type='filter' 代表是筛选
 */
 export const couponType = (value, type = 'filter') => {
  const obj = [
    { key: 1, value: '体验券' },
    { key: 2, value: '满减券' }
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 何志祥
 * @Date: 2022-03-11 14:39:30
 * @Description: 风险单状态
 * @param {*} value
 * @param {*} type
 * type='filter' 代表是筛选
 */
export const riskStatusFormat = (value, type = 'filter') => {
  const obj = [
    { key: 1, value: '待分配'},
    { key: 2, value: '待处理'},
    { key: 3, value: '遗留中'},
    { key: 4, value: '处理中'},
    { key: 5, value: '已退回'},
    { key: 6, value: '待审核'},
    { key: 7, value: '审核不通过'},
    { key: 8, value: '已完成'},
    { key: 9, value: '已关闭'}
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 何志祥
 * @Date: 2022-04-01 10:50:48
 * @Description: 优惠券发券渠道
 * type='filter' 代表是筛选
 */
export const couponChannel = (value, type = 'filter') => {
  if (value.split(',').length > 1) {
    const type = {
      '1': '活动渠道',
      '2': '兑换码渠道',
      '3': '线下渠道'
    }
    const val = [];
    for (const item of value.split(',')) {
      val.push(type[item.toString()])
    }
    return val.join(',');
  }
  const obj = [
    { key: 1, value: '活动渠道' },
    { key: 3, value: '线下渠道' },
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 何志祥
 * @Date: 2022-04-01 10:50:48
 * @Description: 优惠券状态
 * type='filter' 代表是筛选
 */
export const couponStatus = (value, type = 'filter') => {
  const obj = [
    { key: 1, value: '待审核' },
    { key: 2, value: '待上架' },
    { key: 3, value: '退回' },
    { key: 4, value: '已上架' },
    { key: 5, value: '已下架' },
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 何志祥
 * @Date: 2022-03-11 14:39:30
 * @Description: 订单类型
 * @param {*} value
 * @param {*} type
 * type='filter' 代表是筛选
 */
 export const orderTypeFormat = (value, type = 'filter') => {
  const obj = [
    { key: 1, value: '付费'}
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}
/**
 * @Author: 何志祥
 * @Date: 2022-04-01 10:50:48
 * @Description: 发券状态
 * type='filter' 代表是筛选
 */
export const sendCouponStatus = (value, type = 'filter') => {
  const obj = [
    { key: 1, value: '待审核' },
    { key: 2, value: '审核通过' },
    { key: 3, value: '退回' }
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 何志祥
 * @Date: 2022-04-01 10:50:48
 * @Description: 发券用户类型
 * type='filter' 代表是筛选
 */
export const couponUserType = (value, type = 'filter') => {
  const obj = [
    { key: 1, value: '付费用户' },
    { key: 2, value: '特定名单' },
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 何志祥
 * @Date: 2022-03-11 14:39:30
 * @Description: 外诉单状态
 * @param {*} value
 * @param {*} type
 * type='filter' 代表是筛选
 */
 export const isRevocationFormat = (value, type = 'filter') => {
  const obj = [
    { key: true, value: '已撤销'},
    { key: false, value: '未撤销'}
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 何志祥
 * @Date: 2022-04-01 10:50:48
 * @Description: 时间戳转时间
 * type='filter' 代表是筛选
 */
import moment from "moment";
export const timestapeTransition = (timestamp) => {
  return timestamp ? moment(timestamp).format("YYYY-MM-DD HH:mm:ss") : '--'
}

/**
 * @Author: 何志祥
 * @Date: 2022-04-01 10:50:48
 * @Description: 临时用户配置 - 类型
 * type='filter' 代表是筛选
 */
 export const tempType = (value, type = 'filter') => {
  const obj = [
    { key: 1, value: '用户ID' },
    { key: 2, value: '设备CID' }
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 何志祥
 * @Date: 2022-04-01 10:50:48
 * @Description: 临时用户配置 - 状态
 * type='filter' 代表是筛选
 */
 export const tempStatus = (value, type = 'filter') => {
  const obj = [
    { key: 1, value: '已上线' },
    { key: 0, value: '待上线' },
    { key: -1, value: '已下线' }
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}


/**
 * @Author: 何志祥
 * @Date: 2022-04-01 10:50:48
 * @Description: 领券中心 - 状态
 * type='filter' 代表是筛选
 */
 export const discountStatusFormat = (value, type = 'filter') => {
  const obj = [
    { key: 1, value: '待审核' },
    { key: 2, value: '待上架' },
    { key: 3, value: '退回' },
    { key: 4, value: '已上架' },
    { key: 5, value: '已下架' }
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}


/**
 * @Author: 何志祥
 * @Date: 2022-04-01 10:50:48
 * @Description: 领券中心 - 用户类型
 * type='filter' 代表是筛选
 */
 export const discountuserTypeFormat = (value, type = 'filter') => {
  const obj = [
    { key: 2, value: '付费用户' },
    { key: 1, value: '特定名单' },
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}

/**
 * @Author: 何志祥
 * @Date: 2022-03-11 14:39:30
 * @Description: 风险单操作类型
 * @param {*} value
 * @param {*} type
 * type='filter' 代表是筛选
 */
 export const operationTypeFormat = (value, type = 'filter') => {
  const obj = [
    { key: 1, value: '提交'},
    { key: 2, value: '分配'},
    { key: 3, value: '处理'},
    { key: 4, value: '遗留'},
    { key: 5, value: '退回'},
    { key: 6, value: '转移'},
    { key: 7, value: '提交审核'},
    { key: 8, value: '审核通过'},
    { key: 9, value: '审核不通过'},
    { key: 10, value: '回收'}
  ]

  // 如果是筛选
  if (type === 'filter') {
    for (let val of obj) {
      if (val.key == value) {
        return val.value;
      }
    }
  } else {
    // 如果是返回全部
    return obj
  }
}
