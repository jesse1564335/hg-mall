var Helper = require('./Helper.js');
var ICache = require('./ICache.js');
var ISession = require('./ISession.js');
var CasEnum = require('./CasEnum.js');
var Lib = require('./Lib.js');
var CasException = require('./CasException.js');
var _ = require('underscore');

var prefixKey = "cas:key:";
var prefixUserKey = "cas:key:user:";

/**
 * Cas constructor.
 * @param {ICache} cache
 * @param {ISession} session
 * @param casUrl
 * @param systemId
 * @param {Helper|null} helper
 * @author oujiamin
 * @Date 2019-07-15 14:53
 */
function Cas (cache, session, casUrl, systemId, helper) {
    this['casEnum'] = CasEnum;
    this['casEnum']['casUrl'] = casUrl;
    this['systemId'] = systemId;

    if (Lib.ensureImplements(cache, ICache)) {
        // 保证传入的cache对象是接口ICache的实例
        this['cache'] = cache;
    } else {
        throw new CasException('interface_type', cache);
    }
    
    if (Lib.ensureImplements(session, ISession)) {
        // 保证传入的session对象是接口ISession的实例
        this['session'] = session;
    } else {
        throw new CasException('interface_type', session);
    }

    this['helper'] = _.isUndefined(helper) ? new Helper() : helper;
};

/**
 * 设置接口请求加密Key
 * @param signKey
 */
Cas.prototype.setSignKey = function(signKey) {
    if (!signKey || signKey == '') {
        throw new CasException('sign_key', '不能为空')
    }
    this['signKey'] = signKey;
};

/**
 * 是否登录
 * @return boolean
 */
Cas.prototype.hasLogin = function() {
    // 获取用户id
    var userId = this['session'].get(prefixUserKey);

    // "", null, undefined, false
    // 判断用户id是否有值
    if (!userId) {
        return false;
    }

    // 获取cache
    var cache = this['cache'].get(prefixUserKey + userId);
    if (!cache) {
        return false;
    }
    return true;
};

/**
 * 清理
 */
Cas.prototype.clear = function() {
    var userId = this['session'].get(prefixUserKey);
    // 获取用户id失败
    if (userId) {
        // 清理指定用户
        this.cleanUser(userId);
        //throw new CasException('sign_key', '获取session中userId失败');
    }
    this['session'].flush();
};

/**
 * 清理指定用户
 * @param userId
 */
Cas.prototype.cleanUser = function(userId) {
    this['cache'].del(prefixUserKey + userId);
};

/**
 * 重定向登录
 * @param originBackUrl
 * @return string
 */
Cas.prototype.redirectLogin = function(originBackUrl) {
    var helper = new Helper();
    var state = helper.genRandStr(10);
    this['session'].set(prefixKey + 'state', state);
    console.log("cas-redirectLogin-state："+state);
    var query = {
        state: state,
        redirect_url: originBackUrl,
    };

    // 生成秘钥
    var params = helper.paramsSignature(query, this['signKey']);
    // 重定向保存信息地址
    var authPreStore = this['casEnum'].genUrl(this['casEnum']['authPreStore']);

    return authPreStore + '?' + Lib.obj2UrlParam(params);
};

/**
 * 退出登录
 * @param redirectUrl
 * @param systemUserId
 * @return string
 */
Cas.prototype.logout = function(redirectUrl, systemUserId) {
    // 通知 cas 服务器
    this.clear();
    
    var query = {
        redirect_url: redirectUrl,
        system_id: this['systemId'],
        system_user_id: systemUserId,
    };
    // 生成秘钥
    var params = this['helper'].paramsSignature(query, this['signKey']);
    // 重定向 退出登录地址
    var authLogout = this['casEnum'].genUrl(this['casEnum']['authLogout']);

    return authLogout + '?' + Lib.obj2UrlParam(params);
};

/**
 * 回调
 * @param ticket
 * @param state
 * @param callback 回调函数
 */
Cas.prototype.callback = function (ticket, state, callback) {
    var _this = this;
    var sessionState = this['session'].get(prefixKey + 'state');
    // 该数据有误
    // if (sessionState !== state) {
    //     console.log("cas-callback-回调state："+state);
    //     console.log("cas-callback-ticket："+ticket);
    //     console.log("cas-callback-sessionState："+sessionState);
    //     callback(null,{'msg':"传入不一致"})
    //     return;
    //     //throw new CasException('state', "传入不一致");
    // }

    var query = {
        ticket: ticket,
        system_id: this['systemId'],
    };
    // 生成秘钥
    var params = this['helper'].paramsSignature(query, this['signKey']);
    // 请求地址
    var aurl = this['casEnum'].genUrl(this['casEnum']['authToken']);
    console.log("cas-callback-aurl--------:"+aurl)
    // 请求用户数据
    this['helper'].curl(aurl, params, function(res) {
        console.log("cas-callback-res------"+JSON.stringify(res))
        // 无数据处理
        if (!res) {
            callback(null,{'curl_empty':{
                ticket: ticket,
                state: state
            },"msg":"没有数据处理"})
            return;
            /*throw new CasException('curl_empty', {
                ticket: ticket,
                state: state
            });*/
        }

        // res.code不为0
        if (res['code'] != 0) {
            callback(null,{'curl_date_error':{
                ticket: ticket,
                state: state,
                res: res
            },"msg":res['msg'] })
            /*throw new CasException('curl_date_error', {
                ticket: ticket,
                state: state,
                res: res
            });*/
            return;
        }
        // 用户id
        var userId = res['data'];
            
        // 设置用户id-> cache，session
        _this['cache'].set(prefixUserKey + userId, userId);
        _this['session'].set(prefixUserKey, userId);
        // 当前系统的用户id
        callback(userId);
    });
};

/**
 * 绑定用户, userId 为 cas 中的 userId, systemUserId 为本系统的用户userId
 * 若本系统的 userId 不是唯一，则可以加加上对应的标识符来做唯一
 * 比如 : 呼叫系统，那么则业务的 systemUserId 可以为 sell:123
 * @param systemUserId
 * @param userId
 * @throws CasException
 * @param callback 回调函数
 */
Cas.prototype.bindUser = function (systemUserId, userId, callback) {
    var query = {
        user_id: userId,
        system_user_id: systemUserId,
        platform_system_id: this['systemId']
    };
    
    // 获取秘钥
    var params = this['helper'].paramsSignature(query, this['signKey']);
    // 请求地址
    var aurl = this['casEnum'].genUrl(this['casEnum']['authBind']);

    // 获取绑定用户管理
    this['helper'].curl(aurl, params, function(res) {
        // 无数据处理
        if (!res) {
            callback(null,{'curl_empty':{
                systemUserId: systemUserId,
                userId: userId
            },"msg":"没有数据处理"})
            return;
            /*throw new CasException('curl_empty', {
                systemUserId: systemUserId,
                userId: userId
            });*/
        }

        // res.code不为0
        if (res['code'] != 0) {
            callback(null,{'curl_date_error':{
                systemUserId: systemUserId,
                userId: userId,
                res: res
            },"msg":res['msg']})
            return;
            /*throw new CasException('curl_date_error', {
                systemUserId: systemUserId,
                userId: userId,
                res: res
            });*/
        }
        callback(res['data']);
    });
};

/**
 * 获取用户列表
 *
 * @param {object} query
 * @param {number} perPage
 * @param {number} page
 * @param callback 回调函数
 * @throws CasException
 */
Cas.prototype.pagingUserList = function (query, perPage, page, callback){
    query = query || {};
    query['per_page'] = perPage;
    query['page'] = page;

    // 获取秘钥
    var params = this['helper'].paramsSignature(query, this['signKey']);
    // 请求地址
    var aurl = (this['casEnum'].genUrl(this['casEnum']['authPagingUserList']) + "?" + Lib.obj2UrlParam(params));

    // 请求用户数据
    this['helper'].curl(aurl, function (res) {
        if (!res) {
            throw new CasException('curl_empty', {
                query: query,
                perPage: perPage,
                page: page
            });
        }

        // res.code不为0
        if (res['code'] != 0) {
            throw new CasException('curl_date_error', {
                query: query,
                perPage: perPage,
                page: page,
                res: res
            });
        }

        callback(res['data']);
    });
};

module.exports = Cas;