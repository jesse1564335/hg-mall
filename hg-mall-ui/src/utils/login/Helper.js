var _ = require('underscore');
var Lib = require('./Lib.js');
var Md5 = require('md5');
var Request = require('request');


var RAND_CHAR = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

/**
 * @author oujiamin
 * @date 2019-07-15 15:40
 */

function Helper () {}

/**
 * 随机生成指定长度的随机字符串
 * @param length
 * @return string
 * @todo php函数转为js方法
 */
Helper.prototype.genRandStr = function(length) {
    // 若传入的不符合规范的字符串，则直接使用 10
    if (!length || !_.isNumber(length) || length < 10) {
        length = 10;
    }

    var string = '';
    var strlen = RAND_CHAR.length;

    for (var i = length; i > 0; i--) {
        string += RAND_CHAR[_.random(0, strlen - 1)];
    }

    return string;
}

/**
 * curl 请求
 * @param {string} url
 * @param {object|function} postData 提交数据或者回调函数
 * @param {object|function} useCert 证书数据或者回调函数
 * @param callback 回调函数
 */
Helper.prototype.curl = async function(url, postData, useCert, callback) {
    // 传进来的参数可能是2|3|4|5个，判断哪个是回调函数
    if (_.isFunction(postData) && arguments.length == 2) {
        callback = postData;
        postData = null;
        useCert = {};
    } else if (_.isFunction(useCert) && arguments.length == 3) {
        callback = useCert;
        useCert = {};
    } else {
        postData = postData || null;
        useCert = useCert || {};
    }

    var method = 'get';
    if (postData) {
        method = 'post';
    }

    var options = {
        url: url,
        method: method,
        form: postData,
        agentOptions: {}
    };

    // 设置证书
    if (!_.isEmpty(useCert)) {
        options['agentOptions']['cert'] = useCert['sslcertPath'];
        options['agentOptions']['key'] = useCert['sslkeyPath'];
    }

    Request(options, function(error, response, body) {
        console.log(error)
        if (!error && response.statusCode == 200) {
            var res = _.isString(body) ? JSON.parse(body) : body;
            callback(res);
        } else {
            callback(null);
        }
    });
}

/**
 * 生成证书
 * @param params 参数
 * @param key  string  加密的key
 * @param exceptArr  array 排除的字段
 * @return string
 */
Helper.prototype.genSign = function (params, key, exceptArr) {
    exceptArr = exceptArr || [];
    
    
    var tmpstr = '';
    tmpstr += this.kSortParams(params, key, exceptArr);

    if (tmpstr == "") {
        tmpstr += '_key=' + key;
    } else {
        tmpstr += '&_key=' + key;
    }

    // 先md5加密后在把所有小写字母转换成大写
    var sign = Md5(tmpstr).toUpperCase();

    return sign;
}

/**
 * 过滤字符串 && 数字 && 数组 && 对象的属性值两端的空格
 * @author  oujiamin
 * @param   data 需要过滤的数据
 * @notic   支持多维
 */
Helper.prototype.trimAny = function (data) {
    // 数字转字符串
    if (_.isNumber(data)) {
        data = Number(data);
    }

    // 判断是否字符串
    if (_.isString(data)) {
        return Lib.handleTrim(data);
    }

    
    // 判断是否对象或者数组
    if (_.isObject(data) || _.isArray(data)) {
        // 按照 key 排序
        for (var key in data) {
            var value = data[key];
            if (_.isObject(value) || _.isArray(value)) {
                data[key] = this.trimAny(value);
            } else {
                data[key] = Lib.handleTrim(value);
            }
        }
    }

    return data;
}

// 按照 key 排序
Helper.prototype.kSortParams = function(value, k, exceptArr) {
    exceptArr = exceptArr || [];
    
    // 数字转字符串
    if (_.isNumber(value)) {
        value = String(value);
    }

    // 判断是否字符串
    if (_.isString(value)) {
        return Lib.handleTrim(value);
    }

    
    var outKey = [];
    for (var i in value) {
        if (_.indexOf(exceptArr, k) == -1 && !_.isNaN(value[i]) && !_.isNull(value[i]) && !_.isUndefined(value[i]) && value[i] !== '') {
            outKey.push(i);
        }
    }

    outKey.sort()

    var outStr = "";
    for (var i in outKey) {
        outStr += outKey[i] + "=" + value[outKey[i]] + "&";
    }
    
    if (outKey.length > 0) {
        return outStr.substr(0, outStr.length-1);
    }

    return "";
}

/**
 * 请求参数生成秘钥
 *
 * @param params
 * @param signKey
 * @return object
 */
Helper.prototype.paramsSignature = function (params, signKey) {
    if (!_.isObject(params)) {
        params = {};
    }
    params['_ts'] = Lib.getNow();
    params['_rd'] = this.genRandStr(8);
    params['_sign'] = this.genSign(params, signKey, ['_sign']);

    return params;
}

module.exports = Helper;
