/**
 * 模拟接口类，必须提供相关方法
 * 
 */
function ISession() {
    this['interfaces'] = ['set', 'get', 'flush'];
    this.storage = {},
    this.set = function(key, value) {
        this.storage[key] = value;
    };
    this.get = function(key) {
        return this.storage[key];
    };
    this.flush = function(){
        for(var sKey in this['storage']) {
            delete this['storage'][sKey];
        }
    };
}


ISession.prototype.set = function(key, value) {};
ISession.prototype.get = function(key) {};
ISession.prototype.flush = function() {};

module.exports = ISession;
