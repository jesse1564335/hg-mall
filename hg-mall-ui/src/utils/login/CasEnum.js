/*
 * @Author: 何志祥
 * @Date: 2021-12-28 10:16:01
 * @LastEditors: 何志祥
 * @LastEditTime: 2021-12-28 10:48:15
 * @Description: 
 */
/**
 * @author oujiamin
 * @date 2019-07-15 15:40
 */
var CasEnum = {
    casUrl: "http://company.module.58ag8.com",
    authPreStore: "/web/cas/auth/pre-store", // 重定向保存信息
    authLogout: "/v1/cas/auth/logout", // 重定向 退出登录
    authToken: "/v1/cas/auth/ticket", // 通过 ticket 获取对应的用户信息
    authBind: "/v1/cas/entity/user-platform-system", // 绑定用户管理
    authPagingUserList: "/v1/cas/entity/user-list", // 分页获取用户列表
    /**
     * 生成指定链接
     * @param urlPath
     * @return {String}
     */
    genUrl: function (urlPath) {
        return this['casUrl'] + urlPath;
    }
}

module.exports = CasEnum;