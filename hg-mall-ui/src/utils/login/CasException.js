/**
 * CasException继承自Error，继承方式-组合式
 * @author oujiamin
 * @date 2019-07-15 15:40
 */
function CasException () {
    Error.call(this);
    this['interfaceType'] = "interface_type";
    this['state'] = "state";
    this['curlEmpty'] = "curl_empty";
    this['curlDateError'] = "curl_date_error";
}
CasException.prototype = new Error();
CasException.prototype.constructor = CasException;

/**
 * CasException 转字符串
 * @return string
 */
CasException.prototype.__toString = function(type, data) {
    return '错误类型:' + this[type] || type + ';错误内容:' + json_encode(data);
}

module.exports = CasException;
