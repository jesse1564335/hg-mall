/**
 * 模拟接口类，必须提供相关方法
 * 
 */
function ICache() {
    this.cache = {};
    this['interfaces'] = ['set', 'get', 'del'];
    this.set =  function(key, value) {
        this.cache[key] = value;
    };
    this.get =  function(key) {
        return this.cache[key];
    };
    this.del =  function(key) {
        delete this.cache[key];
    };
}

ICache.prototype.set = function(key, value) {};
ICache.prototype.get = function(key) {};
ICache.prototype.del = function(key) {};

module.exports = ICache;
