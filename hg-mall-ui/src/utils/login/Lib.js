var Moment = require("moment");
var _ = require('underscore');

var Lib = {
    /**
     * 过滤左右空格
     * @author  huzisong
     */
    handleTrim: function (str) {
        if (!_.isString(str)) {
            // 非字符串返回原值
            return str;
        }
        return str.replace(/^(\s|\u00A0)+/,'').replace(/(\s|\u00A0)+$/,'');
    },

    /**
     * 获取当前时间
     */
    getNow: function () {
        return Moment().format('YYYYMMDDHHmmss');
    },

    /**
     * 保证某个实例实现了某个接口类的所有方法
     * @param {Object} instance 某个实例
     * @param {class} iClass 某个接口类
     * @return {Boolean}
     * @author oujiamin
     * @date 2019-07-22 10:10
     */
    ensureImplements: function (instance, IClass) {
        // console.log('lib-----',instance);
        // console.log('lib-----',IClass);

        if (!instance || !IClass) {
            return false;
        }
        var isSatisfy = true; // 实例是否满足接口类

        // 获取所有keys值
        var keys = _.keys(instance);
        var iClassInstance = new IClass();
        var interfaces = iClassInstance['interfaces']; // 接口类里面定义的需要实现的接口函数数组

        for (var i = 0; i < interfaces.length; i++) {
            var val = interfaces[i];
            // console.log('lib----------', instance[val])
            // console.log('lib----------', keys)
            if ((_.indexOf(keys, val) == -1) || !_.isFunction(instance[val])) {
                //console.log('lib----------', val)
                isSatisfy = false;
                break;
            }
        }
        return isSatisfy;
    },

    /**
     * 对象转url参数字符串
     * @param {Object} obj
     * @return {string} 转换后字符串
     * @author oujiamin
     * @date 2019-07-23 09:40
     */
    obj2UrlParam: function(obj) {
        if (obj == null) {
            return '';
        }
        var esc = encodeURIComponent;
        var keys = _.keys(obj);
        var params = []; // 参数数组
        
        _.forEach(keys, function(k) {
            if (obj[k] != null) {
                // null和undefined不传
                params.push(esc(k) + '=' + esc(obj[k]));
            }
        });
        return params.join('&');
    }
};

module.exports = Lib;
