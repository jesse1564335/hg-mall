/*
 * @Author: 何志祥
 * @Date: 2022-04-01 13:36:05
 * @LastEditors: 何志祥
 * @LastEditTime: 2022-04-08 11:00:32
 * @Description: 导出excel
 */

/**
 * @Author: 何志祥
 * @Date: 2022-04-01 13:24:12
 * @Description: 导出 excel 表格(前端实现)
 * @param {*} tHeader 表头 - 数组(比如: ['名字', '年龄'])
 * @param {*} filterVal 表头对应取值的字段 - 数组(比如: ['name', 'age'])
 * @param {*} data  数据
 * @param {*} filename  导出文件名
 * @param {*} isAutoWidth 是否自适应宽度
 */
 export const exportExcel = (
  tHeader,
  filterVal,
  data,
  filename,
  isAutoWidth = true
) => {
  require.ensure([], () => {
      const { export_json_to_excel } = require('./Export2Excel');
      const tempData = data.map(v => filterVal.map(j => v[j]));
      export_json_to_excel(tHeader, tempData, filename, isAutoWidth);
  });
};


/**
 * @Author: 何志祥
 * @Date: 2022-04-08 11:00:33
 * @Description: 解决Vue Template模板中无法使用可选链的问题
 * @param {*} obj
 * @param {array} rest
 */
export const optionalChaining = (obj, ...rest) => {
  let tmp = obj;
  for (let key in rest) {
    let name = rest[key];
    tmp = tmp?.[name];
  }
  return tmp || "";
};
