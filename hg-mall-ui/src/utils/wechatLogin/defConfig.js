const DEV = {
  req: {
    localUrl: 'http://localhost:1024/',
  },
  cas: {
    signKey: 'Q0Ijt6NZQvTuntvRfDxARviqZGAzf9lg',
    systemId: 2,
    casUrl: 'http://testcompanymodule.gp932.com',
    user_type: 'company_wechat',
    company_id: "9,15,17",
  }
}


const SIT = {
  req: {
    localUrl: 'http://8.136.114.198:10000/'
  },
  cas: {
    signKey: 'Q0Ijt6NZQvTuntvRfDxARviqZGAzf9lg',
    systemId: 2,
    casUrl: 'http://testcompanymodule.gp932.com',
    user_type: 'company_wechat',
    company_id: "9,15,17",
  },
}



const PRD = {
  req: {
      localUrl: 'https://hydrus.gp343.com/',
  },
  cas: {
    signKey: 'K4naCByUAy5V71c5hKQQtGI3RkUfjDBu',
    systemId: 2,
    casUrl: 'https://companymodule.gp343.com',
    user_type: 'company_wechat',
    company_id: "3,11"
  }
}

const CONFIG = {
  SIT,
  PRD,
  DEV
};
// - 判断环境
let ENV;
if (process.env.NODE_ENV === 'production') {
  ENV = 'PRD';
}  else if (process.env.NODE_ENV === 'development') {
  ENV = 'DEV';
} else {
  ENV = 'SIT';
}
// 特殊处理：编译时候的NODE_ENV=production，导致在测试环境配置读取的是生产的
if (process.env.CONFIG_ENV == 'sit') {
  ENV = 'SIT'
}

// - 根据环境获取对应的配置列表
const defConfig = CONFIG[ENV];

/**
 *
 * @param {string} type 类型
 */
let getOption = function (type) {
  return defConfig[type]
}

module.exports = getOption;
