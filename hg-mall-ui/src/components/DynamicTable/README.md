<!--
 * @Author: 何志祥
 * @Date: 2022-02-25 09:32:29
 * @LastEditors: 何志祥
 * @LastEditTime: 2022-04-20 13:52:55
 * @Description: 动态表格 + 动态表单 配置文件
-->

# 动态表格 + 动态表单 配置说明文件



## 组件引入选择

```
1. 如果只需要searchForm, 不需要 table，则直接用 dySearch 这个全局组件即可

2. 如果只需要表格, 不需要上方 searchForm 页面，则直接用 dyTable 这个全局组件即可

3. 需要 searchForm + table 布局, 则引入 dySearchTable 组件

4. 如果需要表格且不需要分页, 则 total 传 0, 或者在父页面添加插槽, 名字定义为pagination，给插槽插入空div即可
```


## 组件插槽定义

```bash

# 1. 自定义搜索表单内容插槽(父组件写入)

<template v-slot:插槽名字自定义>
  ...
</template>


# 2. 加在 searchForm 和 table之间的按钮，比如新增按钮(父组件写入)

<template v-slot:extraBtnList>
  ...
</template>


# 3. 自定义表格插槽(父组件写入)

<template v-slot:table> 
  ...
</template>


# 4. 自定义分页插槽(父组件写入)

<template v-slot:pagination> 
  ...
</template>


# 5. 自定义 <table-column></table-column>  作用域插槽, 值回传至当前页
<template v-slot:自定义插槽名字="{ columnData }">
  {{columnData}}
</template>
```


## 组件配置项  

```bash
# 1. searchForm 配置项  

  最终所有的表单数据都在 propSearchData.formData 里面


  1). input 配置项
      # 对象名需要为此表单v-model绑定的属性名
      customNumber: {
        type: "input",               # 表单类型
        label:  "客户编号",           # 表单名字
        placeholder: "请输入客户编号"  # 默认展示请输入xxx, 可不传
        inputLimit: () => {}         # 增加输入限制用，默认不传
      }


  2). select 配置项
      # 对象名需要为此表单v-model绑定的属性名
      origin: {
        type: "select",
        label: "来源",
        placeholder: "请选择来源数据",     # 默认展示请选择xxx, 可不传
        optionLabel: "label",           # select 下拉框数据需要绑定的 label 字段
        optionValue: "value",           # 选择该 label 获取的值的 value
        disabled: true,                 # 是否禁用
        multiple: true,                 # 是否多选

        # 下拉数据不需要从接口获取的情况
        optionList: [
          {
            label: "全部",
            value: undefined,
          },
          {
            label: "来源1",
            value: 1,
          },
          {
            label: "来源2",
            value: 2,
          }
        ]

        # 下拉数据需要从接口获取的情况参考 index.vue 页面
      }


  3) 时间选择器组件
      # 对象名需要为此表单v-model绑定的属性名
      orderTime: {
        type: "time_picker",    # 代表走时间控件
        time_type: 'datetimerange',  #  时间控件的类型; 可不传, 默认 datetimerange
        value_format: "yyyy-MM-dd HH:mm:ss",  # 时间格式; 可不传, 默认 yyyy-MM-dd H:mm:ss
        label: "订单创建时间",
        value: "orderTime",    # 需要为此表单v-model绑定的属性名
        range_separator: "至",
        start_placeholder: "开始日期",
        end_placeholder: "结束日期",
      }

  
  4) 自定义表单(需要在上方加入插槽, 插槽名字跟下面 name 名字一致)
    # 自定义表单 插槽 的 name 值
    customForm123: {
      name: "customForm123", # 自定义表单 插槽 的 name 值
      isCustomForm: true    # 是否需要自定义form-item(不走动态表单)
    }


# 2. table 配置项
    
    # 选中变更列表选中行颜色
    setSelected: true
    
    说明： 1. 有 type 字段的代表是需要额外处理的
          2. 如果需要分页功能, 则需要在 dyTable 或者 dySearchTable 组件加入 @pagination="xxx", xxx代表获取列表方法
      
    {
      label: "序号",
      type: "serial",  # 出现 type, 需要做额外操作
      width: 60,
      fixed: "left"   # 是否固定栏
    },
    {
      label: "客户姓名",
      prop: "realName",
      width: 100,
      fixed: "left",
      isTooltip: true,       # 鼠标放到文字是否需要出现toast弹窗显示具体信息
      sortField: "realName", # 排序字段，需要根据哪个字段排序
      formatter: 'cidFormatter',  # 要格式化运行的筛选方法名, 具体实现在 dyTable.vue 组件中
    },
    {
      isCustomColumn: true,  # 是否自定义 table-column
      name: 'orderNumber',   # 自定义插槽名字
      label: "自定义column",
      width: 100,
      fixed: "left",
      sortField: "realName1",
    }

    # 需要多选框(详细使用看 ./index.vue )
    # 1. 定义多选框字段
    propTableData: {
      selectionList: []  # 如果需要多选操作, 添加此字段。此字段的值代表当前选中的行数据
    }
    # 2. 定义组件传值
    <dy-table :selectionList.sync="propTableData.selectionList"> </dy-table> 
  ```
