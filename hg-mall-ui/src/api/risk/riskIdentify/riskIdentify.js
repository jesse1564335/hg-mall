import request from '@/utils/request'

// 风险标识-列表
export function getIdentificationList(data) {
  return request({
    url: '/risk/identification/list',
    method: 'post',
    data: data
  })
}
// 风险标识-列表
export function getIdentificationDetail(data) {
  return request({
    url: '/risk/identification/getById',
    method: 'get',
    params: data
  })
}
// 风险标识-撤销
export function identificationCancel(data) {
  return request({
    url: '/risk/identification/cancel',
    method: 'post',
    data: data
  })
}
// 风险标识-信泽
export function identificationAdd(data) {
  return request({
    url: '/risk/identification/add',
    method: 'post',
    data: data
  })
}
