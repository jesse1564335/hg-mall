import request from '@/utils/request'

// 风险单-分配设置
export function distributeConfig(data) {
  return request({
    url: '/risk/distribute/query',
    method: 'post',
    data: data
  })
}
// 风险单-分配设置查询
export function distributeSet(data) {
  return request({
    url: '/risk/distribute/setting',
    method: 'post',
    data: data
  })
}
// 获取可分配人员
export function distributeUserListWithDeptId(data) {
  return request({
    url: '/risk/distribute/distributeUserList',
    method: 'post',
    data
  })
}
