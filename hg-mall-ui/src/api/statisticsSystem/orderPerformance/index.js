/*
 * @Author: 何志祥
 * @Date: 2021-10-28 13:43:07
 * @LastEditors: 何志祥
 * @LastEditTime: 2021-11-04 10:45:40
 * @Description: 统计系统 - 订单业绩 接口api
 */

import request from '@/utils/request'

/**
 * @Author: 何志祥
 * @Date: 2021-10-28 13:44:30
 * @Description: 商务统计列表
 */
export function getOrderList(data) {
    return request({
        url: '/orderSummary/getOrderList',
        method: 'post',
        data: data
    })
}

/**
 * @Author: 何志祥
 * @Date: 2021-11-01 09:54:56
 * @Description: 商务统计详情
 */
 export function getOrderDetail(data) {
    return request({
        url: '/orderSummary/getOrderDetail',
        method: 'post',
        data: data
    })
}

/**
 * @Author: 何志祥
 * @Date: 2021-11-01 15:05:36
 * @Description: 修改记录
 */
 export function list(data) {
    return request({
        url: '/operation/log/list',
        method: 'post',
        data: data
    })
}

/**
 * @Author: 何志祥
 * @Date: 2021-11-01 15:05:36
 * @Description: 修改业务员
 */
 export function updateOrderFollower(data) {
    return request({
        url: '/orderSummary/updateOrderFollower',
        method: 'post',
        data: data
    })
}

/**
 * @Author: 何志祥
 * @Date: 2021-11-03 10:49:56
 * @Description: 商务统计导出设置列表
 * @param {*} data
 */
export function exportList(data) {
    return request({
        url: '/statistics/export/list',
        method: 'post',
        data: data
    })
}

/**
 * @Author: 何志祥
 * @Date: 2021-11-03 10:50:40
 * @Description: 商务统计删除导出设置
 * @param {*} data
 */
export function exportDelete(data) {
    return request({
        url: '/statistics/export/delete',
        method: 'post',
        data: data
    })
}

/**
 * @Author: 何志祥
 * @Date: 2021-11-03 10:50:40
 * @Description: 商务统计添加或修改导出设置
 * @param {*} data
 */
 export function addOrUpdate(data) {
    return request({
        url: '/statistics/export/addOrUpdate',
        method: 'post',
        data: data
    })
}

/**
 * @Author: 毛诚杰
 * @Date: 2021-12-17 10:20:40
 * @Description: 审核订单列表
 * @param {*} data
 */
export function getOrderApplyList(data) {
  return request({
    url: '/orderApply/getList',
    method: 'post',
    data: data
  })
}

/**
 * @Author: 毛诚杰
 * @Date: 2021-12-17 10:20:40
 * @Description: 获取审核信息
 * @param {*} data
 */
export function getApplyDetail(data) {
  return request({
    url: '/orderApply/getApplyDetail',
    method: 'post',
    data: data
  })
}

/**
 * @Author: 毛诚杰
 * @Date: 2021-12-17 10:20:40
 * @Description: 获取审核信息
 * @param {*} data
 */
export function submitOrderApply(data) {
  return request({
    url: '/orderApply/audit',
    method: 'post',
    data: data
  })
}
