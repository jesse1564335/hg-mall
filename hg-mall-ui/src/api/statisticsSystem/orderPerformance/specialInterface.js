/*
 * @Author: 何志祥
 * @Date: 2021-11-01 18:00:10
 * @LastEditors: 何志祥
 * @LastEditTime: 2022-01-24 10:05:16
 * @Description: 特殊接口 - 走cools项目的域名
 */

import request from '@/utils/requestOther'

/**
 * @Author: 何志祥
 * @Date: 2021-10-28 13:44:30
 * @Description: 查询业务员
 */
 export function info(data) {
    return request({
        url: 'scrm-business/staff/list',
        method: 'post',
        data: data
    })
}

/**
 * @Author: 何志祥
 * @Date: 2022-1-24 13:44:30
 * @Description: 查询业务员(新)
 */
 export function staffLevellist(data) {
    return request({
        url: 'scrm-business/staff/staffLevellist',
        method: 'post',
        data: data
    })
}


/**
 * @Author: 何志祥
 * @Date: 2021-10-28 13:44:30
 * @Description: 获取产品名称
 */
 export function listProductName(data) {
    return request({
        url: 'product-store/store/listProductName',
        method: 'post',
        data: data
    })
}


/**
 * @Author: 何志祥
 * @Date: 2021-10-28 13:44:30
 * @Description: 获取来源
 */
 export function option(params) {
    return request({
        url: 'scrm-business/field/list/option',
        method: 'get',
        params: params
    })
}