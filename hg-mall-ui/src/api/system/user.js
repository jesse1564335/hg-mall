import request from '@/utils/request'
import requestFile from '@/utils/requestFile'
import { praseStrEmpty } from "@/utils/hydrus";

// 企微登录根据用户userId查token
export function ewechatLogin(query) {
  return request({
    url: '/ewechatLogin',
    method: 'post',
    data: query
  })
}


// 查询用户列表
export function listUser(query) {
  return request({
    url: '/system/user/list',
    method: 'get',
    params: query
  })
}

// 查询用户详细
export function getUser(userId) {
  return request({
    url: '/system/user/info',
    method: 'post',
    data: {'userId':  praseStrEmpty(userId)}
  })
}

// 新增用户
export function addUser(data) {
  return request({
    url: '/system/user/add',
    method: 'post',
    data: data
  })
}

// 修改用户
export function updateUser(data) {
  return request({
    url: '/system/user/update',
    method: 'post',
    data: data
  })
}

// 删除用户
export function delUser(userId) {
  return request({
    url: '/system/user/' + userId,
    method: 'delete'
  })
}

// 导出用户
export function exportUser(query) {
  return request({
    url: '/system/user/export',
    method: 'get',
    params: query
  })
}

// 用户密码重置
export function resetUserPwd(userId, password) {
  return request({
    url: '/system/user/resetPwd/' + userId + "/" + password,
    method: 'get'
  })
}

// 用户状态修改
export function changeUserStatus(userId, status) {
  return request({
    url: '/system/user/isDisable/' + userId + "/" + status,
    method: 'get'
  })
}

// 查询用户个人信息
export function getUserProfile() {
  return request({
    url: '/system/user/profile',
    method: 'get'
  })
}

// 修改用户个人信息
export function updateUserProfile(data) {
  return request({
    url: '/system/user/updateProfile',
    method: 'post',
    data: data
  })
}

// 用户密码重置
export function updateUserPwd(oldPassword, newPassword) {
  const data = {
    oldPassword,
    newPassword
  }
  return request({
    url: '/system/user/profile/updatePwd',
    method: 'put',
    params: data
  })
}

// 用户头像上传
export function uploadAvatar(data) {
  return requestFile({
    url: '/file/system/img',
    method: 'post',
    data: data
  })
}

// 保存用户头像
export function saveAvatar(data) {
  return request({
    url: '/system/user/avatar',
    method: 'post',
    data: {url: data}
  })
}
