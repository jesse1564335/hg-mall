/*
 * @Author: 何志祥
 * @Date: 2021-10-28 13:43:07
 * @LastEditors: 何志祥
 * @LastEditTime: 2022-03-09 13:41:11
 * @Description: 风险系统 - 风险任务 接口api
 */

import request from '@/utils/request';

/**
 * @Author: 何志祥
 * @Date: 2022-03-03 09:41:02
 * @Description: 任务分配 - 退回
 * @param {*} data
 */
export function refusal(data) {
  return request({
    url: '/risk/riskOrder/refusal',
    method: 'post',
    data: data,
  });
}

/**
 * @Author: 何志祥
 * @Date: 2022-03-03 09:41:02
 * @Description: 任务分配 - 转单
 * @param {*} data
 */
 export function transfer(data) {
  return request({
    url: '/risk/riskOrder/transfer',
    method: 'post',
    data: data,
  });
}


/**
 * @Author: 何志祥
 * @Date: 2022-03-03 09:41:02
 * @Description: 任务分配 - 遗留
 * @param {*} data
 */
 export function handDown(data) {
  return request({
    url: '/risk/riskOrder/handDown',
    method: 'post',
    data: data,
  });
}


/**
 * @Author: 何志祥
 * @Date: 2022-03-03 09:41:02
 * @Description: 任务分配 - 回收
 * @param {*} data
 */
 export function rollback(data) {
  return request({
    url: '/risk/riskOrder/rollback',
    method: 'post',
    data: data,
  });
}


/**
 * @Author: 何志祥
 * @Date: 2022-03-03 09:41:02
 * @Description: 风险单 - 提单
 * @param {*} data
 */
 export function commit(data) {
  return request({
    url: '/risk/riskOrder/commit',
    method: 'post',
    data: data,
  });
}
// 风险单-分配
export function riskOrderAllocate(data) {
  return request({
    url: '/risk/riskOrder/allocate',
    method: 'post',
    data: data,
  });
}
// 风险单-回收
export function riskOrderRollback(data) {
  return request({
    url: '/risk/riskOrder/rollback',
    method: 'post',
    data: data,
  });
}
// 风险单-转单
export function riskOrderTransfer(data) {
  return request({
    url: '/risk/riskOrder/transfer',
    method: 'post',
    data: data,
  });
}
