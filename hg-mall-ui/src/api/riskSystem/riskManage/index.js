/*
 * @Author: 何志祥
 * @Date: 2021-10-28 13:43:07
 * @LastEditors: 何志祥
 * @LastEditTime: 2022-03-24 16:04:55
 * @Description: 风险系统 - 风险管理 接口api
 */

import request from '@/utils/request';

/**
 * @Author: 何志祥
 * @Date: 2022-03-03 09:41:02
 * @Description: 黑名单列表
 * @param {*} data
 */
export function list(data) {
  return request({
    url: '/risk/black/list',
    method: 'post',
    data: data,
  });
}


/**
 * @Author: 何志祥
 * @Date: 2022-03-03 10:02:58
 * @Description: 黑名单-恢复
 */
export function recover(params) {
  return request({
    url: '/risk/black/recover',
    method: 'get',
    params: params,
  });
}


/**
 * @Author: 何志祥
 * @Date: 2022-03-01 17:00:30
 * @Description: 风险单-客户信息查询
 */
export function query(data) {
  return request({
    url: '/risk/customer/query',
    method: 'post',
    data: data,
  });
}


/**
 * @Author: 何志祥
 * @Date: 2022-03-04 13:44:48
 * @Description: 风险单-分页列表
 * @param {*} data
 */
export function page(data) {
  return request({
    url: '/risk/riskOrder/page',
    method: 'post',
    data: data,
  });
}


/**
 * @Author: 何志祥
 * @Date: 2022-03-04 13:44:48
 * @Description: 风险单-催促
 * @param {*} data
 */
 export function add(data) {
  return request({
    url: '/risk/riskOrder/hasten/add',
    method: 'post',
    data: data,
  });
}

/**
 * @Author: 何志祥
 * @Date: 2022-03-04 13:44:48
 * @Description: 风险单-催促记录
 * @param {*} data
 */
 export function hastenList(data) {
  return request({
    url: '/risk/riskOrder/hasten/list',
    method: 'post',
    data: data,
  });
}


/**
 * @Author: 何志祥
 * @Date: 2022-03-04 13:44:48
 * @Description: 风险单-详情
 * @param {*} data
 */
 export function risk_detail(data) {
  return request({
    url: '/risk/riskOrder/detail',
    method: 'post',
    data: data,
  });
}

/**
 * @Author: 何志祥
 * @Date: 2022-03-10 14:22:27
 * @Description: 关闭风险单
 * @param {*} data
 */
export function close(data) {
  return request({
    url: '/risk/riskOrder/close',
    method: 'post',
    data: data,
  });
}


export function log_list(data) {
  return request({
    url: '/risk/log/list',
    method: 'post',
    data: data,
  });
}


/**
 * @Author: 何志祥
 * @Date: 2022-03-03 10:09:17
 * @Description: 外诉风险单-列表
 * @param {*} data
 */
export function orderList(data) {
  return request({
    url: '/risk/out/complaint/order/list',
    method: 'post',
    data: data,
  });
}


/**
 * @Author: 何志祥
 * @Date: 2022-03-03 10:09:17
 * @Description: 外诉风险单-详情
 * @param {*} data
 */
 export function detail(data) {
  return request({
    url: '/risk/riskOrder/detail',
    method: 'post',
    data: data,
  });
}


/**
 * @Author: 何志祥
 * @Date: 2022-03-03 14:05:15
 * @Description: 外诉单-撤销
 * @param {*} data
 */
export function cancel(data) {
  return request({
    url: '/risk/out/complaint/order/cancel',
    method: 'post',
    data: data,
  });
}

/**
 * @Author: 何志祥
 * @Date: 2022-03-03 14:05:15
 * @Description: 风险单-获取短信-根据风险单编号
 * @param {*} data
 */
export function getMessage(data) {
  return request({
    url: '/risk/message/getMessage',
    method: 'post',
    data: data,
  });
}


/**
 * @Author: 毛诚杰
 * @Date: 2022-03-01 17:00:30
 * @Description: 风险单-客户订单查询
 */
export function getCustomerOrderList(data) {
  return request({
    url: '/risk/customer/getOrderList',
    method: 'get',
    params: data
  });
}

/**
 * @Author: 毛诚杰
 * @Date: 2022-03-01 17:00:30
 * @Description: 风险单-退回
 */
export function riskRefusal(data) {
  return request({
    url: '/risk/riskOrder/refusal',
    method: 'post',
    data: data,
  });
}
/**
 * @Author: 毛诚杰
 * @Date: 2022-03-01 17:00:30
 * @Description: 风险单-遗留
 */
export function riskHandDown(data) {
  return request({
    url: '/risk/riskOrder/handDown',
    method: 'post',
    data: data,
  });
}
/**
 * @Author: 毛诚杰
 * @Date: 2022-03-01 17:00:30
 * @Description: 风险单-根据部门查询短信模板
 */
export function getMessageTemplate(data) {
  return request({
    url: '/risk/message/getMessageTemplate',
    method: 'post',
    data: data,
  });
}
/**
 * @Author: 毛诚杰
 * @Date: 2022-03-01 17:00:30
 * @Description: 风险单-发送短信
 */
export function sendMessage(data) {
  return request({
    url: '/risk/message/sendMessage',
    method: 'post',
    data: data,
  });
}
/**
 * @Author: 毛诚杰
 * @Date: 2022-03-01 17:00:30
 * @Description: 风险单-呼出
 */
export function riskPhoneCall(data) {
  return request({
    url: '/risk/message/getDialUrl',
    method: 'post',
    data: data,
  });
}


/**
 * @Author: 何志祥
 * @Date: 2022-03-24 16:04:58
 * @Description: 判断用户是否有已退回
 * @param {*} data
 */
export function commitCheck(data) {
  return request({
    url: '/risk/riskOrder/commit/check',
    method: 'post',
    data: data,
  });
}
