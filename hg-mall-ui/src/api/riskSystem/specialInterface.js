/*
 * @Author: 何志祥
 * @Date: 2021-11-01 18:00:10
 * @LastEditors: 何志祥
 * @LastEditTime: 2022-03-09 15:44:55
 * @Description: 特殊接口 - 走cools项目的域名
 */

import request from '@/utils/requestOther'

/**
 * @Author: 何志祥
 * @Date: 2021-10-28 13:44:30
 * @Description: 查询业务员
 */
export function uploadAllPicture(data) {
    return request({
        url: 'third-auth/oss/uploadAllPicture',
        method: 'post',
        data: data
    })
}

