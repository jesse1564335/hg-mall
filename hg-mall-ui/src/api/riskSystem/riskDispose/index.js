/*
 * @Author: 何志祥
 * @Date: 2022-03-10 19:47:58
 * @LastEditors: 何志祥
 * @LastEditTime: 2022-03-11 13:26:51
 * @Description: 
 */
import request from '@/utils/request'

// 风险单处理 - 提交
export function commitPlan(data) {
  return request({
    url: '/risk/riskOrder/commitPlan',
    method: 'post',
    data: data
  })
}


// 风险单处理 - 审核
export function review(data) {
  return request({
    url: '/risk/riskOrder/review',
    method: 'post',
    data: data
  })
}


// 风险单处理 - 保存
export function savePlan(data) {
  return request({
    url: '/risk/riskOrder/savePlan',
    method: 'post',
    data: data
  })
}
