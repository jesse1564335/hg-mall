import requestFile from '@/utils/requestFile'

export function uploadImage(data) {
    return requestFile({
      url: '/file/system/img',
      method: 'post',
      data: data
    })
  }