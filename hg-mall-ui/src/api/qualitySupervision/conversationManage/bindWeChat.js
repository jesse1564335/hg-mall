import request from '@/utils/request'
import requestOther from '@/utils/requestOther'

// 企微会话 - 客户微信绑定列表
export function getCustomerList(data) {
  return request({
    url: '/company/user/list',
    method: 'post',
    data: data
  })
}
// 企微会话-通过cid查询绑定信息
export function getRelationByCid(data) {
  return request({
    url: '/company/relation/getRelationByCid',
    method: 'post',
    data: data
  })
}
// 企微会话-通过cidUid查绑定关系
export function getRelationByCidUid(data) {
  return request({
    url: '/company/relation/getRelationByCidUid',
    method: 'post',
    data: data
  })
}
// 企微会话-互客企微绑定
export function addRelation(data) {
  return request({
    url: '/company/relation/addRelation',
    method: 'post',
    data: data
  })
}
// 业务员通过UserId查询accid
export function getAccidByExtId(data) {
  return requestOther({
    url: '/scrm-business/staff/getAccidByExtId',
    method: 'get',
    params: data
  })
}
// 企微会话-企微解绑
export function delRelation(data) {
  return request({
    url: '/company/relation/delRelation',
    method: 'post',
    data: data
  })
}
// 企微会话-修复cid
export function repairCid(data) {
  return request({
    url: '/company/user/repair',
    method: 'post',
    data: data
  })
}
