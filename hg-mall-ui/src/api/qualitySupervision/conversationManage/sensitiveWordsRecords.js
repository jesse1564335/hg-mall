import request from '@/utils/request'

// 查询敏感词-聚合数据列表
export function getSensitiveWordAggregationList(data) {
  return request({
    url: '/sensitive/word/aggregation/list',
    method: 'post',
    data: data
  })
}
// 查询敏感词-详情数据列表
export function getSensitiveWordDetailList(data) {
  return request({
    url: '/sensitive/word/detail/list',
    method: 'post',
    data: data
  })
}
// 敏感词-统计数据--状态批量处理
export function sensitiveWordAggregationDeal(data) {
  return request({
    url: '/sensitive/word/aggregation/deal/batch',
    method: 'post',
    data: data
  })
}
// 敏感词-详情数据--状态批量处理
export function sensitiveWordDetailDeal(data) {
  return request({
    url: 'sensitive/word/detail/deal/batch',
    method: 'post',
    data: data
  })
}
// 企微-会话消息列表
export function getSensitiveCustomerList(data) {
  return request({
    url: 'sensitive/word/getSensitiveDetail/byAggregationId',
    method: 'post',
    data: data
  })
}
