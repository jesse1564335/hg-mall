import request from '@/utils/request'

// 查询质检分配设置
export function distributeConfig() {
  return request({
    url: '/quality/distribute/query',
    method: 'get'
  })
}

// 设置质检分配
export function distributeSet(data) {
  return request({
    url: '/quality/distribute/setting',
    method: 'post',
    data: data
  })
}

// 获取可分配人员
export function distributeUserList(data) {
  return request({
    url: '/quality/distribute/distributeUserList',
    method: 'get'
  })
}