import request from '@/utils/request'

// 查询任务列表
export function listTask(data) {
  return request({
    url: '/quality/task/getList',
    method: 'post',
    data: data
  })
}

// 分配任务
export function distributeTask(data) {
  return request({
    url: '/quality/task/distribute',
    method: 'post',
    data: data
  })
}

// 回收任务
export function recoveryTask(data) {
  return request({
    url: '/quality/task/recovery',
    method: 'post',
    data: data
  })
}

// 根据订单id查询录音文件列表
export function voiceList(data) {
  return request({
    url: '/quality/voice/getList',
    method: 'post',
    data: data
  })
}

// 单个录音文件审核结果
export function voiceCheck(data) {
  return request({
    url: '/quality/voice/updateStatus',
    method: 'post',
    data: data
  })
}

// 质检审核-违规详情
export function voiceInspect(query) {
  return request({
    url: '/quality/voiceRecordInspect/query',
    method: 'get',
    params: query
  })
}

// 质检审核-违规记录保存
export function saveVoiceInspect(data) {
  return request({
    url: '/quality/voiceRecordInspect/save',
    method: 'post',
    data: data
  })
}

// 质检结果列表
export function subList(data) {
  return request({
    url: '/quality/task/subList',
    method: 'post',
    data: data
  })
}

// 任务的录音或会话质检结果提交
export function subtaskAudit(data) {
  return request({
    url: '/quality/task/subtask/audit',
    method: 'post',
    data: data
  })
}

// 质检任务审核结果提交
export function taskAudit(data) {
  return request({
    url: '/quality/task/audit',
    method: 'post',
    data: data
  })
}
