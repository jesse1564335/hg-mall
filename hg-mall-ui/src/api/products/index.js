import request from '@/utils/request'

// 查询列表
export function listProduct(query) {
  return request({
    url: '/product/page',
    method: 'get',
    params: query
  })
}

// 查询产品详情
export function getProduct(productId) {
  return request({
    url: '/product/menu/menuInfo/' + menuId,
    method: 'get'
  })
}

// 新增产品
export function addProduct(data) {
  return request({
    url: '/products/add',
    method: 'post',
    data: data
  })
}

// 修改产品
export function updateProduct(data) {
  return request({
    url: '/products/update',
    method: 'post',
    data: data
  })
}

// 删除菜单
export function delMenu(menuId) {
  return request({
    url: '/system/menu/del/' + menuId,
    method: 'delete'
  })
}