import request from '@/utils/request'

// 查询分类列表
export function listCategory(query) {
  return request({
    url: '/product/category/list',
    method: 'post',
    data: query
  })
}

// 查询分类详细
export function getCategory(categoryId) {
  return request({
    url: '/product/category/info/' + categoryId,
    method: 'get'
  })
}

// 新增菜单
export function addCategory(data) {
  return request({
    url: '/product/category/add',
    method: 'post',
    data: data
  })
}

// 修改菜单
export function updateCategory(data) {
  return request({
    url: '/product/category/update',
    method: 'post',
    data: data
  })
}

// 删除菜单
export function delCategory(categoryId) {
  return request({
    url: '/product/category/' + categoryId,
    method: 'delete'
  })
}