/*
 * @Author: 何志祥
 * @Date: 2021-11-01 18:00:10
 * @LastEditors: 何志祥
 * @LastEditTime: 2022-02-13 13:51:23
 * @Description:  商务系统 - 产品权限 - 订单关闭模块接口api 特殊接口 - 走cools项目的域名
 */

import request from '@/utils/requestOther'

/**
 * @Author: 何志祥
 * @Date: 2021-10-28 13:44:30
 * @Description: 列表接口
 */
 export function orderList(data) {
    return request({
        url: 'console-product/store/manage/orderList',
        method: 'post',
        data: data
    })
}

/**
 * @Author: 何志祥
 * @Date: 2022-02-13 13:51:20
 * @Description: 关闭订单接口
 * @param {*} data
 */
export function cancelOrder(data) {
    return request({
        url: 'product-store/store/cancelOrder',
        method: 'post',
        data: data
    })
}
