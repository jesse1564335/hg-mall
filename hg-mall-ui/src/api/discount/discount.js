/*
 * @Author: 何志祥
 * @Date: 2022-03-31 17:30:24
 * @LastEditors: 何志祥
 * @LastEditTime: 2022-04-08 16:50:51
 * @Description: 优惠券接口api
 */

import request from '@/utils/requestDiscount'

/**
 * @Author: 何志祥
 * @Date: 2022-03-31 17:31:12
 * @Description: 已领优惠券列表
 * @param {*} data
 */
export function receivedCouponList(data) {
  return request({
      url: '/coupon/user/coupon/manage/list',
      method: 'post',
      data: data
  })
}

/**
 * 优惠券列别熬
 */
export function couponManageList(data) {
  return request({
    url: '/coupon/coupon/manage/list',
    method: 'post',
    data: data
  })
}
/**
 * 优惠券详情
 */
export function couponManagegetDetail(data) {
  return request({
    url: '/coupon/coupon/manage/getDetail',
    method: 'get',
    params: data
  })
}

/**
 * 新增/编辑优惠券
 * @param data
 * @returns {*}
 */
export function couponInsertOrUpdate(data) {
  return request({
    url: '/coupon/coupon/manage/insertOrUpdate',
    method: 'post',
    data: data
  })
}
/**
 * 获取产品列表
 * @param data
 * @returns {*}
 */
export function getProductList(data) {
  return request({
    url: '/goods/productAction/listProductManager',
    method: 'post',
    data: data
  })
}
export function getProductAddedList(data) {
  return request({
    url: '/goods/productAction/listProductAdded',
    method: 'post',
    data: data
  })
}
/**
 * 获取套餐列表
 * @param data
 * @returns {*}
 */
export function getPriceConfigList(data) {
  return request({
    url: '/goods/priceAction/listPrice',
    method: 'post',
    data: data
  })
}

/**
 * 导出兑换码
 * @param data
 * @returns {*}
 */
export function exportExchangeCode(data) {
  return request({
    url: '/coupon/coupon/manage/exportExchangeCode',
    method: 'post',
    data: data
  })
}
/**
 * 发券列表
 */
export function sendCouponConfigList(data) {
  return request({
    url: '/coupon/send/coupon/config/list',
    method: 'post',
    data: data
  })
}
/**
 * 发券编辑
 */
export function sendCouponConfigInsertOrUpdate(data) {
  return request({
    url: '/coupon/send/coupon/config/insertOrUpdate',
    method: 'post',
    data: data
  })
}
/**
 * 发券详情
 */
export function sendCouponConfigDetail(data) {
  return request({
    url: '/coupon/send/coupon/config/getDetail',
    method: 'get',
    params: data
  })
}


/**
 * @Author: 何志祥
 * @Date: 2022-04-01 17:50:16
 * @Description: 临时用户配置列表接口 - 短期
 */
 export function casualUserConfigList(data) {
  return request({
      url: '/data-collection/t/temp/user/config/list',
      method: 'post',
      data: data
  })
}


/**
 * @Author: 何志祥
 * @Date: 2022-04-01 17:50:16
 * @Description: 临时用户配置列表删除接口
 */
 export function configListDelete(id) {
  return request({
      url: '/data-collection/t/temp/user/config/delete?id=' + id,
      method: 'get'
  })
}


/**
 * @Author: 何志祥
 * @Date: 2022-04-06 10:20:29
 * @Description: 新增配置上传文件
 */
 export function uploadAllPicture(data) {
  return request({
      url: '/third-auth/oss/uploadAllPicture',
      method: 'post',
      data: data
  })
}

/**
 * @Author: 何志祥
 * @Date: 2022-04-06 10:20:29
 * @Description: 新增配置提交
 */
export function casualUserConfigUpdate(data) {
  return request({
      url: '/data-collection/t/temp/user/config/insertOrUpdate',
      method: 'post',
      data: data
  })
}

/**
 * @Author: 何志祥
 * @Date: 2022-04-06 10:20:29
 * @Description: 领券中心 - 列表
 */
 export function receiveCouponManageList(data) {
  return request({
      url: '/coupon/receive/coupon/manage/list',
      method: 'post',
      data: data
  })
}


/**
 * @Author: 何志祥
 * @Date: 2022-04-06 10:20:29
 * @Description: 用户列表 - 长期
 */
 export function couponManageUserList() {
  return request({
      url: '/data-collection/tag/information/select/all',
      method: 'get'
  })
}


/**
 * @Author: 何志祥
 * @Date: 2022-04-06 10:20:29
 * @Description: 领券中心 - 新增/编辑/查看领券
 */
 export function receiveCouponInsertOrUpdate(data) {
  return request({
      url: '/coupon/receive/coupon/manage/insertOrUpdate',
      method: 'post',
      data: data
  })
}


/**
 * @Author: 何志祥
 * @Date: 2022-04-06 10:20:29
 * @Description: 领券中心 - 新增/编辑/查看领券 - 优惠券ID
 */
 export function receiveCouponCouponManageList(data) {
  return request({
      url: '/coupon/coupon/manage/list',
      method: 'post',
      data: data
  })
}

/**
 * @Author: 何志祥
 * @Date: 2022-04-06 10:20:29
 * @Description: 领券中心 - 新增/编辑/查看领券 - 优惠券ID
 */
 export function receiveCouponSelectDetail(id) {
  return request({
      url: '/coupon/receive/coupon/manage/selectDetail?id='+id,
      method: 'get'
  })
}

