import request from '@/utils/request'

// 查询定时任务调度列表
export function listJob(query) {
  return request({
    url: '/monitor/crontab/list',
    method: 'get',
    params: query
  })
}

// 查询定时任务调度详细
export function getJob(jobId) {
  return request({
    url: '/monitor/crontab/info/' + jobId,
    method: 'get'
  })
}

// 新增定时任务调度
export function addJob(data) {
  return request({
    url: '/monitor/crontab/add',
    method: 'post',
    data: data
  })
}

// 修改定时任务调度
export function updateJob(data) {
  return request({
    url: '/monitor/crontab/update',
    method: 'post',
    data: data
  })
}

// 任务状态修改
export function changeJobEnable(jobId, jobEnable) {
  return request({
    url: '/monitor/crontab/enable/' + jobId + "/" + jobEnable,
    method: 'get'
  })
}


// 定时任务立即执行一次
export function runJob(jobId) {
  return request({
    url: '/monitor/crontab/run/' + jobId,
    method: 'get'
  })
}