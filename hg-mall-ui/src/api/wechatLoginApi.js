/*
 * @Author: 何志祥
 * @Date: 2021-12-23 15:31:19
 * @LastEditors: 何志祥
 * @LastEditTime: 2021-12-27 10:19:17
 * @Description: 企微登录api
 */
import axios from 'axios'
import moment from "moment";
//
// let base = 'http://localhost:55571';

let base = ''
let loginConnectUrl = 'https://sec.wedengta.com'
// let serviceUrl = "http://privitest.wedengta.com";//测试环境
// let serviceUrl = 'http://privi.wedengta.com'//正式环境
// export const req  uestLogin = params => { return axios.post(`${base}/login`, params).then(res => res.data); };
//
// export const getUserList = params => { return axios.get(`${base}/api/getUserList`, { params: params }); };
//
// export const delUser = para
export default {
  getSession: function (params) {
    return axios.get(`${loginConnectUrl}/conn/` + params.url, {
      timeout: 50000
    }).then(res => res.data).catch(e => e)
  },
  getMessage: function (params) {
    return axios.get(`${loginConnectUrl}/conn/` + params.url, {
      timeout: 50000
    }).then(res => res.data).catch(e => e)
  },

  /*******************************禁言管理***************************/
  // 禁言管理
  getForbidList: function (params) {
    return axios.post(`${base}/api/getForbidList`, params).then(res => res.data).catch(e => e)
  },
  //禁言范围
  getForbidRange: function (params) {
    return axios.post(`${base}/api/getForbidRange`, params).then(res => res.data).catch(e => e)
  },
  //添加禁言
  addForbid: function (params) {
    return axios.post(`${base}/api/addForbid`, params).then(res => res.data).catch(e => e)
  },
  //解除禁言
  deleteForbid: function (params) {
    return axios.post(`${base}/api/deleteForbid`, params).then(res => res.data).catch(e => e)
  },
  /******************************* 互动直播 **********************************/
  // 查询消息
  getLiveMsgList: function (params) {
    return axios.post(`${base}/api/getInteractiveMsgList`, params).then(res => res.data).catch(e => e)
  },
  // 公开消息
  publicMsg: function (params) {
    return axios.post(`${base}/api/publicMsg`, params).then(res => res.data).catch(e => e)
  },
  // 是否存在置顶消息
  topMsgCount: function (params) {
    return axios.post(`${base}/api/topMsgCount`, params).then(res => res.data).catch(e => e)
  },
  // 置顶和取消置顶
  topMsg: function (params) {
    return axios.post(`${base}/api/topMsg`, params).then(res => res.data).catch(e => e)
  },
  // 获取机器人答案
  getAiAnswer: function (params) {
    return axios.post(`${base}/api/getAiAnswer`, params).then(res => res.data).catch(e => e)
  },
  // 发送互动直播消息
  sendInteractiveLiveMsg: function (params) {
    if (localStorage.getItem('currentChannel') == 'all') {
      return axios.post(`${base}/api/sendInteractiveLiveMsgAll`, params).then(res => res.data).catch(e => e)
    } else {
      return axios.post(`${base}/api/sendInteractiveLiveMsg`, params).then(res => res.data).catch(e => e)
    }
  },
  // 互动历史
  getInteractiveHistoryList: function (params) {
    return axios.post(`${base}/api/getInteractiveHistoryList`, params).then(res => res.data).catch(e => e)
  },
  // 删除消息
  deleteInteractiveMsg: function (params) {
    return axios.post(`${base}/api/deleteInteractiveMsg`, params).then(res => res.data).catch(e => e)
  },

  /***************************实时播报-删除消息管理**************************/
  //删除消息管理
  getDeleteMsgList: function (params) {
    return axios.post(`${base}/api/getDeleteMsgList`, params).then(res => res.data).catch(e => e)
  },

  /******************业务员考核相关***************/
  // 最近考核时间
  getLastAssessMonth: function (params) {
    return axios.post(`${base}/api/getLastAssessMonth`, params, {
      timeout: 5000
    }).then(res => res.data).catch(e => e)
  },
  // 业绩考核列表数据
  getAssessList: function (params) {
    params.timeout = 5000
    return axios.post(`${base}/api/listAssessRecord`, params).then(res => res.data).catch(e => e)
  },
  // 考核记录
  getAssessRecordList: function (params) {
    params.timeout = 5000
    return axios.post(`${base}/api/listOneCircleHistory`, params).then(res => res.data).catch(e => e)
  },
  // 查询圈子打分占比
  getScorePercentage: function (params) {
    params.timeout = 5000
    return axios.post(`${base}/api/queryOneCircleScoreDetail`, params).then(res => res.data).catch(e => e)
  },
  // 修改圈子打分占比
  modifyScorePercentage: function (params) {
    params.timeout = 5000
    return axios.post(`${base}/api/modifyOneCircleScore`, params).then(res => res.data).catch(e => e)
  },
  // 修改跟踪平分
  modifyContinueScore: function (params) {
    params.timeout = 5000
    return axios.post(`${base}/api/modifyAssessRecord`, params).then(res => res.data).catch(e => e)
  },
  // 保存业绩考核
  saveAssess: function (params) {
    params.timeout = 5000
    return axios.post(`${base}/api/addAssessRecord`, params).then(res => res.data).catch(e => e)
  },
  // 计算总分
  caculateTotalScore: function (params) {
    params.timeout = 5000
    return axios.post(`${base}/api/caculateTotalScore`, params).then(res => res.data).catch(e => e)
  },
  //获取投顾角色的圈子
  getUserAdvisorList: function (params) {
    params.timeout = 5000
    return axios.post(`${base}/api/getUserAdvisorList`, params).then(res => res.data).catch(e => e)
  },

  //改造后的接口
  getUserPermission: function (params) {
    return axios.post(`${base}/api/getUserPermission`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 获取角色列表
   * @param {*} params 
   */
  getAllRolesList: function (params) {
    return axios.post(`${base}/api/getAllRolesList`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 获取角色权限（编辑的时候用）
   * @param {*} params 
   */
  getRolePermissionList: function (params) {
    return axios.post(`${base}/api/getRolePermissionList`, params).then(res => res.data).catch(e => e)
  },
  /**
   *删除角色（编辑的时候用）
   * @param {*} params 
   */
  deleteRole: function (params) {
    return axios.post(`${base}/api/deleteRole`, params).then(res => res.data).catch(e => e)
  },
  /**
   *新增角色（编辑的时候用）
   * @param {*} params 
   */
  addRole: function (params) {
    return axios.post(`${base}/api/addRole`, params).then(res => res.data).catch(e => e)
  },
  /**
   *修改角色
   * @param {*} params 
   */
  updateRole: function (params) {
    return axios.post(`${base}/api/updateRole`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 获取用户列表
   * @param {*} params 
   */
  getAllUserList: function (params) {
    return axios.post(`${base}/api/getAllUserList`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 添加用户
   * @param {*} params 
   */
  addNewUser: function (params) {
    return axios.post(`${base}/api/addNewUser`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 添加用户
   * @param {*} params 
   */
  updateUser: function (params) {
    return axios.post(`${base}/api/updateUser`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 删除用户
   * @param {*} params 
   */
  deleteUser: function (params) {
    return axios.post(`${base}/api/deleteUser`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 根据id查询用户权限(用于用户编辑页面权限显示)
   * @param {*} params 
   */
  getEditUserPermission: function (params) {
    return axios.post(`${base}/api/getEditUserPermission`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 编辑用户权限
   * @param {*} params 
   */
  updateUserPermission: function (params) {
    return axios.post(`${base}/api/updateUserPermission`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 获取全部权限列表
   * @param {*} params 
   */
  getPermissionList: function (params) {
    return axios.post(`${base}/api/getPermissionList`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 删除节点
   * @param {*} params 
   */
  deletePermission: function (params) {
    return axios.post(`${base}/api/deletePermission`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 新增节点
   * @param {*} params 
   */
  addPermission: function (params) {
    return axios.post(`${base}/api/addPermission`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 修改节点
   * @param {*} params 
   */
  editPermission: function (params) {
    return axios.post(`${base}/api/editPermission`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 根据用户id获取用户信息
   * @param {*} params 
   */
  getUserInfo: function (params) {
    return axios.post(`${base}/api/getUserInfo`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 根据用户id获取用户信息
   * @param {*} params 
   */
  getUserToken: function (params) {
    return axios.post(`${base}/api/getUserToken`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 根据企业QQ获取用户信息
   * @param {*} params 
   */
  getUserInfoByQQ: function (params) {
    return axios.post(`${base}/api/getUserInfoByQQ`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 根据企业微信获取用户信息
   * @param {*} params 
   */
  getUserInfoByWechatId: function (params) {
    return axios.post(`${base}/api/getUserInfoByWechatId`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 根据企业QQ获取用户信息
   * @param {*} params 
   */
  changePwd: function (params) {
    return axios.post(`${base}/api/changePwd`, params).then(res => res.data).catch(e => e)
  },

  /**** *******************cas相关接口**********/
  /**
   * 获取用户
   */
  getPagingUserList: function (params) {
    return axios.post(`${base}/api/getPagingUserList`, params).then(res => res.data).catch(e => e)
  },
  /**
   * 获取用户
   */
  getPagingUserListWechat: function (params) {
    return axios.post(`${base}/api/getPagingUserListWechat`, params).then(res => res.data).catch(e => e)
  },

  /**
   * 判断是否登录
   * @param {*} params 
   */
  hasLogin: function (params) {
    return axios.post(`${base}/api/hasLogin`, params).then(res => res.data).catch(e => e)
  },

  /**
   * 获取重定向地址
   * @param {*} params 
   */
  redirectLogin: function (params) {
    return axios.post(`${base}/api/redirectLogin`, params).then(res => res.data).catch(e => e)
  },

  /**
   * 登录回调地址
   * @param {*} params 
   */
  loginCallback: function (params) {
    return axios.post(`${base}/api/loginCallback`, params).then(res => res.data).catch(e => e)
  },

  /**
   * 绑定用户关系
   * @param {*} params 
   */
  bindUser: function (params) {
    return axios.post(`${base}/api/bindUser`, params).then(res => res.data).catch(e => e)
  },

  /**
   * 退出登录
   * @param {*} params 
   */
  casLogout: function (params) {
    return axios.post(`${base}/api/casLogout`, params).then(res => res.data).catch(e => e)
  },

  /**
   * 验签
   * @param {*} params 
   */
  signCheck: function (params) {
    return axios.post(`${base}/api/signCheck`, params).then(res => res.data).catch(e => e)
  },

  /*************************************客服分配规则***************************/
  /**
   * 获取客服分配规则列表
   * @param {*} params 
   */
  selectCustomerServiceList: function (params) {
    return axios.post(`${base}/api/selectCustomerServiceList`, params).then(res => res.data).catch(e => e)
  },

  /**
   * 更新客服分配规则列表
   * @param {*} params 
   */
  updateCustomerService: function (params) {
    return axios.post(`${base}/api/updateCustomerService`, params).then(res => res.data).catch(e => e)
  },

  /**
   * 结束回话
   * @param {*} params 
   */
  endchat: function (params) {
    return axios.post(`${base}/api/endchat`, params).then(res => res.data).catch(e => e)
  },

  /**
   * 添加操作日志
   */
  addOperatorLog: function (params) {
    params.sys = "adviser";
    params.ip = "";
    params.create_time = moment(new Date()).format("YYYYMMDDHHmmss");
    return axios.post(`${base}/api/addOperatorLog`, params).then(res => res.data).catch(e => e)
  },

  /**
   * 获取一级渠道
   * @param {*} params 
   */
  getTenantIds: function (params) {
    return axios.post(`${base}/api/getTenantIds`, params).then(res => res.data).catch(e => e)
  },

  /**
   * 获取二级渠道
   * @param {*} params 
   */
  getPlatforms: function (params) {
    return axios.post(`${base}/api/getPlatforms`, params).then(res => res.data).catch(e => e)
  },
}
