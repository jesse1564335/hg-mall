

import Vue from 'vue'

import Cookies from 'js-cookie'

import Element from 'element-ui'
import './assets/styles/element-variables.scss'

import '@/assets/styles/index.scss' // global css
import '@/assets/styles/hydrus.scss'
import App from './App'
import store from './store'
import router from './router'
import permission from './directive/permission'

import './assets/icons' // icon
import './permission' // permission control
import { getDicts } from "@/api/system/dict/data";
import { getConfigKey } from "@/api/system/config";
import { parseTime, resetForm, addDateRange, selectDictLabel, selectDictLabels, download, handleTree } from "@/utils/hydrus";
import Pagination from "@/components/Pagination";
// 自定义表格工具扩展
import RightToolbar from "@/components/RightToolbar"
// 页面高度控制
import PageContent from '@/components/pageContent'
// 动态表格组件(全局)
import DyTable from '@/components/DynamicTable/dyTable'
import DySearch from '@/components/DynamicTable/dySearch'
import DySearchTable from '@/components/DynamicTable/dySearchTable'

// 代码高亮插件
import hljs from 'highlight.js'
import 'highlight.js/styles/github-gist.css'


// 全局方法挂载
Vue.prototype.getDicts = getDicts
Vue.prototype.getConfigKey = getConfigKey
Vue.prototype.parseTime = parseTime
Vue.prototype.resetForm = resetForm
Vue.prototype.addDateRange = addDateRange
Vue.prototype.selectDictLabel = selectDictLabel
Vue.prototype.selectDictLabels = selectDictLabels
Vue.prototype.download = download
Vue.prototype.handleTree = handleTree

import {optionalChaining} from "./utils/commonMethods";
Vue.prototype.$$ = optionalChaining;


// ====== 企微登录包 begin
const casConfig = require('@/utils/wechatLogin/defConfig')('cas');
Vue.prototype.casConfig = casConfig
const defConfig = require('@/utils/wechatLogin/defConfig')('req');
Vue.prototype.defConfig = defConfig

const Cas = require('@/utils/login/Cas.js');
 Vue.prototype.Cas = Cas
const ICache = require('@/utils/login/ICache.js');
Vue.prototype.ICache = ICache
const ISession = require('@/utils/login/ISession.js');
Vue.prototype.ISession = ISession
const Helper = require('@/utils/login/Helper.js');
Vue.prototype.Helper = Helper
// var CasException = require('../src/common/login/CasException.js');
const cas = new Cas(new ICache(), new ISession(), casConfig.casUrl, casConfig.systemId, new Helper());
Vue.prototype.cas = cas
cas.setSignKey(casConfig.signKey)

// 设为全局变量，因为request.js需要用到这两个，此时不能重新 new 一个 cas，不然两份内存地址是不一样的
window.defConfig = defConfig;
window.cas = cas;
// ====== 企微登录包 end



Vue.prototype.msgSuccess = function (msg) {
  this.$message({ showClose: true, message: msg, type: "success" });
}

Vue.prototype.msgError = function (msg) {
  this.$message({ showClose: true, message: msg, type: "error" });
}

Vue.prototype.msgInfo = function (msg) {
  this.$message.info(msg);
}

// 全局组件挂载
Vue.component('Pagination', Pagination)
Vue.component('RightToolbar', RightToolbar)
Vue.component('PageContent', PageContent)
Vue.component('DyTable', DyTable)
Vue.component('DySearch', DySearch)
Vue.component('DySearchTable', DySearchTable)


Vue.use(permission)
Vue.use(hljs.vuePlugin);

// 拖拽排序插件
import VueDND from 'awe-dnd';
Vue.use(VueDND);

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */

Vue.use(Element, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
})

Vue.config.productionTip = false

let context = new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
export default context;
