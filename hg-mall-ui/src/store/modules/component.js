/*
 * @Author: 何志祥
 * @Date: 2021-11-01 10:04:44
 * @LastEditors: 何志祥
 * @LastEditTime: 2021-11-09 13:53:41
 * @Description: 组件级数据储存
 */
const modulesComponent = {
    state: {
        // 统计系统 - 订单业绩 下的共享数据
        orderPerformance: {
            orderId: '', // 订单列表进入详情需传入的id
        },
        // 统计系统 - 订单业绩 导出相关字段(因为导出是前端自己做)
        exportSet: {
            // 默认表头相关字段
            exportHeader: [
                "关联主订单",
                "是否最后一笔支付",
                "客户姓名", 
                "用户编号", 
                "订单编号", 
                "网易CID", 
                "产品名称", 
                "订单状态", 
                "支付时间", 
                "支付方式", 
                "实付金额", 
                "订单金额", 
                "产品价格", 
                "支付流水号", 
                "产品版本", 
                "服务期限(天)", 
                "权限开始时间", 
                "权限结束时间", 
                "成单业务", 
                "成单经理", 
                "成单总监", 
                "底单业务", 
                "底单经理", 
                "底单总监", 
                "签约时间", 
                "订单单量", 
                "是否大额支付", 
                "协议编号", 
                "产品风险等级", 
                "客户风险等级", 
                "线上ID", 
                "手机号码", 
                "来源", 
                "订单类型", 
                "产品ID", 
                "订单创建时间", 
                "投放渠道", 
                "投放页面", 
                "资源录入时间",
            ],
            // 默认表头相关字段对应的key
            exportHeaderKey: [
                "associatedOrder",
                "isLastpay",
                "realName",
                "userNumber",
                "orderId",
                "cid",
                "productName",
                "orderStatus",
                "payTime",
                "payChannel",
                "realPayMoney",
                "orderAmount",
                "totalMoney",
                "tradeNo",
                "priceConfig",
                "serviceTerm",
                "beginTime",
                "endTime",
                "businessManager",
                "manager",
                "chiefInspector",
                "firstBusinessManagerName",
                "firstManagerName",
                "firstChiefInspectorName",
                "signTime",
                "isOrder",
                "isBigpay",
                "protocolNum",
                "productLevel",
                "userRiskType",
                "accountId",
                "phoneNum",
                "source",
                "renewOrder",
                "productCode",
                "createTime",
                "marketChannel",
                "marketPage",
                "resourceCreateTime"
            ],
            // 默认导出字段选中的状态
            exportChecked: [],
            // 最终需要导出的字段
            finallyExportHeader: [],
            // 最终表头相关字段对应的key
            finallyExportHeaderKey: [],
            // 最终相关字段对应的checked选中状态
            finallyExportChecked: []
        }
    },
    mutations: {
        /**
         * @Author: 何志祥
         * @Date: 2021-11-02 13:33:39
         * @Description: 改变订单id方法
         * @param {*} state
         * @param {*} orderId
         */        
        orderIdChange (state, orderId) {
            state.orderPerformance.orderId = orderId;
        },

        /**
         * @Author: 何志祥
         * @Date: 2021-11-02 13:36:18
         * @Description: exportChecked初始化
         * @param {*} state
         * @param {*} list 数组
         */        
        exportCheckedInitChange (state, list) {
            state.exportSet.exportChecked = list;
        },

        /**
         * @Author: 何志祥
         * @Date: 2021-11-03 09:51:57
         * @Description: 生成最终需要导出的数据表头
         * @param {*} state
         * @param {*} list 数组
         */        
        finallyExportHeaderChange (state, list) {
            state.exportSet.finallyExportHeader = list;
        },

        /**
         * @Author: 何志祥
         * @Date: 2021-11-03 09:51:57
         * @Description: 生成最终需要导出的数据表头key值
         * @param {*} state
         * @param {*} list 数组
         */        
        finallyExportHeaderKeyChange (state, list) {
            state.exportSet.finallyExportHeaderKey = list;
        },

        /**
         * @Author: 何志祥
         * @Date: 2021-11-03 09:51:57
         * @Description: 生成最终需要导出的数据表头key值
         * @param {*} state
         * @param {*} list 数组
         */        
        finallyExportCheckedChange (state, list) {
            state.exportSet.finallyExportChecked = list;
        }
    }
}

export default modulesComponent;
