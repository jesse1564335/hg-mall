## 开发

```bash

# 进入项目目录
cd hydrus-ui

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 http://localhost:80

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 质检系统开发

> 工作备忘

* [java接口文档地址](http://47.98.111.219:8080/mock/?wework_cfm_code=OA0LHkbuAq9S2JWDskb0qgxuKJpcV2%2F0LmnWkLBtf6HgZYLXmzU3V45kPpW8zJOrLigp4Fg4XOddVVaVLUzRlHZ29aN4sMcYPjcaO0wD9H9gVzGQOTlW1y6FLbusNZ1ZDw%3D%3D#/list/60cffb0a86c7db0841076654)

> jenkins

* [任务名：hydrus-ui](http://116.62.135.43:8080/jenkins/job/hydrus-ui)
