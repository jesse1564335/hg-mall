<?php
/**
 * Class FileTypeConstant
 * @package App\Constants
 * Desc:
 * created by: lhw at 2022/8/17 10:26
 */

namespace App\Constants;


class FileTypeConstant
{
  public const IMG_ALLOW_TYPE = ['image/jpg', 'image/jpeg', 'image/png', 'image/pjpeg', 'image/gif', 'image/bmp', 'image/x-png'];
  public const MIME_TO_EXT = [
      'image/jpg' => 'jpg',
      'image/jpeg' => 'jpeg',
      'image/png' => 'png',
      'image/pjpeg' => 'jpeg',
      'image/gif' => 'gif',
      'image/bmp' => 'bmp',
      'image/x-png' => 'png'
  ];
}