<?php

namespace App\Services\Impl;

use App\Constants\FileTypeConstant;
use App\Exception\BusinessException;
use App\Services\UploadFileService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Filesystem\FilesystemFactory;
use Hyperf\HttpMessage\Upload\UploadedFile;
use Hyperf\RpcServer\Annotation\RpcService;
use Hyperf\Utils\Str;
use League\Flysystem\FilesystemException;

/**
 * @RpcService(name="FileUploadRpcService", protocol="jsonrpc-http", server="jsonrpc-http", publishTo="nacos")
 * Class UploadFileServiceImpl
 * created by: lhq at 2022/8/14 15:58
 */
class UploadFileServiceImpl implements UploadFileService
{

    /**
     * @Inject()
     * @var FilesystemFactory
     */
    private FilesystemFactory $factory;


    public function uploadImage(UploadedFile $file, string $driverName = "local", string $categoryPath = ""): array {
        $driver = $this->factory->get($driverName);
        $mime = strtolower($file->getMimeType());
        if (!in_array($mime, FileTypeConstant::IMG_ALLOW_TYPE)) {
            throw new BusinessException(403, '上传格式不允许');
        }
        $ext = FileTypeConstant::MIME_TO_EXT[$mime];
        $name = Str::random(16) . "." . $ext;
        if ($categoryPath == "") {
            $folderPath =  date('Ymd') . '/' . $name;
        } else {
            $folderPath =  date('Ymd') . "/$categoryPath/" . $name;
        }
        $contents = fopen($file->getRealPath(), "r+");
        try {
            $driver->writeStream($folderPath, $contents);
        } catch (FilesystemException $e) {
            throw new BusinessException($e->getCode(), $e->getMessage());
        }
        $path = "";
        switch ($driverName) {
            case "local":
                $path = "/".$folderPath;
                break;
            default:
                break;
        }
        return [$name, $path];
    }

    public function uploadImageByRpc(array $params): string {
        return "";
    }
}