<?php

namespace App\Services;

use Hyperf\HttpMessage\Upload\UploadedFile;

interface UploadFileService
{
    /**
     * desc: 图片上传
     * @param UploadedFile $file
     * @param string $driverName
     * created by: lhw at 2022/8/14 15:42
     * @param string $categoryPath
     * @return array
     */
    public function uploadImage(UploadedFile $file, string $driverName = "local", string $categoryPath = ""): array;

    /**
     * desc:远程调用上传图片
     * @param array $params
     * created by: lhw at 2022/9/8 16:41
     * @return string
     */
    public function uploadImageByRpc(array $params): string;
}