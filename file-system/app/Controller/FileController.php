<?php

namespace App\Controller;

use App\Constants\ErrorCode;
use App\Domain\Request\UploadFileReq;
use App\Services\UploadFileService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\PostMapping;
use Psr\Http\Message\ResponseInterface;

/**
 * @Controller(prefix="/file/system")
 * Class FileController
 * @package App\Controller
 * created by: lhw at 2022/8/16 10:54
 */
class FileController extends AbstractController
{
    /**
     * @Inject()
     * @var UploadFileService
     */
    private UploadFileService $fileService;

    /**
     * @PostMapping(path="img")
     * desc: 图片上传
     * @param UploadFileReq $req
     * created by: lhw at 2022/8/16 10:59
     * @return ResponseInterface
     */
    public function uploadImage(UploadFileReq $req): ResponseInterface
    {
        if (!$req->hasFile("file")) {
            return $this->response->json(["code"=>500, "message"=>"文件不存在"]);
        }
        $file = $req->file("file");
        $driveName = $req->input("driver", "local");
        $categoryPath = $req->input("category", "");
        $result = $this->fileService->uploadImage($file, $driveName, $categoryPath);
        if (empty($result)) {
            return $this->response->fail(ErrorCode::SERVER_ERROR);
        }
        $urlInfo = $req->getUri();
        $path = "http://". $urlInfo->getHost() . ":" . $urlInfo->getPort() . $result[1];
        return $this->response->success(["name"=>$result[0], "url"=>$path]);
    }
}