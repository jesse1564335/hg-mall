<?php
/**
 * Class UploadFileReq
 * @package App\Domain\Request
 * Desc:
 * created by: lhw at 2022/8/15 16:48
 */

namespace App\Domain\Request;


use Hyperf\Validation\Request\FormRequest;

class UploadFileReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'file' => 'required|file',
            'driver' => 'string|nullable',
            'category' => 'string|nullable'
        ];
    }

    public function messages(): array
    {
        return [
            'file.required' => '缺少上传文件'
        ];
    }
}