<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
use Hyperf\ConfigCenter\Mode;

return [
    'enable' => (bool) env('CONFIG_CENTER_ENABLE', true),
    'driver' => env('CONFIG_CENTER_DRIVER', 'nacos'),
    'mode' => env('CONFIG_CENTER_MODE', Mode::PROCESS),
    'drivers' => [
        'nacos' => [
            'driver' => Hyperf\ConfigNacos\NacosDriver::class,
            'merge_mode' => Hyperf\ConfigNacos\Constants::CONFIG_MERGE_OVERWRITE,
            'interval' => 3,
            'default_key' => 'nacos_config',
            'listener_config' => [
                // dataId, group, tenant, type, content
//                'nacos_config.data' => [
//                    'tenant' => 'tenant', // corresponding with service.namespaceId
//                    'data_id' => '',
//                    'group' => 'DEFAULT_GROUP',
//                ],
                'nacos_config' => [
                    'tenant' => env('NACOS_CONFIG_DATA_TENANT', ''),
                    'data_id' => env('NACOS_CONFIG_DATA_DATAID', ''),
                    'group' => env('NACOS_CONFIG_DATA_GROUP', 'DEFAULT_GROUP'),
                    'type' => 'json',
                ],
            ],
            'client' => [
                // nacos server url like https://nacos.hyperf.io, Priority is higher than host:port
                'uri' => '',
                'host' => env('NACOS_HOST', '127.0.0.1'),
                'port' => env('NACOS_PORT', 8848),
                'username' => env('NACOS_USERNAME', 'nacos'),
                'password' => env('NACOS_PASSWORD', 'nacos'),
                'guzzle' => [
                    'config' => null,
                ],
            ],
        ],
    ],
];
