<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

use App\Services\Impl\UploadFileServiceImpl;
use App\Services\UploadFileService;

return [
    Hyperf\Contract\StdoutLoggerInterface::class => App\Common\Log\LoggerFactory::class,
    Hyperf\Server\Listener\AfterWorkerStartListener::class => App\Common\Http\WorkerStartListener::class,
    UploadFileService::class => UploadFileServiceImpl::class
];
