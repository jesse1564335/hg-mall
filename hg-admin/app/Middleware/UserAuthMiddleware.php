<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Services\UserAuth;
use Hyperf\Utils\Context;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class UserAuthMiddleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * 白名单：跳过token验证
     * @var string[]
     */
    private $whiteList = ["/sys/login", "/sys/captchaImg", "/sys/logout"];


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        //运行跨域访问设置
        $response = Context::get(ResponseInterface::class);
        $response = $response->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Credentials', 'true')
            ->withHeader('Access-Control-Allow-Methods','GET,POST,OPTIONS,PATCH,DELETE')
            ->withHeader('Access-Control-Allow-Headers', 'uid,x-token,Keep-Alive,User-Agent,Cache-Control,Content-Type,Authorization');

        Context::set(ResponseInterface::class, $response);

        if ($request->getMethod() == 'OPTIONS') {
            return $response;
        }
        $path = $request->getUri()->getPath();
        if (in_array($path, $this->whiteList)) {
            return $handler->handle($request);
        }
        //token刷新
        $token = $request->getHeaderLine(UserAuth::AUTH_TOKEN);
        if (!empty($token)) {
            UserAuth::instance()->reload($token);
        } else {
            throw new BusinessException(ErrorCode::INVALID_TOKEN, ErrorCode::getMessage(ErrorCode::INVALID_TOKEN));
        }
        $body = $request->getParsedBody();
        if (!empty($body)) {
            $user = UserAuth::instance()->getSysUser()->toArray();
            $body["currentUser"] = $user["name"] ?? "";
            $body["currentUserId"] = $user["id"] ?? "";
            $request = $request->withParsedBody($body);
        }
        return $handler->handle($request);
    }
}
