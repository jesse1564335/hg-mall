<?php

namespace App\Model;

/**
 * Class SysCrontab
 * created by: lhw at 2022/8/21 19:47
 * @property integer $job_id 主键ID
 * @property string $job_name 任务名称
 * @property string $job_rule 执行规则
 * @property string $job_callback 执行回调方法
 * @property string $job_memo 任务备注
 * @property integer $job_enable 是否生效 0-否 1-生效
 * @property string $job_type 任务类型 callback和command
 * @property integer $singleton 是否并发执行 1-只会同时执行一个 0-否
 * @property integer $onOneServer '1-多实例部署项目时，则只有一个实例会被触发 0-否
 * @property string $mutexPool 互斥锁连接池
 * @property integer $mutexExpires 互斥锁超时时间(秒)
 * @property string $create_by
 * @property string $create_time
 * @property string $update_by
 * @property string $update_time
 */
class SysCrontab extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sys_crontab';

    protected $primaryKey = 'job_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * @return int
     */
    public function getJobId(): int
    {
        return $this->job_id;
    }

    /**
     * @param int $job_id
     */
    public function setJobId(int $job_id): void
    {
        $this->job_id = $job_id;
    }

    /**
     * @return string
     */
    public function getJobName(): string
    {
        return $this->job_name;
    }

    /**
     * @param string $job_name
     */
    public function setJobName(string $job_name): void
    {
        $this->job_name = $job_name;
    }

    /**
     * @return string
     */
    public function getJobRule(): string
    {
        return $this->job_rule;
    }

    /**
     * @param string $job_rule
     */
    public function setJobRule(string $job_rule): void
    {
        $this->job_rule = $job_rule;
    }

    /**
     * @return string
     */
    public function getJobCallback(): string
    {
        return $this->job_callback;
    }

    /**
     * @param string $job_callback
     */
    public function setJobCallback(string $job_callback): void
    {
        $this->job_callback = $job_callback;
    }

    /**
     * @return ?string
     */
    public function getJobMemo(): ?string
    {
        return $this->job_memo;
    }

    /**
     * @param ?string $job_memo
     */
    public function setJobMemo(?string $job_memo): void
    {
        $this->job_memo = $job_memo;
    }

    /**
     * @return int
     */
    public function getJobEnable(): int
    {
        return $this->job_enable;
    }

    /**
     * @param int $job_enable
     */
    public function setJobEnable(int $job_enable): void
    {
        $this->job_enable = $job_enable;
    }

    /**
     * @return string
     */
    public function getJobType(): string
    {
        return $this->job_type;
    }

    /**
     * @param string $job_type
     */
    public function setJobType(string $job_type): void
    {
        $this->job_type = $job_type;
    }

    /**
     * @return ?int
     */
    public function getSingleton(): ?int
    {
        return $this->singleton;
    }

    /**
     * @param ?int $singleton
     */
    public function setSingleton(?int $singleton): void
    {
        $this->singleton = $singleton;
    }

    /**
     * @return ?int
     */
    public function getOnOneServer(): ?int
    {
        return $this->onOneServer;
    }

    /**
     * @param ?int $onOneServer
     */
    public function setOnOneServer(?int $onOneServer): void
    {
        $this->onOneServer = $onOneServer;
    }

    /**
     * @return ?string
     */
    public function getMutexPool(): ?string
    {
        return $this->mutexPool;
    }

    /**
     * @param ?string $mutexPool
     */
    public function setMutexPool(?string $mutexPool): void
    {
        $this->mutexPool = $mutexPool;
    }

    /**
     * @return ?int
     */
    public function getMutexExpires(): ?int
    {
        return $this->mutexExpires;
    }

    /**
     * @param ?int $mutexExpires
     */
    public function setMutexExpires(?int $mutexExpires): void
    {
        $this->mutexExpires = $mutexExpires;
    }

    /**
     * @return ?string
     */
    public function getCreateBy(): ?string
    {
        return $this->create_by;
    }

    /**
     * @param ?string $create_by
     */
    public function setCreateBy(?string $create_by): void
    {
        $this->create_by = $create_by;
    }

    /**
     * @return ?string
     */
    public function getCreateTime(): ?string
    {
        return $this->create_time;
    }

    /**
     * @param ?string $create_time
     */
    public function setCreateTime(?string $create_time): void
    {
        $this->create_time = $create_time;
    }

    /**
     * @return ?string
     */
    public function getUpdateBy(): ?string
    {
        return $this->update_by;
    }

    /**
     * @param ?string $update_by
     */
    public function setUpdateBy(?string $update_by): void
    {
        $this->update_by = $update_by;
    }

    /**
     * @return ?string
     */
    public function getUpdateTime(): ?string
    {
        return $this->update_time;
    }

    /**
     * @param ?string $update_time
     */
    public function setUpdateTime(?string $update_time): void
    {
        $this->update_time = $update_time;
    }


}