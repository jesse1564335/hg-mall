<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property integer $menu_id
 * @property integer $role_id
 */
class SysRoleHasMenu extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sys_role_has_menus';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ["role_id", "menu_id"];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * @return array
     */
    public function getGuarded(): array
    {
        return $this->guarded;
    }

    /**
     * @param array $guarded
     */
    public function setGuarded(array $guarded): void
    {
        $this->guarded = $guarded;
    }

    /**
     * @return int
     */
    public function getMenuId(): int
    {
        return $this->menu_id;
    }

    /**
     * @param int $menu_id
     */
    public function setMenuId(int $menu_id): void
    {
        $this->menu_id = $menu_id;
    }

    /**
     * @return int
     */
    public function getRoleId(): int
    {
        return $this->role_id;
    }

    /**
     * @param int $role_id
     */
    public function setRoleId(int $role_id): void
    {
        $this->role_id = $role_id;
    }


}