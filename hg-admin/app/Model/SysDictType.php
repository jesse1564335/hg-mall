<?php

declare (strict_types=1);
namespace App\Model;


/**
 * @property integer $dict_id 数据字典表主键ID
 * @property string $dict_name 字典名称
 * @property string $dict_type 字典类型
 * @property integer $status 状态 '1-正常 0-停用'
 * @property string $create_time
 * @property string $create_by
 * @property string $update_by
 * @property string $update_time
 * @property string $remark
 */
class SysDictType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sys_dict_type';

    protected $primaryKey = 'dict_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];


    /**
     * @return int
     */
    public function getDictId(): int
    {
        return $this->dict_id;
    }

    /**
     * @param int $dict_id
     */
    public function setDictId(int $dict_id): void
    {
        $this->dict_id = $dict_id;
    }

    /**
     * @return string
     */
    public function getDictName(): string
    {
        return $this->dict_name;
    }

    /**
     * @param string $dict_name
     */
    public function setDictName(string $dict_name): void
    {
        $this->dict_name = $dict_name;
    }

    /**
     * @return string
     */
    public function getDictType(): string
    {
        return $this->dict_type;
    }

    /**
     * @param string $dict_type
     */
    public function setDictType(string $dict_type): void
    {
        $this->dict_type = $dict_type;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCreateTime(): string
    {
        return $this->create_time;
    }

    /**
     * @param string $create_time
     */
    public function setCreateTime(string $create_time): void
    {
        $this->create_time = $create_time;
    }

    /**
     * @return string
     */
    public function getCreateBy(): string
    {
        return $this->create_by;
    }

    /**
     * @param string $create_by
     */
    public function setCreateBy(string $create_by): void
    {
        $this->create_by = $create_by;
    }

    /**
     * @return string
     */
    public function getUpdateBy(): string
    {
        return $this->update_by;
    }

    /**
     * @param string $update_by
     */
    public function setUpdateBy(string $update_by): void
    {
        $this->update_by = $update_by;
    }

    /**
     * @return string
     */
    public function getUpdateTime(): string
    {
        return $this->update_time;
    }

    /**
     * @param string $update_time
     */
    public function setUpdateTime(string $update_time): void
    {
        $this->update_time = $update_time;
    }

    /**
     * @return string
     */
    public function getRemark(): string
    {
        return $this->remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark(string $remark): void
    {
        $this->remark = $remark;
    }



}