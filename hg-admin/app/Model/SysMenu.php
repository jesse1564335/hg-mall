<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property integer $menu_id 菜单ID
 * @property string $menu_name 菜单名称
 * @property integer $parent_id 父菜单ID
 * @property integer $sort 显示顺序
 * @property string $path 路由地址
 * @property string $component 组件地址
 * @property integer $is_frame 是否为外链(0-否 1-是)
 * @property string $menu_type 菜单类型 (M目录 C菜单 F按钮）
 * @property integer $is_visible 菜单状态(1显示 0隐藏)
 * @property string $status 菜单状态（1正常 0停用）
 * @property string $perms 权限标识
 * @property string $icon 菜单图标
 * @property string $create_by 创建人
 * @property string $create_time 创建时间
 * @property string $update_by 修改人
 * @property string $update_time 修改时间
 * @property string $remark 备注
 * @property array $children 子菜单
 */
class SysMenu extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sys_menus';

    protected $primaryKey = 'menu_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * @return int
     */
    public function getMenuId(): int
    {
        return $this->menu_id;
    }

    /**
     * @param int $menu_id
     */
    public function setMenuId(int $menu_id): void
    {
        $this->menu_id = $menu_id;
    }

    /**
     * @return string
     */
    public function getMenuName(): string
    {
        return $this->menu_name;
    }

    /**
     * @param string $menu_name
     */
    public function setMenuName(string $menu_name): void
    {
        $this->menu_name = $menu_name;
    }

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->parent_id;
    }

    /**
     * @param int $parent_id
     */
    public function setParentId(int $parent_id): void
    {
        $this->parent_id = $parent_id;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort(int $sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return ?string
     */
    public function getComponent(): ?string
    {
        return $this->component;
    }

    /**
     * @param string $component
     */
    public function setComponent(string $component): void
    {
        $this->component = $component;
    }

    /**
     * @return int
     */
    public function getIsFrame(): int
    {
        return $this->is_frame;
    }

    /**
     * @param int $is_frame
     */
    public function setIsFrame(int $is_frame): void
    {
        $this->is_frame = $is_frame;
    }

    /**
     * @return string
     */
    public function getMenuType(): string
    {
        return $this->menu_type;
    }

    /**
     * @param string $menu_type
     */
    public function setMenuType(string $menu_type): void
    {
        $this->menu_type = $menu_type;
    }

    /**
     * @return integer
     */
    public function getIsVisible(): int
    {
        return $this->is_visible;
    }

    /**
     * @param integer $isVisible
     */
    public function setIsVisible(int $isVisible): void
    {
        $this->is_visible = $isVisible;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getPerms(): string
    {
        return $this->perms;
    }

    /**
     * @param string $perms
     */
    public function setPerms(string $perms): void
    {
        $this->perms = $perms;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon(string $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * @return string
     */
    public function getCreateBy(): string
    {
        return $this->create_by;
    }

    /**
     * @param string $create_by
     */
    public function setCreateBy(string $create_by): void
    {
        $this->create_by = $create_by;
    }

    /**
     * @return string
     */
    public function getCreateTime(): string
    {
        return $this->create_time;
    }

    /**
     * @param string $create_time
     */
    public function setCreateTime(string $create_time): void
    {
        $this->create_time = $create_time;
    }

    /**
     * @return string
     */
    public function getUpdateBy(): string
    {
        return $this->update_by;
    }

    /**
     * @param string $update_by
     */
    public function setUpdateBy(string $update_by): void
    {
        $this->update_by = $update_by;
    }

    /**
     * @return string
     */
    public function getUpdateTime(): string
    {
        return $this->update_time;
    }

    /**
     * @param string $update_time
     */
    public function setUpdateTime(string $update_time): void
    {
        $this->update_time = $update_time;
    }

    /**
     * @return string
     */
    public function getRemark(): string
    {
        return $this->remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark(string $remark): void
    {
        $this->remark = $remark;
    }

    /**
     * @return ?array
     */
    public function getChildren(): ?array
    {
        return $this->children;
    }

    /**
     * @param array $children
     */
    public function setChildren(array $children): void
    {
        $this->children = $children;
    }



}
