<?php

declare (strict_types=1);
namespace App\Model;


/**
 * @property integer $config_id
 * @property string $config_name 参数名称
 * @property string $config_key 参数键名
 * @property string $config_value 参数键值
 * @property string $config_type 系统内置（Y是 N否）
 * @property string $create_by 创建者
 * @property string $create_time
 * @property string $update_by 更新者
 * @property string $update_time
 * @property string $remark
 */
class SysConfig extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sys_config';

    protected $primaryKey = 'config_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * @return int
     */
    public function getConfigId(): int
    {
        return $this->config_id;
    }

    /**
     * @param int $config_id
     */
    public function setConfigId(int $config_id): void
    {
        $this->config_id = $config_id;
    }

    /**
     * @return string
     */
    public function getConfigName(): string
    {
        return $this->config_name;
    }

    /**
     * @param string $config_name
     */
    public function setConfigName(string $config_name): void
    {
        $this->config_name = $config_name;
    }

    /**
     * @return string
     */
    public function getConfigKey(): string
    {
        return $this->config_key;
    }

    /**
     * @param string $config_key
     */
    public function setConfigKey(string $config_key): void
    {
        $this->config_key = $config_key;
    }

    /**
     * @return string
     */
    public function getConfigValue(): string
    {
        return $this->config_value;
    }

    /**
     * @param string $config_value
     */
    public function setConfigValue(string $config_value): void
    {
        $this->config_value = $config_value;
    }

    /**
     * @return string
     */
    public function getConfigType(): string
    {
        return $this->config_type;
    }

    /**
     * @param string $config_type
     */
    public function setConfigType(string $config_type): void
    {
        $this->config_type = $config_type;
    }

    /**
     * @return string
     */
    public function getCreateBy(): string
    {
        return $this->create_by;
    }

    /**
     * @param string $create_by
     */
    public function setCreateBy(string $create_by): void
    {
        $this->create_by = $create_by;
    }

    /**
     * @return string
     */
    public function getCreateTime(): string
    {
        return $this->create_time;
    }

    /**
     * @param string $create_time
     */
    public function setCreateTime(string $create_time): void
    {
        $this->create_time = $create_time;
    }

    /**
     * @return string
     */
    public function getUpdateBy(): string
    {
        return $this->update_by;
    }

    /**
     * @param string $update_by
     */
    public function setUpdateBy(string $update_by): void
    {
        $this->update_by = $update_by;
    }

    /**
     * @return string
     */
    public function getUpdateTime(): string
    {
        return $this->update_time;
    }

    /**
     * @param string $update_time
     */
    public function setUpdateTime(string $update_time): void
    {
        $this->update_time = $update_time;
    }

    /**
     * @return string
     */
    public function getRemark(): string
    {
        return $this->remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark(string $remark): void
    {
        $this->remark = $remark;
    }



}