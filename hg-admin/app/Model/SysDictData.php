<?php

declare (strict_types=1);
namespace App\Model;


/**
 * @property integer $dict_code 字典编码
 * @property integer $dict_sort 排序
 * @property string $dict_label 字典标签
 * @property string $dict_value 字典键值
 * @property string $dict_type 字典类型
 * @property string $css_class 样式属性（其他样式扩展）
 * @property string $list_class 表格回显样式
 * @property integer $status 状态 '1-正常 0-停用'
 * @property integer $is_default 1-默认 0-否
 * @property string $create_time
 * @property string $create_by
 * @property string $update_by
 * @property string $update_time
 * @property string $remark
 */
class SysDictData extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sys_dict_data';

    protected $primaryKey = 'dict_code';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * @return int
     */
    public function getDictCode(): int
    {
        return $this->dict_code;
    }

    /**
     * @param int $dict_code
     */
    public function setDictCode(int $dict_code): void
    {
        $this->dict_code = $dict_code;
    }

    /**
     * @return int
     */
    public function getDictSort(): int
    {
        return $this->dict_sort;
    }

    /**
     * @param int $dict_sort
     */
    public function setDictSort(int $dict_sort): void
    {
        $this->dict_sort = $dict_sort;
    }

    /**
     * @return string
     */
    public function getDictLabel(): string
    {
        return $this->dict_label;
    }

    /**
     * @param string $dict_label
     */
    public function setDictLabel(string $dict_label): void
    {
        $this->dict_label = $dict_label;
    }

    /**
     * @return string
     */
    public function getDictValue(): string
    {
        return $this->dict_value;
    }

    /**
     * @param string $dict_value
     */
    public function setDictValue(string $dict_value): void
    {
        $this->dict_value = $dict_value;
    }

    /**
     * @return string
     */
    public function getDictType(): string
    {
        return $this->dict_type;
    }

    /**
     * @param string $dict_type
     */
    public function setDictType(string $dict_type): void
    {
        $this->dict_type = $dict_type;
    }

    /**
     * @return ?string
     */
    public function getCssClass(): ?string
    {
        return $this->css_class;
    }

    /**
     * @param ?string $css_class
     */
    public function setCssClass(?string $css_class): void
    {
        $this->css_class = $css_class;
    }

    /**
     * @return ?string
     */
    public function getListClass(): ?string
    {
        return $this->list_class;
    }

    /**
     * @param ?string $list_class
     */
    public function setListClass(?string $list_class): void
    {
        $this->list_class = $list_class;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getIsDefault(): int
    {
        return $this->is_default;
    }

    /**
     * @param int $is_default
     */
    public function setIsDefault(int $is_default): void
    {
        $this->is_default = $is_default;
    }

    /**
     * @return string
     */
    public function getCreateTime(): string
    {
        return $this->create_time;
    }

    /**
     * @param string $create_time
     */
    public function setCreateTime(string $create_time): void
    {
        $this->create_time = $create_time;
    }

    /**
     * @return string
     */
    public function getCreateBy(): string
    {
        return $this->create_by;
    }

    /**
     * @param string $create_by
     */
    public function setCreateBy(string $create_by): void
    {
        $this->create_by = $create_by;
    }

    /**
     * @return string
     */
    public function getUpdateBy(): string
    {
        return $this->update_by;
    }

    /**
     * @param string $update_by
     */
    public function setUpdateBy(string $update_by): void
    {
        $this->update_by = $update_by;
    }

    /**
     * @return string
     */
    public function getUpdateTime(): string
    {
        return $this->update_time;
    }

    /**
     * @param string $update_time
     */
    public function setUpdateTime(string $update_time): void
    {
        $this->update_time = $update_time;
    }

    /**
     * @return ?string
     */
    public function getRemark(): ?string
    {
        return $this->remark;
    }

    /**
     * @param ?string $remark
     */
    public function setRemark(?string $remark): void
    {
        $this->remark = $remark;
    }





}