<?php

declare (strict_types=1);
namespace App\Model;
/**
 * @property integer $id
 * @property string $role_name
 * @property string $role_key
 * @property ?integer $role_sort
 * @property ?string $description
 * @property integer $status 0-禁用 1-正常
 * @property integer $menu_check_strictly 菜单树选项是否关联展示 0-不关联 1-关联
 * @property ?string $create_time
 * @property ?string $update_time
 * @property ?string $create_by
 * @property ?string $update_by
 */
class SysRole extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sys_roles';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public function isAdmin(): bool
    {
        return self::isSuperAdmin($this->id);
    }

    public static function isSuperAdmin(int $roleId): bool
    {
        return 1 == $roleId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getRoleName(): string
    {
        return $this->role_name;
    }

    /**
     * @param string $role_name
     */
    public function setRoleName(string $role_name): void
    {
        $this->role_name = $role_name;
    }

    /**
     * @return ?string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param ?string $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return ?string
     */
    public function getCreateTime(): ?string
    {
        return $this->create_time;
    }

    /**
     * @param ?string $create_time
     */
    public function setCreateTime(?string $create_time): void
    {
        $this->create_time = $create_time;
    }

    /**
     * @return ?string
     */
    public function getUpdateTime(): ?string
    {
        return $this->update_time;
    }

    /**
     * @param ?string $update_time
     */
    public function setUpdateTime(?string $update_time): void
    {
        $this->update_time = $update_time;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getRoleKey(): string
    {
        return $this->role_key;
    }

    /**
     * @param string $role_key
     */
    public function setRoleKey(string $role_key): void
    {
        $this->role_key = $role_key;
    }

    /**
     * @return ?int
     */
    public function getRoleSort(): ?int
    {
        return $this->role_sort;
    }

    /**
     * @param ?int $role_sort
     */
    public function setRoleSort(?int $role_sort): void
    {
        $this->role_sort = $role_sort;
    }

    /**
     * @return int
     */
    public function getMenuCheckStrictly(): int
    {
        return $this->menu_check_strictly;
    }

    /**
     * @param int $menu_check_strictly
     */
    public function setMenuCheckStrictly(int $menu_check_strictly): void
    {
        $this->menu_check_strictly = $menu_check_strictly;
    }

    /**
     * @return string
     */
    public function getCreateBy(): string
    {
        return $this->create_by;
    }

    /**
     * @param string $create_by
     */
    public function setCreateBy(string $create_by): void
    {
        $this->create_by = $create_by;
    }

    /**
     * @return string
     */
    public function getUpdateBy(): string
    {
        return $this->update_by;
    }

    /**
     * @param string $update_by
     */
    public function setUpdateBy(string $update_by): void
    {
        $this->update_by = $update_by;
    }



}