<?php

namespace App\Listener;


use App\Model\SysCrontab;
use App\Services\SysCrontabService;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Crontab\Crontab;
use Hyperf\Crontab\CrontabManager;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Process\Event\BeforeCoroutineHandle;
use Hyperf\Process\Event\BeforeProcessHandle;


/**
 * @Listener
 * Class SelfCrontabRegisterListener
 * created by: lhw at 2022/8/21 20:03
 */
class SelfCrontabRegisterListener implements ListenerInterface
{

    /**
     * @var \Hyperf\Crontab\CrontabManager
     */
    protected $crontabManager;

    /**
     * @var \Hyperf\Contract\StdoutLoggerInterface
     */
    protected $logger;

    /**
     * @var SysCrontabService
     */
    private SysCrontabService $crontabService;



    public function __construct(CrontabManager $crontabManager, StdoutLoggerInterface $logger, SysCrontabService $crontabService)
    {
        $this->crontabManager = $crontabManager;
        $this->logger = $logger;
        $this->crontabService = $crontabService;
    }

    /**
     * @inheritDoc
     */
    public function listen(): array
    {
        return [
            BeforeProcessHandle::class,
            BeforeCoroutineHandle::class,
        ];
    }

    /**
     * Handle the Event when the event is triggered, all listeners will
     * complete before the event is returned to the EventDispatcher.
     */
    public function process(object $event)
    {
        $crontabs = $this->parseCrontabs();
        foreach ($crontabs as $crontab) {
            if ($crontab instanceof Crontab && $this->crontabManager->register($crontab)) {
                $this->logger->info(sprintf('Crontab %s have been registered.', $crontab->getName()));
            }
        }
    }

    private function parseCrontabs(): array
    {
        $configCrontabs = $this->crontabService->selectAllCrontab();
        $crontabs = [];
        foreach ($configCrontabs as $crontab) {
            $crontab = $this->buildCrontabByAnnotation($crontab);
            $crontabs[$crontab->getName()] = $crontab;
        }
        return array_values($crontabs);
    }

    private function buildCrontabByAnnotation(SysCrontab $annotation): Crontab
    {
        $crontab = new Crontab();
        isset($annotation->job_name) && $crontab->setName($annotation->job_name);
        isset($annotation->job_type) && $crontab->setType($annotation->job_type);
        isset($annotation->job_rule) && $crontab->setRule($annotation->job_rule);
        isset($annotation->singleton) && $crontab->setSingleton($annotation->singleton);
        isset($annotation->mutexPool) && $crontab->setMutexPool($annotation->mutexPool);
        isset($annotation->mutexExpires) && $crontab->setMutexExpires($annotation->mutexExpires);
        isset($annotation->onOneServer) && $crontab->setOnOneServer($annotation->onOneServer);
        isset($annotation->job_callback) && $crontab->setCallback(json_decode($annotation->job_callback, true));
        isset($annotation->job_memo) && $crontab->setMemo($annotation->job_memo);
        isset($annotation->job_enable) && $crontab->setEnable($annotation->job_enable == 1);

        return $crontab;
    }

}