<?php

namespace App\Domain\Dao;

use App\Constants\CommonCode;
use App\Model\SysRole;
use Hyperf\Database\Model\Builder;
use Hyperf\Utils\Collection;

class SysRoleDao extends BaseDao
{
    public function getModel($isAlias = false): SysRole {
        if (!$isAlias) {
            return new SysRole();
        }
        $model = new SysRole();
        $model->setTable("sys_roles as r");
        return $model;
    }

    /**
     * desc: 获取全部角色信息
     * created by: lhw at 2022/8/2 22:37
     * @return Collection
     */
    public function getRoleAll(): Collection
    {
        $model = $this->getModel(true);
        return $model->leftJoin("sys_user_role as ur", "ur.role_id", "=", "r.id")
            ->leftJoin("sys_users as u", "u.id", "=", "ur.user_id")
            ->where("r.status", "=", 1)
            ->distinct()
            ->get(["r.id as roleId", "r.role_name as roleName", "r.description", "r.status", "r.create_time as createTime"]);
    }

    /**
     * desc: 根据用户ID获取角色列表信息
     * @param int $userId
     * created by: lhw at 2022/8/2 23:15
     */
    public function selectRoleListByUserId(int $userId): Collection
    {
        $model = $this->getModel(true);
        return $model->leftJoin("sys_user_role as ur", "ur.role_id", "=", "r.id")
            ->leftJoin("sys_users as u", "u.id", "=", "ur.user_id")
            ->where("u.id", "=", $userId)
            ->get(["r.id as roleId"]);
    }

    /**
     * desc: 根据用户获取角色信息
     * @param int $userId
     * created by: lhw at 2022/8/1 22:57
     * @return Collection
     */
    public function selectRolePermissionByUserId(int $userId): Collection
    {
        $model = $this->getModel(true);
        return $model->leftJoin("sys_user_role as ur", "ur.role_id", "=", "r.id")
                      ->leftJoin("sys_users as u", "u.id", "=", "ur.user_id")
                      ->where("u.id", "=", $userId)
                      ->where("r.status", "=", 1)
                      ->get(["r.id", "r.role_name as roleName", "r.description", "r.status", "r.create_time"]);
    }

    /**
     * desc: 分页查询
     * @param array $condition
     * @param array|string[] $columns
     * created by: lhw at 2022/8/17 22:09
     * @return mixed
     */
    public function selectRoleList(array $condition, array $columns = ["*"]) {
        if (!isset($condition['sortColumns'])) {
            $condition['sortColumns'] =[
                [
                    'sortColumn' => 'role_sort',
                    'sort' => CommonCode::MODEL_SORT_ASC
                ]
            ];
        }
        return $this->fetchData($condition, $columns);
    }

    /**
     * desc: 删除角色
     * @param array $roleIds
     * created by: lhw at 2022/8/19 15:11
     * @return int
     */
    public function deleteRole(array $roleIds): int {
        return $this->getModel()::whereIn("id", $roleIds)->delete();
    }

    /**
     * desc: 筛选条件组装
     * @param SysRole $model
     * @param array $condition
     * created by: lhw at 21/7/2022 10:01 下午
     * @return SysRole|Builder
     */
    public function filterConditions(SysRole $model, array $condition = []) {
        if (!empty($condition['roleName'])) {
            $model = $model->where('role_name', 'like', "%". $condition['roleName'] . "%");
        }
        if (!empty($condition['roleNameEq'])) {
            $model = $model->where('role_name', '=', $condition['roleNameEq']);
        }
        if (!empty($condition['roleKey'])) {
            $model = $model->where('role_key', 'like', "%". $condition['roleKey'] . "%");
        }
        if (!empty($condition['roleKeyEq'])) {
            $model = $model->where('role_key', $condition['roleKeyEq']);
        }
        if (isset($condition['status'])) {
            $model = $model->where('status', '=', $condition['status']);
        }
        if (isset($condition['roleId'])) {
            $model = $model->where('id', '=', $condition['roleId']);
        }
        if (isset($condition['roleIdIn'])) {
            $model = $model->whereIn('id', $condition['roleIdIn']);
        }
        if (!empty($condition['beginTime'])) {
            $model = $model->where('create_time', '>=',  $condition['beginTime']);
        }
        if (!empty($condition['endTime'])) {
            $model = $model->where('create_time', '<=',  $condition['endTime']);
        }

        return $model;
    }
}