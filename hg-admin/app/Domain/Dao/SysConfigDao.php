<?php
/**
 * Class SysConfigDao
 * @package App\Domain\Dao
 * Desc:
 * created by: lhw at 2022/8/8 14:56
 */

namespace App\Domain\Dao;


use App\Constants\CommonCode;
use App\Model\SysConfig;

class SysConfigDao extends BaseDao
{
    public function getModel($isAlias = false): SysConfig {
        if (!$isAlias) {
            return new SysConfig();
        }
        $model = new SysConfig();
        $model->setTable("sys_config as sc");
        return $model;
    }

    /**
     * desc:分页查询
     * @param array $conditions
     * @param array $columns
     * created by: lhw at 2022/8/8 15:41
     * @return mixed
     */
    public function getPageList(array $conditions, array $columns = ["*"]) {
        return $this->fetchData($conditions, $columns);
    }

    /**
     * desc:新增配置
     * @param array $params
     * created by: lhw at 2022/8/8 17:58
     * @return bool
     */
    public function addConfig(array $params): bool {
        $model = $this->getModel();
        $model->setConfigKey($params["configKey"]);
        $model->setConfigName($params["configName"]);
        $model->setConfigValue($params["configValue"]);
        $model->setConfigType($params["configType"]);
        $model->setCreateBy($params["user"] ?? "");
        $model->setCreateTime(date("Y-m-d H:i:s"));
        $model->setUpdateBy($params["currentUser"] ?? "");
        $model->setUpdateTime(date("Y-m-d H:i:s"));
        $model->setRemark($params["remark"] ?? "");
        return $model->save();
    }

    /**
     * desc: 更新配置
     * @param array $params
     * created by: lhw at 2022/8/10 14:52
     * @return bool
     */
    public function updateConfig(array $params): bool {
        $model = $this->getModel()->find($params["configId"]);
        $model->setConfigKey($params["configKey"]);
        $model->setConfigName($params["configName"]);
        $model->setConfigValue($params["configValue"]);
        $model->setConfigType($params["configType"]);
        $model->setUpdateBy($params["user"] ?? "");
        $model->setUpdateTime(date("Y-m-d H:i:s"));
        $model->setRemark($params["remark"] ?? "");
        return $model->save();
    }

    /**
     * desc:删除配置
     * @param array $params
     * created by: lhw at 2022/8/10 15:12
     * @return bool
     */
    public function deleteConfig(array $params): bool {
        return $this->filterConditions($this->getModel(), $params)->forceDelete();
    }

    /**
     * desc: 查询配置信息
     * @param int $configId
     * created by: lhw at 2022/8/9 10:24
     * @return SysConfig|SysConfig[]|\Hyperf\Database\Model\Collection|\Hyperf\Database\Model\Model|null
     */
    public function getConfigInfo(int $configId) {
        return $this->getModel()->find($configId);
    }

    /**
     * desc: 校验参数key是否唯一
     * @param array $params
     * created by: lhw at 2022/8/10 16:19
     * @return mixed
     */
    public function checkConfigKeyUnique(array $params) {
        if (!isset($params['first'])) {
            $params['first'] = true;
        }
        return $this->fetchData($params);
    }


    public function filterConditions(SysConfig $model, array $condition = []) {
        if (isset($condition['configId'])) {
            $model = $model->where('config_id', '=', $condition['configId']);
        }
        if (isset($condition['configName'])) {
            $model = $model->where('config_name', 'like', "%".$condition['configName']."%");
        }
        if (isset($condition['configKey'])) {
            $model = $model->where('config_key', 'like', "%".$condition['configKey']."%");
        }
        if (isset($condition['configKeyUnique']) && $condition['configKeyUnique'] == true) {
            $model = $model->where('config_key', '=', $condition['configKeyEq']);
        }
        if (isset($condition['configType'])) {
            $model = $model->where('config_type', '=', $condition['configType']);
        }
        if (isset($condition['batchDelete']) && $condition['batchDelete'] == true) {
            $model = $model->whereIn('config_id', $condition['configIds']);
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $model = $model->whereBetween('create_time', [$condition['beginTime'], $condition['endTime']]);
        }

        return $model;
    }

}