<?php

namespace App\Domain\Dao;

use App\Model\SysDictType;

class SysDictTypeDao extends BaseDao
{

    public function getModel($isAlias = false): SysDictType
    {
        if (!$isAlias) {
            return new SysDictType();
        }
        $dict = new SysDictType();
        $dict->setTable("sys_dict_type as st");
        return $dict;
    }

    public function getListByPage(array $conditions) {
        return $this->fetchData($conditions);
    }

    /**
     * desc: 字典类型详情
     * @param int $dictId
     * created by: lhw at 2022/8/6 16:53
     * @return mixed
     */
    public function getInfoById(int $dictId) {
        $conditions = [
          "dictId" => $dictId,
          "first" => true
        ];
        return $this->fetchData($conditions);
    }

    /**
     * desc: 字典类型详情
     * @param string $dictType
     * created by: lhw at 2022/8/6 17:06
     * @return mixed
     */
    public function getInfoByType(string $dictType) {
        $conditions = [
            "dictType" => $dictType,
            "first" => true
        ];
        return $this->fetchData($conditions);
    }

    /**
     * desc: 新增字典类型
     * @param array $params
     * created by: lhw at 2022/8/6 21:53
     * @return bool
     */
    public function addDictType(array $params): bool {
        $model = $this->getModel();
        $model->setDictName($params["dictName"]);
        $model->setDictType($params["dictType"]);
        $model->setStatus($params["status"]);
        $model->setRemark($params["remark"] ?? "");
        $model->setCreateTime(date("Y-m-d H:i:s"));
        $model->setUpdateTime(date("Y-m-d H:i:s"));
        $model->setCreateBy($params["currentUser"]);
        $model->setUpdateBy($params["currentUser"]);
        return $model->save();
    }

    /**
     * desc:修改字典
     * @param array $params
     * created by: lhw at 2022/8/6 22:26
     * @return bool
     */
    public function updateDictType(array $params): bool {
        $model = $this->getInfoById($params["dictId"]);
        $model->setDictId($params["dictId"]);
        $model->setDictName($params["dictName"]);
        $model->setDictType($params["dictType"]);
        $model->setStatus($params["status"]);
        $model->setRemark($params["remark"] ?? "");
        $model->setUpdateTime(date("Y-m-d H:i:s"));
        $model->setUpdateBy($params["currentUser"]);
        return $model->save();
    }

    /**
     * desc: 删除
     * @param string $dictId
     * created by: lhw at 2022/8/7 13:28
     * @return bool
     */
    public function deleteDictType(string $dictId): bool {
        $condition = [
            "batchDelete" => true,
            "dictIds" => explode(",", $dictId)
        ];
        return $this->filterConditions($this->getModel(), $condition)->forceDelete();
    }

    /**
     * desc: 筛选条件
     * @param SysDictType $model
     * @param array $condition
     * created by: lhw at 2022/8/6 15:40
     * @return SysDictType|\Hyperf\Database\Model\Builder
     */
    public function filterConditions(SysDictType $model, array $condition = []) {
        if (isset($condition['status'])) {
            $model = $model->where('status', '=', $condition['status']);
        }
        if (isset($condition['dictId'])) {
            $model = $model->where('dict_id', '=', $condition['dictId']);
        }
        if (isset($condition['dictType'])) {
            $model = $model->where('dict_type', 'like', "%".$condition['dictType']."%");
        }
        if (isset($condition['dictName'])) {
            $model = $model->where('dict_name', 'like', "%".$condition['dictName']."%");
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $model = $model->whereBetween('create_time', [$condition['beginTime'], $condition['endTime']]);
        }
        if (isset($condition['batchDelete']) && $condition['batchDelete'] == true) {
            $model = $model->whereIn('dict_id', $condition['dictIds']);
        }

        return $model;
    }
}