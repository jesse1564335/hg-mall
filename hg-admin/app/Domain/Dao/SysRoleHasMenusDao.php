<?php

namespace App\Domain\Dao;

use App\Model\SysRoleHasMenu;
use Hyperf\Database\Model\Builder;
use Hyperf\DbConnection\Annotation\Transactional;

class SysRoleHasMenusDao extends BaseDao
{

    public function getModel(): SysRoleHasMenu {
        return new SysRoleHasMenu();
    }

    /**
     * desc: 检查菜单是否分配有角色
     * @param array $conditions
     * created by: checkMenuHasRole at 2022/7/24 21:58
     * @return int
     */
    public function checkMenuHasRole(array $conditions): int {
        return $this->fetchData($conditions);
    }

    public function deleteByRoleIds(array $roleIds): int {
        return $this->getModel()::whereIn("role_id", $roleIds)->delete();
    }

    /**
     * desc: 添加角色菜单关系
     * @param int $roleId
     * @param array $menus
     * @return bool
     */
    public function addRoleMenus(int $roleId, array $menus): bool {
        $model = $this->getModel();
        $res = true;
        foreach ($menus as $menu) {
            $model->create(["role_id"=>$roleId, "menu_id"=>$menu]);
        }
        return $res;
    }

    /**
     * desc: 筛选条件组装
     * @param SysRoleHasMenu $model
     * @param array $condition
     * created by: lhw at 2022/7/24 21:56
     * @return SysRoleHasMenu|Builder
     */
    public function filterConditions(SysRoleHasMenu $model, array $condition = []) {
        if (!empty($condition['menuId'])) {
            $model = $model->where('menu_id', '=', $condition['menuId']);
        }
        if (!empty($condition['roleId'])) {
            $model = $model->where('role_id', '=', $condition['roleId']);
        }
        if (isset($condition['roleIdIn'])) {
            $model = $model->whereIn("role_id", $condition["roleIdIn"]);
        }

        return $model;
    }
}