<?php


namespace App\Domain\Dao;


use App\Constants\CommonCode;
use App\Model\SysMenu;
use Carbon\Carbon;
use \Hyperf\DbConnection\Db;


class SysMenuDao extends BaseDao
{

    public function getModel(?bool $isAlias = false): SysMenu {
       if (!$isAlias) {
           return new SysMenu();
       }
       $model = new SysMenu();
       $model->setTable("sys_menus as m");
       return $model;
    }

    /**
     * desc:添加菜单
     * @param array $params
     * created by: lhw at 21/7/2022 10:07 下午
     * @return bool
     */
    public function addMenu(array $params): bool {
        $model = $this->getModel();
        $model->setMenuName($params['menuName']);
        $model->setCreateBy($params['createBy']);
        $model->setCreateTime(Carbon::now()->toDateTime()->format("Y-m-d H:i:s"));
        $this->hydrateMenuVariable($model, $params);
        return $model->save();
    }

    public function updateMenu(array $params): bool {
        $model = $this->getModel()::find($params['menuId']);
        $this->hydrateMenuVariable($model, $params);
        return $model->save();
    }

    /**
     * desc:删除菜单
     * @param int $menuId
     * created by: lhw at 21/7/2022 10:12 下午
     * @return int
     */
    public function deleteMenuById(int $menuId): int {
        return $this->getModel()::where("menu_id", "=", $menuId)->forceDelete();
    }

    /**
     * desc:菜单公共信息
     * @param SysMenu|null $model
     * @param array $params
     * created by: lhw at 21/7/2022 9:52 下午
     */
    protected function hydrateMenuVariable(?SysMenu $model, array $params): void {
        $model->setMenuName($params['menuName']);
        $model->setParentId($params['parentId'] ?? 0);
        $model->setSort($params['sort'] ?? 0);
        $model->setPath($params['path'] ?? "");
        $model->setComponent($params['component'] ?? "");
        $model->setMenuType($params['menuType']);
        $model->setPerms($params['perms'] ?? "");
        $model->setIsFrame($params['isFrame'] ?? 0);
        $model->setStatus($params['status'] ?? 1);
        $model->setIsVisible($params['isVisible'] ?? 1);
        $model->setIcon($params['icon'] ?? "");
        $model->setRemark($params['remark'] ?? "");
        $model->setUpdateBy($params['updateBy']);
        $model->setUpdateTime(Carbon::now()->toDateTime()->format("Y-m-d H:i:s"));
    }

    /**
     * desc: 获取菜单列表
     * @param array $params
     * created by: lhw at 2022/7/24 19:16
     * @return mixed
     */
    public function getMenuList(array $params) {
        if (!isset($params['sortColumns'])) {
            $params['sortColumns'] =[
                [
                    'sortColumn' => 'parent_id',
                    'sort' => CommonCode::MODEL_SORT_ASC
                ],
                [
                    'sortColumn' => 'sort',
                    'sort' => CommonCode::MODEL_SORT_ASC
                ]
            ];
        }
       return $this->fetchData($params);
    }

    /**
     * desc: 根据userId获取菜单列表信息
     * * @param array $params
     * created by: lhw at 2022/7/25 17:13
     * @return mixed
     */
    public function getMenuListByUserId(array $params): object {
        $model = $this->getModel(true);
        return $model->leftJoin("sys_role_has_menus as rm", "m.menu_id", "=", "rm.menu_id")
            ->leftJoin("sys_user_role as ur", "rm.role_id", "=", "ur.role_id")
            ->leftJoin("sys_roles as ro", "ur.role_id", "=", "ro.id")
            ->where("ur.user_id", "=", $params["userId"])
            ->when(isset($params["menuName"]), function ($query) use($params) {
                $query->where("m.menu_name", "like", "%". $params["menuName"] ."%");
            })
            ->when(isset($params["status"]), function ($query) use($params) {
                $query->where("m.status", "=", $params["status"]);
            })
            ->get(["m.menu_id", "m.parent_id", "m.menu_name", "m.path", "m.component", "m.is_visible", "m.status", "m.perms", "m.is_frame", "m.menu_type", "m.icon", "m.sort", "m.create_time"])
            ;
    }

    /**
     * desc: 根据角色id查找菜单信息
     * * @param int $roleId
     * created by: lhw at 2022/7/25 17:20
     * @return array
     */
    public function getMenuListByRoleId(int $roleId): array {
        return Db::select("select m.menu_id from sys_menus m 
                    left join sys_role_has_menus rm on m.menu_id=rm.menu_id
                    where rm.role_id = {$roleId}
        ");
    }

    /**
     * desc: 根据菜单ID查询
     * @param array $conditions
     * created by: lhw at 2022/7/24 20:42
     * @return mixed
     */
    public function getMenuById(array $conditions) {
        return $this->fetchData($conditions);
    }

    /**
     * desc: 根据用户id查找权限标识
     * * @param int $userId
     * created by: lhw at 2022/8/1 14:32
     * @return \Hyperf\Utils\Collection
     */
    public function selectMenuPermsByUserId(int $userId): \Hyperf\Utils\Collection {
        $model = $this->getModel(true);
        return $model->leftJoin("sys_role_has_menus as rm", "m.menu_id", "=", "rm.menu_id")
                  ->leftJoin("sys_user_role as ur", "rm.role_id", "=", "ur.role_id")
                  ->leftJoin("sys_roles as r", "ur.role_id", "=", "r.id")
                  ->where("ur.user_id", "=", $userId)
                  ->where("m.status", "=", 1)
                  ->where("r.status", "=", 1)
                  ->get(["m.perms"]);
    }

    /**
     * desc: 根据用户ID获取全部菜单信息
     * created by: lhw at 2022/7/28 13:55
     * @return mixed
     */
    public function selectMenuTreeAll() {
        $params = [
            "all" => true,
            "sortColumns" => [
                [
                    'sortColumn' => 'parent_id',
                    'sort' => CommonCode::MODEL_SORT_ASC
                ],
                [
                    'sortColumn' => 'sort',
                    'sort' => CommonCode::MODEL_SORT_ASC
                ]
            ],
            "menuTypeIn" => [
                "menuType" => ["M", "C"]
            ],
            "status" => 1

        ];
        return $this->fetchData($params);
    }

    /**
     * desc: 根据用户ID获取全部菜单信息
     * @param int $userId
     * created by: lhw at 2022/7/28 14:03
     * @return mixed
     */
    public function selectMenuTreeByUserId(int $userId)
    {
        $model = $this->getModel(true);
        return $model->leftJoin("sys_role_has_menus as rm", "m.menu_id", "=", "rm.menu_id")
            ->leftJoin("sys_user_role as ur", "rm.role_id", "=", "ur.role_id")
            ->leftJoin("sys_roles as ro", "ur.role_id", "=", "ro.id")
            ->leftJoin("sys_users as u", "ur.user_id", "=", "u.id")
            ->where("u.id", "=", $userId)
            ->whereIn("m.menu_type", ['M', 'C'])
            ->where("m.status", "=", 1)
            ->where("ro.status", "=", 1)
            ->get(["m.menu_id", "m.parent_id", "m.menu_name", "m.path", "m.component", "m.is_visible", "m.status", "m.perms", "m.is_frame", "m.menu_type", "m.icon", "m.sort", "m.create_time"]);


//        return  Db::select("select distinct m.menu_id, m.parent_id, m.menu_name, m.path, m.component, m.is_visible, m.status, ifnull(m.perms,'') as perms, m.is_frame, m.menu_type, m.icon, m.sort, m.create_time
//                            from sys_menus m
//                                 left join sys_role_has_menus rm on m.menu_id = rm.menu_id
//                                 left join sys_user_role ur on rm.role_id = ur.role_id
//                                 left join sys_roles ro on ur.role_id = ro.id
//                                 left join sys_users u on ur.user_id = u.id
//                            where u.id = {$userId} and m.menu_type in ('M', 'C') and m.status = 1  AND ro.status = 1
//                            order by m.parent_id, m.sort
//              ");
//      return  Db::table("sys_menus as m")
//            ->leftJoin("sys_role_has_menus as rm", "m.menu_id", "=", "rm.menu_id")
//            ->leftJoin("sys_user_role as ur", "rm.role_id", "=", "ur.role_id")
//            ->leftJoin("sys_roles as ro", "ur.role_id", "=", "ro.id")
//            ->leftJoin("sys_users as u", "ur.user_id", "=", "u.id")
//            ->where("u.id", "=", $userId)
//            ->whereIn("m.menu_type", ['M', 'C'])
//            ->where("m.status", "=", 1)
//            ->where("ro.status", "=", 1)
//            ->get(["m.menu_id", "m.parent_id", "m.menu_name", "m.path", "m.component", "m.is_visible", "m.status", "m.perms", "m.is_frame", "m.menu_type", "m.icon", "m.sort", "m.create_time"]);
    }

    /**
     * desc: 筛选条件组装
     * @param SysMenu $model
     * @param array $condition
     * created by: lhw at 21/7/2022 10:01 下午
     * @return SysMenu|\Hyperf\Database\Model\Builder
     */
    public function filterConditions(SysMenu $model, array $condition = []) {
        if (!empty($condition['menuId'])) {
            $model = $model->where('menu_id', '=', $condition['menuId']);
        }
        if (!empty($condition['menuName'])) {
            $model = $model->where('menu_name', 'like', "%". $condition['menuName'] . "%");
        }
        if (isset($condition['status'])) {
            $model = $model->where('status', '=', $condition['status']);
        }
        if (!empty($condition['parentId'])) {
           $model = $model->where('parent_id', '=', $condition['parentId']);
        }
        if (!empty($condition['menuTypeIn'])) {
            $model = $model->whereIn('menu_type', $condition['menuTypeIn']['menuType']);
        }

//        if (!empty($condition['relatedId'])) {
//            $relatedId = $condition['relatedId'];
//            $model = $model->where(function ($query) use ($relatedId) {
//                $query->where("sender_id", "=", $relatedId)->orWhere("recv_id", "=", $relatedId);
//            });
//        }
//        if (!empty($condition['sendTimeBetween'])) {
//            $start_at = $condition['sendTimeBetween']['start_at'];
//            $lasted_at = $condition['sendTimeBetween']['lasted_at'];
//            $model = $model->where(function ($query) use ($start_at, $lasted_at) {
//                $query->where('send_time', '>=', $start_at)->where('send_time','<=', $lasted_at);
//            });
//        }
        return $model;
    }
}
