<?php

namespace App\Domain\Dao;

use App\Model\SysUserRole;


class SysUserRoleDao extends BaseDao
{
    public function getModel($isAlias = false): SysUserRole
    {
        if (!$isAlias) {
            return new SysUserRole();
        }
        $model = new SysUserRole();
        $model->setTable("sys_user_role as ur");
        return $model;
    }

    public function batchAddUserRole(array $params) {
        $this->batchCreate($params);
    }

    public function batchUpdateUserRole(array $params) {
        $this->deleteByUserId(["userIds" => $params["userId"]]);
        if (!empty($params["roleIds"])) {
            $this->batchCreate($params);
        }
    }

    public function deleteByUserId(array $userIds) {
        $model = $this->getModel();
        return $this->filterConditions($model, ["userIds"=>$userIds])->forceDelete();
    }

    public function countUserRoleByRoleId(int $roleId): int {
        $condition = [
          "roleId" => $roleId,
          "count" => true
        ];
        return $this->fetchData($condition);
    }


    public function filterConditions(SysUserRole $model, array $condition = []) {
        if (isset($condition['userId'])) {
            $model = $model->where('user_id', '=', $condition['userId']);
        }
        if (isset($condition['userIds'])) {
            $model = $model->whereIn('user_id', $condition['userIds']);
        }

        if (isset($condition['roleId'])) {
            $model = $model->where('role_id', '=', $condition['roleId']);
        }
        return $model;
    }

    /**
     * desc:
     * @param array $params
     * created by: lhw at 2022/8/14 10:34
     */
    private function batchCreate(array $params): void
    {
        $model = $this->getModel();
        foreach ($params["roleIds"] as $roleId) {
            $attrs = [
                "user_id" => $params["userId"],
                "role_id" => $roleId
            ];
            $model::create($attrs);
        }
    }
}