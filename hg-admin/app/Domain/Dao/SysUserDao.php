<?php

namespace App\Domain\Dao;

use App\Common\Helper;
use App\Constants\CommonCode;
use App\Model\SysUser;
use Hyperf\Utils\Collection;

class SysUserDao extends BaseDao
{
    public function getModel(?bool $isAlias = false): SysUser {
        if (!$isAlias) {
            return new SysUser();
        }
        $model = new SysUser();
        $model->setTable("sys_users as u");
        return $model;
    }

    public function getUserList(array $condition) {
        if (!isset($condition['sortColumns'])) {
            $condition['sortColumns'] = [
              [
                  'sortColumn' => 'create_time',
                  'sort' => CommonCode::MODEL_SORT_DESC
              ]
            ];
        }
        return $this->fetchData($condition);
    }

    public function addUser(array $condition): bool {
        $model = $this->getModel();
        $model->setName($condition["userName"]);
        $salt = Helper::instance()->getRandomWordCharacters(8);
        $model->setSalt($salt);
        $model->setPassword(Helper::instance()->md5Pwd($condition["password"], $salt));
        $model->setEmail($condition["email"] ?? "");
        $model->setPhone($condition["phone"] ?? "");
        $model->setGender($condition["sex"] ?? 0);
        $model->setStatus($condition["status"]);
        $model->setRemark($condition["remark"] ?? "");
        $model->setCreateTime(date("Y-m-d H:i:s"));
        return $model->save();
    }

    public function updateUser(array $condition):bool {
        $model = $this->getModel()->find($condition["userId"]);
        $model->setEmail($condition["email"] ?? "");
        $model->setPhone($condition["phone"] ?? "");
        $model->setGender($condition["sex"] ?? 0);
        $model->setStatus($condition["status"]);
        $model->setRemark($condition["remark"] ?? "");
        $model->setUpdateTime(date("Y-m-d H:i:s"));
        return $model->save();
    }

    public function getUserById(int $userId): Collection {
        $model = $this->getModel(true);
        return $model->leftJoin("sys_user_role as ur", "ur.user_id", "=", "u.id")
                ->leftJoin("sys_roles as r", "r.id", "=", "ur.role_id")
                ->where("u.id", "=", $userId)
                ->get(["u.id as userId", "u.name as userName", "u.email", "u.gender as sex", "u.phone", "u.password", "u.salt", "u.avatar",
                        "u.status", "u.remark", "u.create_time as createTime", "r.id as roleId", "r.role_name as roleName", "r.status as roleStatus"]
                );
    }

    public function isDisableUser(int $userId, int $status): int {
        $model = $this->getModel();
        return $model::where("id", "=", $userId)->update(["status" => $status]);
    }

    public function batchDeleteUser(array $params): bool {
        return $this->filterConditions($this->getModel(), $params)->forceDelete();
    }

    public function getUserInfoByName(string $name) {
        $condition = [
          "userNameEq" => $name,
          "first" => true
        ];
        return $this->fetchData($condition);
    }

    public function getUserInfoByPhone(string $phone) {
        $condition = [
            "phone" => $phone,
            "first" => true
        ];
        return $this->fetchData($condition);
    }

    public function getUserInfoByEmail(string $email) {
        $condition = [
            "email" => $email,
            "first" => true
        ];
        return $this->fetchData($condition);
    }


    public function filterConditions(SysUser $model, array $condition = []) {
        if (!empty($condition['userName'])) {
            $model = $model->where('name', 'like', "%" . $condition['userName'] . "%");
        }
        if (!empty($condition['userNameEq'])) {
            $model = $model->where('name', '=', $condition['userNameEq']);
        }
        if (isset($condition['status'])) {
            $model = $model->where('status', '=', $condition['status']);
        }
        if (isset($condition['userId'])) {
            $model = $model->where('id', '=', $condition['userId']);
        }
        if (isset($condition['batchDel']) && $condition['batchDel'] == true) {
            $model = $model->whereIn('id', $condition['userIds']);
        }
        if (isset($condition['phone'])) {
            $model = $model->where('phone', "like", "%" . $condition['phone'] . "%");
        }
        if (isset($condition['email'])) {
            $model = $model->where('email', "=", $condition['email']);
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $condition['endTime'] = date('Y-m-d 23:59:59', strtotime($condition['endTime']));
            $model = $model->whereBetween('create_time', [$condition['beginTime'], $condition['endTime']]);
        } elseif (isset($condition['createTimeStart'])) {
            $model = $model->where('create_time', ">=", $condition['createTimeStart']);
        } elseif (isset($condition['createTimeEnd'])) {
            $condition['endTime'] = date('Y-m-d 23:59:59', strtotime($condition['endTime']));
            $model = $model->where('create_time', "<=", $condition['createTimeEnd']);
        }

        return $model;
    }
}