<?php
//
//
//namespace App\Services\Dao;
//
//
//use App\Constants\CommonCode;
//use App\Model\ChatInfo;
//use Hyperf\Database\Model\Builder;
//
//class ChatDao extends BaseDao
//{
//
//    public function getModel(): ChatInfo
//    {
//        return new ChatInfo();
//    }
//
//    /**
//     * note:聊天列表
//     * @param array $conditions
//     * created by: lhw at 12/1/2022 9:19 下午
//     * @return mixed
//     */
//    public function getMsgList(array $conditions)
//    {
//        if (!isset($conditions['sortColumns'])) {
//            $conditions['sortColumns'] =[
//                [
//                    'sortColumn' => 'send_time',
//                    'sort' => CommonCode::MODEL_SORT_DESC
//                ]
//            ];
//        }
//        return $this->fetchData($conditions);
//    }
//
//    /**
//     * note:查询条件
//     * @param ChatInfo $model
//     * @param array $condition
//     * created by: lhw at 13/1/2022 9:48 下午
//     * @return ChatInfo|Builder
//     */
//    public function filterConditions(ChatInfo $model, array $condition = [])
//    {
//        if (!empty($condition['meetingId'])) {
//            $model = $model->where('meeting_id', '=', $condition['meetingId']);
//        }
//        if (!empty($condition['relatedId'])) {
//            $relatedId = $condition['relatedId'];
//            $model = $model->where(function ($query) use ($relatedId) {
//                $query->where("sender_id", "=", $relatedId)->orWhere("recv_id", "=", $relatedId);
//            });
//        }
//        if (!empty($condition['sendTimeBetween'])) {
//            $start_at = $condition['sendTimeBetween']['start_at'];
//            $lasted_at = $condition['sendTimeBetween']['lasted_at'];
//            $model = $model->where(function ($query) use ($start_at, $lasted_at) {
//                $query->where('send_time', '>=', $start_at)->where('send_time','<=', $lasted_at);
//            });
//        }
//        return $model;
//    }
//}
