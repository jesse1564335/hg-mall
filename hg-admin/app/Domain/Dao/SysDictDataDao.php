<?php

namespace App\Domain\Dao;

use App\Model\SysDictData;

class SysDictDataDao extends BaseDao
{
    public function getModel($isAlias = false): SysDictData
    {
        if (!$isAlias) {
            return new SysDictData();
        }
        $dict = new SysDictData();
        $dict->setTable("sys_dict_data as sd");
        return $dict;
    }

    /**
     * desc: 获取分页数据
     * @param array $conditions
     * created by: lhw at 2022/8/6 15:57
     * @return mixed
     */
    public function getListByPage(array $conditions) {
        return $this->fetchData($conditions);
    }

    /**
     * desc: 根据字典编码查询详情
     * @param int $dictCode
     * created by: lhw at 2022/8/7 15:09
     * @return mixed
     */
    public function getInfo(int $dictCode) {
        $condition = [
          'dictCode' => $dictCode,
          'first' => true
        ];
        return $this->fetchData($condition);
    }

    /**
     * desc: 字典详情
     * @param string $dictType
     * created by: lhw at 2022/8/6 17:06
     * @return mixed
     */
    public function getInfoByType(string $dictType) {
        $conditions = [
            "dictType" => $dictType,
            "all" => true
        ];
        return $this->fetchData($conditions);
    }

    /**
     * desc: 更新字典数据
     * @param array $params
     * created by: lhw at 2022/8/7 15:35
     * @return bool
     */
    public function update(array $params): bool {
        $model = $this->getInfo($params["dictCode"]);
        $model->setDictLabel($params["dictLabel"]);
        $model->setDictType($params["dictType"]);
        $model->setDictSort($params["dictSort"] ?? 0);
        $model->setDictValue($params["dictValue"]);
        $model->setStatus($params["status"]);
        $model->setRemark($params["remark"] ?? "");
        $date = date("Y-m-d H:i:s");
        $model->setUpdateBy($params["currentUser"]);
        $model->setUpdateTime($date);
        return $model->save();
    }

    /**
     * desc: 新增字典数据
     * @param array $params
     * created by: lhw at 2022/8/7 16:30
     * @return bool
     */
    public function add(array $params): bool {
        $model = $this->getModel();
        $model->setDictLabel($params["dictLabel"]);
        $model->setDictType($params["dictType"]);
        $model->setDictSort($params["dictSort"] ?? 0);
        $model->setDictValue($params["dictValue"]);
        $model->setStatus($params["status"]);
        $model->setRemark($params["remark"] ?? "");
        $date = date("Y-m-d H:i:s");
        $model->setCreateBy($params["currentUser"]);
        $model->setCreateTime($date);
        $model->setUpdateBy($params["currentUser"]);
        $model->setUpdateTime($date);
        return $model->save();
    }

    /**
     * desc: 删除字典数据
     * @param string $dictCode
     * created by: lhw at 2022/8/7 16:06
     * @return bool
     */
    public function delete(string $dictCode): bool {
        $condition = [
            "batchDelete" => true,
            "dictCodes" => explode(",", $dictCode)
        ];
        return $this->filterConditions($this->getModel(), $condition)->forceDelete();
    }

    public function countDictDataByType(string $dictType): int {
        $condition = [
            "count" => true,
            "dictTypeByCount" => $dictType
        ];
        return  $this->fetchData($condition);
    }

    /**
     * desc: 筛选条件组装
     * @param SysDictData $model
     * @param array $condition
     * created by: lhw at 2022/8/6 15:39
     * @return SysDictData|\Hyperf\Database\Model\Builder
     */
    public function filterConditions(SysDictData $model, array $condition = []) {
        if (isset($condition['dictCode'])) {
            $model = $model->where('dict_code', '=', $condition['dictCode']);
        }
        if (isset($condition['status'])) {
            $model = $model->where('status', '=', $condition['status']);
        }
        if (isset($condition['dictType'])) {
            $model = $model->where('dict_type', 'like', "%".$condition['dictType']."%");
        }
        if (isset($condition['dictName'])) {
            $model = $model->where('dict_name', 'like', "%".$condition['dictType']."%");
        }
        if (isset($condition['batchDelete']) && $condition['batchDelete'] == true) {
            $model = $model->whereIn('dict_code', $condition['dictCodes']);
        }
        if (isset($condition['count']) && $condition['count'] = true) {
            $model = $model->where('dict_type', '=', $condition['dictTypeByCount']);
        }

        return $model;
    }
}