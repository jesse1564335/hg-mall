<?php

namespace App\Domain\Dao;

use App\Model\SysCrontab;
use Hyperf\Database\Model\Builder;

class SysCrontabDao extends BaseDao
{

    public function getModel(): SysCrontab {
        return new SysCrontab();
    }

    public function pageList(array $conditions, array $columns = ["*"]) {
        return $this->fetchData($conditions, $columns);
    }

    public function getDetail(array $conditions, array $columns = ["*"]) {
        return $this->fetchData($conditions, $columns);
    }



    /**
     * desc: 组装筛选条件
     * @param SysCrontab $model
     * @param array $condition
     * created by: lhw at 2022/8/21 19:58
     * @return SysCrontab|Builder
     */
    public function filterConditions(SysCrontab $model, array $condition = []) {
        if (!empty($condition['jobId'])) {
            $model = $model->where('job_id', '=', $condition['jobId']);
        }
        if (!empty($condition['jobNameEq'])) {
            $model = $model->where('job_name', '=', $condition['jobNameEq']);
        }
        if (!empty($condition['jobName'])) {
            $model = $model->where('job_name', 'like', "%".$condition['jobName']."%");
        }
        if (!empty($condition['jobEnable'])) {
            $model = $model->where('job_enable', '=', $condition['jobEnable']);
        }
        if (!empty($condition["beginTime"]) && !empty($condition["endTime"])) {
            $model->whereBetween("create_time", [$condition["beginTime"], $condition["endTime"]]);
        }
        return $model;
    }
}