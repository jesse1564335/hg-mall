<?php
/**
 * Class SysUserReq
 * @package App\Domain\Request
 * Desc:
 * created by: lhw at 2022/8/2 15:43
 */

namespace App\Domain\Request;


use Hyperf\Validation\Request\FormRequest;

class SysUserReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'userName' => 'string|nullable',
            'phone' => 'string|nullable',
            'status' => 'integer|nullable',
            'beginTime' => 'date|before:tomorrow',
            'endTime' => 'date|after_or_equal:beginTime',
        ];
    }

    public function messages(): array
    {
        return [
            'beginTime.date' => '开始日期格式错误',
            'beginTime.before' => '开始日期最大是今天',
            'endTime.date' => '结束日期格式错误',
            'endTime.after_or_equal' => '结束日期必须大于等于开始日期',
        ];
    }
}