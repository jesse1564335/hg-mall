<?php
/**
 * Class ModifyRoleReq
 * @package App\Domain\Request
 * Desc:
 * created by: lhw at 2022/8/19 16:24
 */

namespace App\Domain\Request;


use Hyperf\Validation\Request\FormRequest;

class ModifyRoleReq extends FormRequest
{

    protected $scenes = [
        "changeStatus" => ["roleId", "status"],
        "update" => ["roleId", "roleName", "roleKey", "status", "roleSort", "menuCheckStrictly", "menuIds", "description"],
        "add" => ["roleName", "roleKey", "status", "roleSort", "menuCheckStrictly", "menuIds", "description"],
    ];

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'roleId' => 'required|integer',
            'status' => 'required|integer',
            'roleName' => 'required',
            'roleKey' => 'required',
            'roleSort' => 'integer|nullable',
            'menuIds' => 'required'
        ];
    }

    public function messages(): array
    {
        return [
            "roleId.required" => "角色ID必传",
            "roleId.integer" => "角色ID必须是整数",
            "status.required" => "状态必传",
            "status.integer" => "状态值必须是整数",
            "roleName.required" => "角色名称必传",
            "roleKey.required" => "权限字符必传",
            "menuIds.required" => "菜单权限必传"
        ];
    }
}