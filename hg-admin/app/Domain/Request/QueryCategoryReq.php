<?php
/**
 * Class QueryCategoryReq
 * @package App\Domain\Request
 * Desc:
 * created by: lhw at 2022/9/13 16:20
 */

namespace App\Domain\Request;


use Hyperf\Validation\Request\FormRequest;

class QueryCategoryReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'categoryName' => 'string|nullable',
            'status' => 'integer|nullable',
        ];
    }

    public function messages(): array
    {
        return [];
    }
}