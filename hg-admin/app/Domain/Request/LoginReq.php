<?php

namespace App\Domain\Request;

use Hyperf\Validation\Request\FormRequest;

class LoginReq extends FormRequest
{
    protected $scenes = [
        //图形验证码登录
        "captcha" => ["username", "password", "code", "uuid"],
    ];

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'username' => 'required|between:2,20',
            'password' => 'required|between:6,20',
            'code' => 'required|alpha_num',
            'uuid' => 'required|string',
        ];
    }

    public function messages(): array
    {
        return [
            'username.required' => '用户名必填',
            'username.between' => '用户名长度在3~100之间',
            'password.required' => '密码必填',
            'password.between' => '密码长度在6~20之间',
            'uuid.required' => '缺少uuid',
            'code.required' => '缺少验证码',
            'code.alpha_num' => '验证码必须是字母或数字'
        ];
    }
}