<?php

namespace App\Domain\Request;

use Hyperf\Validation\Request\FormRequest;

class ModifySysUserReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'userId' => 'required',
            'phone' => 'string|nullable',
            'email' => 'email|nullable',
            'remark' => 'string|nullable',
            'roleIds' => 'nullable',
            'status' => 'integer',
            'sex' => 'integer'
        ];
    }

    public function messages(): array
    {
        return [
            'userId.required' => '缺少用户编号',
            'email.email' => '邮箱格式错误',
        ];
    }
}