<?php


namespace App\Domain\Request;


use Hyperf\Validation\Request\FormRequest;

class SaveCrontabReq extends FormRequest
{
    protected $scenes = [
      "insert" => ["jobName", "jobCallback", "jobRule", "jobEnable"],
      "update" => ["jobId", "jobName", "jobCallback", "jobRule", "jobEnable"]
    ];

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'jobName' => 'required',
            'jobCallback' => 'required',
            'jobRule' => 'required',
            'jobEnable' => 'required|integer|between:0,1',
            'jobId' => 'required|integer'
        ];
    }

    public function messages(): array
    {
        return [
            'jobId.required' => '任务编号必传',
            'jobId.integer' => '任务编号必须是整数',
            'jobName.required' => '任务名称必填',
            'jobCallback.required' => '调用方法必填',
            'jobRule.required' => 'crontab执行表达式必填',
            'jobEnable.required' => '状态必选',
            'jobEnable.between' => '状态必须是0或1',
        ];
    }
}