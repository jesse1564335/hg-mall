<?php
/**
 * Class SysConfigReq
 * @package App\Domain\Request
 * Desc:
 * created by: lhw at 2022/8/8 15:57
 */

namespace App\Domain\Request;

use Hyperf\Validation\Request\FormRequest;


class SysConfigReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'pageNum' => 'integer|nullable',
            'pageSize' => 'integer|nullable',
            'configName' => 'string|nullable',
            'configKey' => 'string|nullable',
            'configType' => 'string|nullable',
            'beginTime' => 'date|before:tomorrow',
            'endTime' => 'date|after_or_equal:beginTime',
        ];
    }

    public function messages(): array
    {
        return [
            'beginTime.before' => '开始日期最大是今天',
            'endTime.after_or_equal' => '结束日期必须大于等于开始日期',
        ];
    }
}