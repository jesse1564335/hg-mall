<?php

namespace App\Domain\Request;

use Hyperf\Validation\Request\FormRequest;


class ModifyDictTypeReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'dictName' => 'required|alpha',
            'dictType' => 'required|string',
            'status' => 'integer',
            'remark' => 'alpha_dash',
        ];
    }

    public function messages(): array
    {
        return [
            'dictName.required' => '字典名称必填',
            'dictName.alpha' => '字典名称必须时字母或汉字',
            'dictType.required' => '字典类型必填',
            'dictType.string' => '字典类型必须是字符串',
            'status.integer' => '状态必须是整型',
            'remark.alpha_dash' => '备注只能包含字母(包含中文)和数字，以及破折号和下划线'
        ];
    }
}