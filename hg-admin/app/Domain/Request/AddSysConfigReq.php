<?php
/**
 * Class AddSysConfigReq
 * @package App\Domain\Request
 * Desc:
 * created by: lhw at 2022/8/8 17:05
 */

namespace App\Domain\Request;

use Hyperf\Validation\Request\FormRequest;


class AddSysConfigReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'configName' => 'required',
            'configKey' => 'required',
            'configValue' => 'required',
            'configType' => 'string',
            'remark' => 'string'
        ];
    }

    public function messages(): array
    {
        return [
            'configName.required' => '参数名称必填',
            'configKey.required' => '参数键名必填',
            'configValue.required' => '参数键值必填',
        ];
    }
}