<?php
/**
 * Class ModifySysConfigReq
 * @package App\Domain\Request
 * Desc:
 * created by: lhw at 2022/8/10 14:48
 */

namespace App\Domain\Request;


use Hyperf\Validation\Request\FormRequest;


class ModifySysConfigReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'configId' => 'required',
            'configName' => 'required',
            'configKey' => 'required',
            'configValue' => 'required',
            'configType' => 'string',
            'remark' => 'string'
        ];
    }

    public function messages(): array
    {
        return [
            'configId.required' => '参数主键ID必传',
            'configName.required' => '参数名称必填',
            'configKey.required' => '参数键名必填',
            'configValue.required' => '参数键值必填',
        ];
    }
}