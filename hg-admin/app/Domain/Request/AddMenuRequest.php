<?php


namespace App\Domain\Request;


use Hyperf\Validation\Request\FormRequest;

class AddMenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * 场景：M主目录 C菜单 F按钮
     * @var array[]
     */
    protected $scenes = [
        'M' => ['menuName', 'menuType', 'sort', 'path', 'isVisible', 'isFrame', 'status'],
        'C' => ['menuName', 'menuType', 'sort', 'path', 'isVisible', 'isFrame', 'status'],
        'F' => ['menuName', 'menuType', 'perms', 'sort']
    ];


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'menuName' => 'required|between:3,100',
            'path' => 'required|between:3,200',
            'component' => 'required|between:3,200',
            'menuType' => 'required|in:M,C,F',
            'isVisible' => 'in:0,1',
            'isFrame' => 'in:0,1',
            'status' => 'in:0,1',
            'sort' => 'integer',
        ];
    }

    public function messages(): array
    {
        return [
            'menuName.required' => '菜单名称必填',
            'menuName.between' => '菜单名称长度在3~100之间',
            'path.required' => '路由地址必填',
            'path.between' => '路由地址长度在3~100之间',
            'component.required' => '组件地址必填',
            'component.between' => '组件地址长度在3~100之间',
            'menuType.required' => '请选择菜单类型',
            'menuType.in' => '请正确选择菜单类型',
            'isVisible.in' => '请正确选择是否可见',
            'isFrame.in' => '请正确选择是否外链',
            'status.in' => '请正确选择状态',
            'sort.integer' => '排序必须是整数',
        ];
    }
}
