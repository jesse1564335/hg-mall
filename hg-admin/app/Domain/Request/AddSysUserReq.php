<?php
/**
 * Class AddSysUserReq
 * @package App\Domain\Request
 * Desc:
 * created by: lhw at 2022/8/11 10:37
 */

namespace App\Domain\Request;


use Hyperf\Validation\Request\FormRequest;


class AddSysUserReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'userName' => 'required',
            'password' => 'required',
            'phone' => 'string|nullable',
            'email' => 'email|nullable',
            'remark' => 'string|nullable',
            'roleIds' => 'nullable',
            'status' => 'integer|nullable',
            'sex' => 'integer|nullable'
        ];
    }

    public function messages(): array
    {
        return [
            'userName.required' => '用户名称必填',
            'password.required' => '密码必填',
            'email.email' => '邮箱格式错误',
        ];
    }
}