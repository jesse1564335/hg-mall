<?php


namespace App\Domain\Request;


use Hyperf\Validation\Request\FormRequest;

class QueryMenuReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'menuName' => 'string|nullable',
            'status' => 'integer|nullable',
        ];
    }

    public function messages(): array
    {
        return [];
    }
}
