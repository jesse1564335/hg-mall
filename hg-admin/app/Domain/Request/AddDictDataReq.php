<?php

namespace App\Domain\Request;

use Hyperf\Validation\Request\FormRequest;


class AddDictDataReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'dictLabel' => 'required',
            'dictValue' => 'required',
            'dictType' => 'required|string',
            'status' => 'integer',
            'dictSort' => 'integer',
            'remark' => 'string',
        ];
    }

    public function messages(): array
    {
        return [
            'dictLabel.required' => '数据标签必填',
            'dictValue.required' => '数据键值必填',
            'dictType.required' => '字典类型必填',
            'dictType.string' => '字典类型必须是字符串',
            'status.integer' => '状态必须是整型',
            'dictSort.integer' => '排序必须是整型',
            'remark.string' => '备注必须是字符串'
        ];
    }
}