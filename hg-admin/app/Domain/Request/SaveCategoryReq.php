<?php
/**
 * Class SaveCategoryReq
 * @package App\Domain\Request
 * Desc:
 * created by: lhw at 2022/9/14 13:18
 */

namespace App\Domain\Request;


use Hyperf\Validation\Request\FormRequest;

class SaveCategoryReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    protected $scenes = [
        "add"=> ["categoryValue", "categoryName", "parentId"],
        "update" => ["parentId", "categoryId", "categoryName", "categoryValue"]
    ];


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'categoryValue' => 'required',
            'categoryId' => 'required|integer',
            'categoryName' => 'required|string',
            'parentId' => 'required|integer'
        ];
    }

    public function messages(): array
    {
        return [
            'categoryId.required' => '分类ID必传',
            'categoryName.required' => '分类名称必填',
            'categoryValue.required' => '分类键值必填',
            'parentId.integer' => '上级分类ID必须是整数'
        ];
    }
}