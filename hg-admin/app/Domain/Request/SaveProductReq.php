<?php
/**
 * Class SaveProductReq
 * @package App\Domain\Request
 * Desc:
 * created by: lhw at 2022/9/9 11:25
 */

namespace App\Domain\Request;


use Hyperf\Validation\Request\FormRequest;


class SaveProductReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    protected $scenes = [
      "add"=> ["productName", "categoryId", "categoryName", "price", "description", "frontImage"],
      "update" => ["id", "productName", "categoryId", "categoryName", "price", "description", "frontImage"]
    ];


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'productName' => 'required',
            'categoryId' => 'required|integer',
            'categoryName' => 'required|string',
            'price' => 'required|integer|min:1',
            'description' => 'required',
            'frontImage' => 'required',
        ];
    }

    public function messages(): array
    {
        return [
            'productName.required' => '产品名称必填',
            'categoryId.required' => '产品分类必选',
            'categoryName.required' => '产品分类必选',
            'price.required' => '产品价格必填',
            'price.integer' => '产品价格必须是整数',
            'price.min' => '产品价格必须最小为1分钱',
            'description.required' => '产品简介必填',
            'frontImage.required' => '产品封面图必传'
        ];
    }
}