<?php
/**
 * Class GetProductListReq
 * @package App\Domain\Request
 * Desc:
 * created by: lhw at 2022/9/9 11:25
 */

namespace App\Domain\Request;


use Hyperf\Validation\Request\FormRequest;

class GetProductListReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'pageNum' => 'required|integer',
            'pageSize' => 'required|integer',
            'productName' => 'string',
            'productCode' => 'string',
            'onSale' => 'integer',
            'beginTime' => 'date',
            'endTime' => 'after_or_equal|beginTime'
        ];
    }

    public function messages(): array
    {
        return [
            'pageNum.required' => '缺少页码',
            'pageSize.required' => '缺少每页大小',
        ];
    }
}