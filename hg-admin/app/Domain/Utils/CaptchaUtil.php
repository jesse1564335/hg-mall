<?php

namespace App\Domain\Utils;

use EasySwoole\VerifyCode\Conf;
use EasySwoole\VerifyCode\VerifyCode;

class CaptchaUtil
{
    /**
     * desc: 生成图形验证码
     * created by: lhw at 2022/7/29 14:56
     * @return array
     */
    public static function getCaptchaImg(): array
    {
        $conf = new Conf();
        $code = new VerifyCode($conf);
        $result = $code->DrawCode();
        $imgCode = $result->getImageCode();
        $img = $result->getImageBase64();
        return [$imgCode, $img];
    }
}