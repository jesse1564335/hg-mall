<?php
/**
 * Class TreeSelect
 * @package App\Domain\Utils
 * created by: lhw at 2022/7/26 19:28
 */

namespace App\Domain\Utils;


use App\Model\SysMenu;

class TreeSelect {

    public static function menuTree(?array $data): array
    {
        $tree = [];
        if (!empty($data)) {
            foreach ($data as $menu) {
                array_push($tree, [
                    "id" => $menu->menu_id,
                    "label" => $menu->menu_name,
                    "children" => self::menuTree($menu->children)
                ]);
            }
        }
        return $tree;
    }

    public static function categoryTree(?array $data): array
    {
        $tree = [];
        if (!empty($data)) {
            foreach ($data as $category) {
                array_push($tree, [
                    "id" => $category["id"],
                    "label" => $category["category_name"],
                    "children" => self::categoryTree($category["children"] ?? [])
                ]);
            }
        }
        return $tree;
    }
}