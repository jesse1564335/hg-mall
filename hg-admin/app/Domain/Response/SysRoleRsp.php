<?php

namespace App\Domain\Response;

use App\Model\SysRole;

class SysRoleRsp extends BaseRsp
{
    public function stdoutFormatter(SysRole $model): array
    {
        return [
            'roleId' => $model->getId(),
            'roleName' => $model->getRoleName(),
            'roleKey' => $model->getRoleKey(),
            'status' => "".$model->getStatus(),
            'roleSort' => $model->getRoleSort(),
            'description' => $model->getDescription(),
            'createTime' => $model->getCreateTime(),
            'menuCheckStrictly' => $model->getMenuCheckStrictly() == 1
        ];
    }

    public function formatList($models): array
    {
        $result = [];
        foreach ($models as $model) {
            $result[] = $this->stdoutFormatter($model);
        }
        return $result;
    }
}