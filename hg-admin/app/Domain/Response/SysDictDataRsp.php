<?php

namespace App\Domain\Response;

use App\Model\SysDictData;

class SysDictDataRsp extends BaseRsp
{
    public function stdoutFormatter(SysDictData $model): array
    {
        return [
            'dictCode' => $model->getDictCode(),
            'dictSort' => $model->getDictSort(),
            'dictLabel' => $model->getDictLabel(),
            'dictValue' =>$model->getDictValue(),
            'dictType' => $model->getDictType(),
            'isDefault' => $model->getIsDefault(),
            'cssClass' => $model->getCssClass(),
            'listClass' => $model->getListClass(),
            'status' => "".$model->getStatus(),
            'remark' => $model->getRemark(),
            'createTime' => $model->getCreateTime(),
            'updateTime' => $model->getUpdateTime()
        ];
    }

    public function arrayStdoutFormatter(array $model): array
    {
        return [
            'dictCode' => $model['dict_code'],
            'dictSort' => $model['dict_sort'],
            'dictLabel' => $model['dict_label'],
            'dictValue' => $model['dict_value'],
            'dictType' => $model['dict_type'],
            'isDefault' => $model['is_default'],
            'cssClass' => $model['css_class'],
            'listClass' => $model['list_class'],
            'status' => $model['status'],
            'createTime' => $model['create_time'],
            'updateTime' => $model['update_time'],
            'remark' => $model['remark']
        ];
    }

    public function formatList($models, $isObjects = true): array
    {
        $result = [];
        if ($isObjects) {
            foreach ($models as $model) {
                $result[] = $this->stdoutFormatter($model);
            }
        } else {
            foreach ($models as $model) {
                $result[] = $this->arrayStdoutFormatter($model);
            }
        }
        return $result;
    }
}