<?php

namespace App\Domain\Response;

use App\Model\SysCrontab;

class SysCrontabRsp extends BaseRsp
{
    public function stdoutFormatter(SysCrontab $model): array
    {
        return [
            'jobId' => $model->getJobId(),
            'jobName' => $model->getJobName(),
            'jobRule' => $model->getJobRule(),
            'jobType' =>$model->getJobType(),
            'jobCallback' => json_decode($model->getJobCallback(), true),
            'jobCallbackStr' => $model->getJobCallback(),
            'jobMemo' => $model->getJobMemo(),
            'jobEnable' => "".$model->getJobEnable(),
            'singleton' => $model->getSingleton(),
            'onOneServer' => $model->getOnOneServer(),
            'mutexPool' => $model->getMutexPool(),
            'mutexExpires' => $model->getMutexExpires(),
            'createBy' => $model->getCreateBy(),
            'createTime' => $model->getCreateTime(),
            'updateBy' => $model->getUpdateBy(),
            'updateTime' => $model->getUpdateTime()
        ];
    }

    public function modelFormatter($models): array {
        $result = [];
        foreach ($models as $model) {
            $result[] = $model;
        }
        return $result;
    }

    public function formatList($models): array
    {
        $result = [];
        foreach ($models as $model) {
            $result[] = $this->stdoutFormatter($model);
        }
        return $result;
    }
}