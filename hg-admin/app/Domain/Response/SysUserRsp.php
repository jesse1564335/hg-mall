<?php
/**
 * Class SysUserRsp
 * @package App\Domain\Response
 * Desc:
 * created by: dell at 2022/8/1 11:07
 */

namespace App\Domain\Response;


use App\Common\Helper;
use App\Model\SysUser;
use Hyperf\Paginator\LengthAwarePaginator;

class SysUserRsp extends BaseRsp
{
    public function stdoutFormatter(SysUser $model): array
    {
        return [
            'userId' => $model->id,
            'userName' => $model->name,
            'email' => $model->email,
            'phone' => Helper::instance()->hidePhone($model->phone),
            'sex' => "".$model->gender,
            'status' => "".$model->status,
            'remark' => $model->remark,
            'createTime' => $model->create_time,
        ];
    }

    public function formatList($models): array
    {
        $result = [];
        foreach ($models as $model) {
            $result[] = $this->stdoutFormatter($model);
        }
        return $result;
    }


}