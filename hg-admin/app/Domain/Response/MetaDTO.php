<?php

namespace App\Domain\Response;

class MetaDTO
{
    /**
     * 设置该路由在侧边栏和面包屑中展示的名字
     */
    private string $title;

    /**
     * 设置该路由的图标，对应路径src/assets/icons/svg
     */
    private string $icon;

    /**
     * 设置为true，则不会被 <keep-alive>缓存
     */
    private bool $noCache;

    /**
     * @param string $title
     * @param string $icon
     * @param bool $noCache
     */
    public function __construct(string $title, string $icon, bool $noCache)
    {
        $this->title = $title;
        $this->icon = $icon;
        $this->noCache = $noCache;
    }

    public function convertToArray(): array
    {
        return [
            "title" => $this->getTitle(),
            "icon" => $this->getIcon(),
            "noCache" => $this->isNoCache()
        ];
    }


    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon(string $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * @return bool
     */
    public function isNoCache(): bool
    {
        return $this->noCache;
    }

    /**
     * @param bool $noCache
     */
    public function setNoCache(bool $noCache): void
    {
        $this->noCache = $noCache;
    }


}