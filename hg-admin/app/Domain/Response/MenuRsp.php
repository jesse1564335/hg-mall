<?php

namespace App\Domain\Response;

use App\Model\SysMenu;

class MenuRsp
{
    public function stdoutFormatter(SysMenu $model): array
    {
        return [
            'menuId' => $model->menu_id,
            'menuName' => $model->menu_name,
            'menuType' => $model->menu_type,
            'parentId' => $model->parent_id,
            'sort' =>$model->sort,
            'createTime' => $model->create_time,
            'icon' => $model->icon,
            'status' => "".$model->status,
            'perms' => $model->perms,
            'path' => $model->path,
            'component' => $model->component,
            'isVisible' => "".$model->is_visible,
            'isFrame' => "".$model->is_frame,
            'children' => $model->children
        ];
    }

    public function formatList($models): array
    {
        $result = [];
        foreach ($models as $model) {
            $result[] = $this->stdoutFormatter($model);
        }
        return $result;
    }
}