<?php

namespace App\Domain\Response;

class RouterDTO
{
    /**
     * 路由名字
     */
    private string $name;

    /**
     * 路由地址
     */
    private string $path;

    /**
     * 是否隐藏路由，当设置 true 的时候该路由不会再侧边栏出现
     */
    private bool $hidden;

    /**
     * 重定向地址，当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
     */
    private ?string $redirect = "";

    /**
     * 组件地址
     */
    private string $component;

    /**
     * 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
     */
    private ?bool $alwaysShow = false;

    /**
     * 其他元素
     */
    private MetaDTO $meta;

    /**
     * 子路由
     */
    private ?array $children = [];


    public function convertToArray(): array {
        return [
            "name" => $this->getName(),
            "path" => $this->getPath(),
            "component" => $this->getComponent(),
            "hidden" => $this->getHidden(),
            "meta" => $this->getMeta()->convertToArray(),
            "redirect" => $this->getRedirect(),
            "alwaysShow" => $this->getAlwaysShow(),
            "children" => $this->getChildren()
        ];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return bool
     */
    public function getHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     */
    public function setHidden(bool $hidden): void
    {
        $this->hidden = $hidden;
    }

    /**
     * @return ?string
     */
    public function getRedirect(): ?string
    {
        return $this->redirect;
    }

    /**
     * @param string $redirect
     */
    public function setRedirect(string $redirect): void
    {
        $this->redirect = $redirect;
    }

    /**
     * @return string
     */
    public function getComponent(): string
    {
        return $this->component;
    }

    /**
     * @param string $component
     */
    public function setComponent(string $component): void
    {
        $this->component = $component;
    }

    /**
     * @return ?bool
     */
    public function getAlwaysShow(): ?bool
    {
        return $this->alwaysShow;
    }

    /**
     * @param bool $alwaysShow
     */
    public function setAlwaysShow(bool $alwaysShow): void
    {
        $this->alwaysShow = $alwaysShow;
    }

    /**
     * @return MetaDTO
     */
    public function getMeta(): MetaDTO
    {
        return $this->meta;
    }

    /**
     * @param MetaDTO $meta
     */
    public function setMeta(MetaDTO $meta): void
    {
        $this->meta = $meta;
    }

    /**
     * @return ?array
     */
    public function getChildren(): ?array
    {
        return $this->children;
    }

    /**
     * @param array $children
     */
    public function setChildren(array $children): void
    {
        $this->children = $children;
    }



}