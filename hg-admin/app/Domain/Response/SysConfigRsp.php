<?php
/**
 * Class SysConfigRsp
 * @package App\Domain\Response
 * Desc:
 * created by: lhw at 2022/8/8 15:54
 */

namespace App\Domain\Response;


use App\Model\SysConfig;

class SysConfigRsp extends BaseRsp
{
    public function stdoutFormatter(SysConfig $model): array
    {
        return [
            'configId' => $model->getConfigId(),
            'configName' => $model->getConfigName(),
            'configKey' => $model->getConfigKey(),
            'configType' =>$model->getConfigType(),
            'configValue' => $model->getConfigValue(),
            'remark' => $model->getRemark(),
            'createBy' => $model->getCreateBy(),
            'createTime' => $model->getCreateTime(),
            'updateBy' => $model->getUpdateBy(),
            'updateTime' => $model->getUpdateTime()
        ];
    }


    public function formatList($models): array
    {
        $result = [];
        foreach ($models as $model) {
            $result[] = $this->stdoutFormatter($model);
        }
        return $result;
    }
}