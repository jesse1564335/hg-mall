<?php

namespace App\Domain\Response;

use App\Model\SysDictType;

class SysDictTypeRsp extends BaseRsp
{
    public function stdoutFormatter(SysDictType $model): array
    {
        return [
            'dictId' => $model->getDictId(),
            'dictName' => $model->getDictName(),
            'dictType' => $model->getDictType(),
            'remark' => $model->getRemark(),
            'status' => "" . $model->getStatus(),
            'createTime' => $model->getCreateTime(),
            'updateTime' => $model->getUpdateTime()
        ];
    }

    public function formatList($models): array
    {
        $result = [];
        foreach ($models as $model) {
            $result[] = $this->stdoutFormatter($model);
        }
        return $result;
    }
}