<?php


namespace App\Services;


/**
 * Interface SysMenuService
 * @package App\Services
 * created by: lhw at 21/7/2022 9:36 下午
 */
interface SysMenuService
{

    /**
     * desc: 添加菜单信息
     * @param array $params
     * created by: lhw at 21/7/2022 9:39 下午
     * @return bool
     */
    public function insertMenu(array $params): bool;

    /**
     * desc:更新菜单信息
     * @param array $params
     * created by: lhw at 21/7/2022 9:40 下午
     * @return bool
     */
    public function updateMenu(array $params): bool;

    /**
     * desc: 根据菜单ID删除菜单
     * @param int $menuId
     * created by: lhw at 21/7/2022 10:10 下午
     * @return int
     */
    public function deleteMenuById(int $menuId): int;

    /**
     * desc: 查询获取菜单列表
     * @param int $userId
     * created by: lhw at 2022/7/24 18:34
     * @param ?array $params
     * @param bool $isFormat
     * @return array|object
     */
    public function selectMenuList(int $userId, ?array $params, bool $isFormat = true);

    /**
     * desc: 根据菜单ID查询信息
     * @param int $menuId
     * created by: lhw at 2022/7/24 20:38
     * @return array
     */
    public function selectMenuById(int $menuId): array;

    /**
     * desc: 根据角色ID查询菜单信息
     * * @param int $roleId
     * created by: lhw at 2022/7/25 16:55
     * @return mixed
     */
    public function selectMenuListByRoleId(int $roleId): array;

    /**
     * desc: 菜单是否存在子节点
     * @param int $menuId
     * created by: lhw at 2022/7/24 21:23
     * @return bool
     */
    public function hasChildByMenuId(int $menuId): bool;

    /**
     * desc: 菜单是否已分配有角色
     * @param int $menuId
     * created by: lhw at 2022/7/24 21:33
     * @return bool
     */
    public function checkMenuExistRole(int $menuId): bool;

    /**
     * desc: 构建前端所需要下拉树结构
     * * @param $params
     * created by: lhw at 2022/7/26 10:52
     * @return array
     */
    public function buildMenuTreeSelect($params): array;

    /**
     * desc:构建menu tree
     * * @param $menus
     * created by: lhw at 2022/7/26 13:48
     * @return array
     */
    public function buildMenuTree($menus): array;

    /**
     * desc: 构建前端所需要的菜单
     * @param $menus
     * created by: lhw at 2022/7/28 15:10
     * @return array
     */
    public function buildMenus($menus): array;

    /**
     * desc: 根据用户id查询菜单树结构
     * @param int $userId
     * created by: lhw at 2022/7/27 13:59
     * @return array
     */
    public function selectMenuTreeByUserId(int $userId): array;

    /**
     * desc: 根据用户id查询权限标识
     * @param int $userId
     * created by: lhw at 2022/8/1 14:34
     * @return array
     */
    public function selectMenuPermsByUserId(int $userId): array;
}
