<?php

namespace App\Services;

use App\Domain\Request\LoginReq;

interface LoginService
{

    /**
     * desc: 生成图形验证信息
     * created by: lhw at 2022/7/29 15:11
     * @return array
     */
    public function makeCaptchaImg(): array;

    /**
     * desc: 登录
     * @param LoginReq $login
     * @param bool $captchaEnabled
     * created by: lhw at 2022/7/29 14:08
     * @return string
     */
    public function login(LoginReq $login, bool $captchaEnabled): string;
}