<?php

namespace App\Services;

use App\Constants\CacheConstant;
use App\Constants\ErrorCode;
use App\Domain\Dao\SysUserDao;
use App\Exception\BusinessException;
use App\Model\SysUser;
use App\Services\Impl\SysMenuServiceImpl;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Redis\Redis;
use Hyperf\Utils\Codec\Json;
use Hyperf\Utils\Traits\StaticInstance;

class UserAuth
{
    use StaticInstance;

    const AUTH_TOKEN = 'Authorization';

    /**
     * @var int
     */
    protected int $userId = 0;

    /**
     * @var string|null
     */
    protected ?string $token;

    /**
     * @Inject()
     * @var SysMenuServiceImpl
     */
    private SysMenuServiceImpl $sysMenuService;

    /**
     * 初始化用户数据
     * @param SysUser $sysUser
     * @param string|null $token
     * @return $this
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function init(SysUser $sysUser, ?string $token = null): UserAuth
    {
        $this->userId = $sysUser->id;
        $this->token = $token ?? md5(uniqid() . time());

        di()->get(Redis::class)->set($this->getCacheKey(), Json::encode([
            "userId" => $this->userId,
            "username" => $sysUser->name,
            "perms" => $this->sysMenuService->selectMenuPermsByUserId($this->userId)
        ]), CacheConstant::USER_AUTH_TTL);
        return $this;
    }

    /**
     * @param $token
     * @return $this
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function reload($token): UserAuth
    {
        $this->token = $token;
        $redis = di()->get(Redis::class);
        $key = $this->getCacheKey();
        if (!$redis->exists($key)) {
            throw new BusinessException(ErrorCode::TOKEN_INVALID, ErrorCode::getMessage(ErrorCode::TOKEN_INVALID));
        }
        $cache =$redis->get($key);
        if ($cache && $data = Json::decode($cache)) {
            $this->userId = intval($data['userId'] ?? 0);
        }
        return $this;
    }

    /**
     * 校验token
     * @return $this
     */
    public function authCheck(): UserAuth
    {
        if ($this->getUserId() === 0) {
            throw new BusinessException(ErrorCode::TOKEN_INVALID, ErrorCode::getMessage(ErrorCode::TOKEN_INVALID));
        }
        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function getSysUser(): SysUser
    {
        $userId = $this->authCheck()->getUserId();
        return (New SysUserDao())->fetchData(["first"=>true, "userId"=>$userId]);
    }

    protected function getCacheKey(): string
    {
        if (empty($this->token)) {
            throw new BusinessException(ErrorCode::INVALID_TOKEN_INIT, ErrorCode::getMessage(ErrorCode::INVALID_TOKEN_INIT));
        }

        return CacheConstant::USER_AUTH_KEY . $this->token;
    }

    public function clearCache(?string $token) {
        if (!empty($token)) {
           $key = CacheConstant::USER_AUTH_KEY . $token;
           di()->get(Redis::class)->del($key);
        }
    }
}
