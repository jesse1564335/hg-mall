<?php

namespace App\Services;

interface SysDictDataService
{

    /**
     * desc:分页查询
     * @param array $conditions
     * created by: lhw at 2022/8/6 15:50
     * @return mixed
     */
    public function getPageList(array $conditions);

    /**
     * desc: 根据字典编码查询信息
     * @param int $dictCode
     * created by: lhw at 2022/8/7 15:06
     * @return mixed
     */
    public function getInfo(int $dictCode);

    /**
     * desc: 更新字典数据
     * @param array $params
     * created by: lhw at 2022/8/7 15:34
     * @return bool
     */
    public function updateInfo(array $params): bool;

    /**
     * desc: 删除
     * @param string $dictCode
     * created by: lhw at 2022/8/7 16:04
     * @return bool
     */
    public function delete(string $dictCode): bool;

    /**
     * desc: 新增字典数据
     * @param array $params
     * created by: lhw at 2022/8/7 16:31
     * @return bool
     */
    public function add(array $params): bool;

    /**
     * desc: 根据字典类型统计数量
     * @param string $dictType
     * created by: lhw at 2022/8/7 17:04
     * @return int
     */
    public function countDictDataByType(string $dictType): int;
}