<?php
/**
 * Interface SysUserService
 * @package App\Services
 * Desc:
 * created by: lhw at 2022/8/2 14:59
 */

namespace App\Services;


interface SysUserService
{

    /**
     * desc: 用户列表
     * @param array $condition
     * created by: lhw at 2022/8/2 15:40
     * @return mixed
     */
    public function getUserList(array $condition);

    /**
     * desc: 根据用户ID获取详细信息
     * @param int $userId
     * created by: lhw at 2022/8/2 21:49
     * @return mixed
     */
    public function getUserById(int $userId);

    /**
     * desc: 设置是否禁用用户
     * @param int $userId
     * @param int $status
     * @return int
     */
    public function setIsDisable(int $userId, int $status): int;

    /**
     * desc: 新增用户
     * @param array $condition
     * created by: lhw at 2022/8/13 23:44
     * @return bool
     */
    public function add(array $condition): bool;

    /**
     * desc: 更新用户
     * @param array $condition
     * created by: lhw at 2022/8/14 10:15
     * @return bool
     */
    public function update(array $condition): bool;

    /**
     * desc: 删除/批量删除
     * @param string $userIds
     * created by: lhw at 2022/8/14 11:48
     * @return bool
     */
    public function delete(string $userIds): bool;

    /**
     * desc: 重置密码
     * @param int $userId
     * @param string $password
     * created by: lhw at 2022/8/14 11:50
     * @return bool
     */
    public function resetPassword(int $userId, string $password): bool;

    /**
     * desc: 更新个人信息
     * @param array $condition
     * created by: lhw at 2022/8/14 14:03
     * @return bool
     */
    public function updateUserProfile(array $condition): bool;

    /**
     * desc:设置用户头像
     * @param int $userId
     * @param string $url
     * created by: lhw at 2022/8/17 16:25
     * @return bool
     */
    public function setAvatar(int $userId, string $url): bool;
}