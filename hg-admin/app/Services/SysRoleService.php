<?php

namespace App\Services;


use App\Model\SysRole;

interface SysRoleService
{

    /**
     * desc: 分页查询
     * @param array $params
     * created by: lhw at 2022/8/17 22:11
     * @return mixed
     */
    public function selectRoleList(array $params);

    /**
     * desc: 根据角色ID获取信息
     * @param int $roleId
     * created by: lhw at 2022/8/18 10:54
     * @return mixed
     */
    public function getRoleInfo(int $roleId);

    /**
     * desc: 删除角色
     * @param string $roleIds
     * created by: lhw at 2022/8/19 14:13
     * @return bool
     */
    public function deleteRole(string $roleIds): bool;

    /**
     * desc: 通过角色ID查询角色使用数量
     * @param int $roleId
     * created by: lhw at 2022/8/19 14:18
     * @return int
     */
    public function countUserRoleByRoleId(int $roleId): int;

    /**
     * desc: 校验角色是否允许操作
     * @param SysRole $role
     * created by: lhw at 2022/8/19 14:23
     */
    public function checkRoleAllowed(SysRole $role): void;

    /**
     * desc: 更新角色状态
     * @param array $params
     * created by: lhw at 2022/8/19 16:51
     * @return bool
     */
    public function updateRoleStatus(array $params): bool;

    /**
     * desc: 更新角色信息
     * @param array $params
     * created by: lhw at 2022/8/20 14:38
     * @return bool
     */
    public function updateRole(array $params): bool;

    /**
     * desc: 新增角色
     * @param array $params
     * created by: lhw at 2022/8/21 11:23
     * @return bool
     */
    public function addRole(array $params): bool;

    /**
     * desc: 根据用户查角色信息
     * @param int $userId
     * created by: lhw at 2022/8/1 22:59
     * @return mixed
     */
    public function selectRolePermissionByUserId(int $userId);

    /**
     * desc: 获取全部角色信息
     * created by: lhw at 2022/8/2 22:36
     * @return mixed
     */
    public function selectRoleAll();

    /**
     * desc:根据用户ID获取角色列表信息
     * @param int $userId
     * created by: lhw at 2022/8/2 23:13
     * @return array
     */
    public function selectRoleListByUserId(int $userId): array;
}