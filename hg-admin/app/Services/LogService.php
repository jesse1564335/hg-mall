<?php

namespace App\Services;

use App\Common\Helper;
use App\Domain\Request\LoginReq;
use App\Model\SysLog;

class LogService
{

    /**
     * desc: 记录登录信息
     * @param string $req
     * @param int $status
     * @param string $msg
     * created by: lhw at 2022/7/31 14:29
     */
    public static function recordLoginInfo(array $params, int $status, string $msg) {
        $model = new SysLog();
        $model->setIp($params["ip"]);
        $model->setAddress($params["ipAddress"]);
        $model->setBrowser($params["browser"]);
        $model->setUsername($params["username"]);
        $model->setStatus($status);
        $model->setMsg($msg);
        $model->setCreateTime(date('Y-m-d H:i:s'));
        $model->save();
    }

    /**
     * desc: 获取登录客户端相关信息
     * @param $req
     * created by: lhw at 2022/7/31 15:19
     * @return array
     */
    public static function getLoginWebInfo($req): array
    {
        $ip = Helper::instance()->getIp($req);
        $ipAddress = Helper::instance()->getIpAddress($ip);
        $browser = Helper::instance()->getBrowser($req);
        return ["ip" => $ip, "ipAddress" => $ipAddress, "browser" => $browser];
    }
}