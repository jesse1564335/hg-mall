<?php


use repository\OrderRpcService;

class OrderService
{

    /**
     * @Inject
     * @var OrderRpcService
     */
    private OrderRpcService $orderRpcService;

    public function getOrderList() {
        return $this->orderRpcService->getOrderList([]);
    }
}
