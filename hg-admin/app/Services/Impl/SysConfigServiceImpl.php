<?php
/**
 * Class SysConfigServiceImpl
 * @package App\Services\Impl
 * Desc:
 * created by: lhw at 2022/8/8 14:58
 */

namespace App\Services\Impl;

use App\Constants\CacheConstant;
use App\Constants\CommonCode;
use App\Constants\ErrorCode;
use App\Constants\UserConstant;
use App\Domain\Dao\SysConfigDao;
use App\Exception\BusinessException;
use App\Services\SysConfigService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Redis\Redis;


class SysConfigServiceImpl implements SysConfigService
{

    /**
     * @Inject()
     * @var SysConfigDao
     */
    private SysConfigDao $configDao;


    public function list(array $params) {
        if (!isset($params["sortColumns"])) {
            $params["sortColumns"] = [
                [
                    "sortColumn" => "create_time",
                    "sort" => CommonCode::MODEL_SORT_DESC
                ]
            ];
        }
        if (!isset($params["pageSize"])) {
            $params["pageNum"] = 1;
            $params["pageSize"] = 10;
        }
        return $this->configDao->getPageList($params, ["*"]);
    }

    public function add(array $params): bool {
       $res = $this->configDao->addConfig($params);
       if ($res) {
           di()->get(Redis::class)->set($this->getCacheKey($params["configKey"]), $params["configValue"]);
       }
       return $res;
    }

    public function getInfo(int $configId) {
        return $this->configDao->getConfigInfo($configId);
    }

    public function update(array $params): bool {
        $res = $this->configDao->updateConfig($params);
        if ($res) {
            di()->get(Redis::class)->set($this->getCacheKey($params["configKey"]), $params["configValue"]);
        }
        return $res;
    }

    public function checkConfigKeyUnique(string $configKey): int {
        $params = [
            'configKeyUnique' => true,
            'configKeyEq' => $configKey,
            'first' => true
        ];
        $info = $this->configDao->checkConfigKeyUnique($params);
        if (empty($info)) {
            return UserConstant::UNIQUE;
        }
        return UserConstant::NOT_UNIQUE;
    }

    public function delete(string $configIds): bool {
        if (empty($configIds)) {
            throw new BusinessException(ErrorCode::PARAMS_ERROR, ErrorCode::getMessage(ErrorCode::PARAMS_ERROR));
        }
        $ids = explode(",", trim($configIds, ","));
        $params = [
          "batchDelete" => true,
          "configIds"  => $ids
        ];
        $res = $this->configDao->deleteConfig($params);
        if ($res) {
            di()->get(Redis::class)->del(CacheConstant::SYS_CONFIG_KEY."*");
        }
        return $res;
    }

    public function selectConfigByKey(string $configKey): string {
        $redis = di()->get(Redis::class);
        $configValue = "";
        $cacheKey = $this->getCacheKey($configKey);
        if ($redis->exists($cacheKey)) {
            $configValue = $redis->get($cacheKey);
        } else {
            $params = [
                "first" => true,
                "configKeyUnique" => true,
                "configKeyEq" => $configKey
            ];
            $info = $this->configDao->checkConfigKeyUnique($params);
            if (!empty($info)) {
                $configValue = $info->config_value;
            }
        }
        return $configValue;
    }

    public function clearCache(): void {
        $key = CacheConstant::SYS_CONFIG_KEY . "*";
        $keys = di()->get(Redis::class)->keys($key);
        di()->get(Redis::class)->del($keys);
    }

    /**
     * desc:获取缓存键名
     * @param string $configKey
     * created by: lhw at 2022/8/10 16:28
     * @return string
     */
    private function getCacheKey(string $configKey): string {
        return CacheConstant::SYS_CONFIG_KEY . $configKey;
    }
}