<?php

namespace App\Services\Impl;

use App\Common\Helper;
use App\Constants\CacheConstant;
use App\Constants\ErrorCode;
use App\Domain\Dao\SysUserDao;
use App\Domain\Request\LoginReq;
use App\Domain\Utils\CaptchaUtil;
use App\Exception\BusinessException;
use App\Job\RecordLogJob;
use App\Services\LoginService;
use App\Services\LogService;
use App\Services\UserAuth;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Redis\Redis;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;


class LoginServiceImpl implements LoginService
{

    /**
     * @Inject()
     * @var SysUserDao
     */
    private SysUserDao $sysUserDao;


    public function makeCaptchaImg(): array {
        $uuid = Helper::instance()->simpleUUID();
        $key = CacheConstant::CAPTCHA_KEY.$uuid;
        list($code, $img) = CaptchaUtil::getCaptchaImg();
        di()->get(Redis::class)->setex($key, CacheConstant::CAPTCHA_TTL, strtolower($code));
        return [
          "uuid" => $uuid,
          "img" => $img
        ];
    }


    public function login(LoginReq $login, bool $captchaEnabled): string {
        $req = $login->all();
        $verifyKey = CacheConstant::CAPTCHA_KEY.$req["uuid"];
        try {
            $redis = di()->get(Redis::class);
        } catch (NotFoundExceptionInterface | ContainerExceptionInterface $e) {
            throw new BusinessException(ErrorCode::SERVER_ERROR, $e->getMessage());
        }
        $captcha = $redis->get($verifyKey);
        $redis->del($verifyKey);
        if ($captchaEnabled) {
            if ($captcha == null) {
                $code = ErrorCode::MSG_CODE_EXPIRED;
                $this->processLoginCaptchaError($login, $code, $req["username"]);
                throw new BusinessException($code, ErrorCode::getMessage($code));
            }
            if ($captcha != strtolower($req["code"])) {
                $code = ErrorCode::MSG_CODE_ERROR;
                $this->processLoginCaptchaError($login, $code, $req["username"]);
                throw new BusinessException($code, ErrorCode::getMessage($code));
            }
        }
        //用户验证
        $loginUser = $this->sysUserDao->getUserInfoByName($req["username"]);
        if (empty($loginUser)) {
            $this->processLoginCaptchaError($login, ErrorCode::USER_NOT_EXIST, $req["username"]);
            throw new BusinessException(ErrorCode::USER_NOT_EXIST, ErrorCode::getMessage(ErrorCode::USER_NOT_EXIST));
        }
        if ($loginUser->password != Helper::instance()->md5Pwd($req["password"], $loginUser->salt)) {
            $this->processLoginCaptchaError($login, ErrorCode::USER_PWD_ERROR, $req["username"]);
            throw new BusinessException(ErrorCode::USER_PWD_ERROR, ErrorCode::getMessage(ErrorCode::USER_PWD_ERROR));
        }
        try {
            $this->processLoginCaptchaError($login, ErrorCode::SUCCESS, $req["username"]);
            return UserAuth::instance()->init($loginUser)->getToken();
        } catch (NotFoundExceptionInterface | ContainerExceptionInterface $e) {
            throw new BusinessException(ErrorCode::SERVER_ERROR, $e->getMessage());
        }
    }

    /**
     * desc: 处理验证码错误异常
     * @param LoginReq $loginReq
     * @param int $code
     * @param string $username
     * created by: lhw at 2022/7/31 17:37
     */
    private function processLoginCaptchaError(LoginReq $loginReq, int $code, string $username) {
        $msg = ErrorCode::getMessage($code);
        $params = LogService::getLoginWebInfo($loginReq);
        $params["username"] = $username;
        queue_push(new RecordLogJob($params, $code, $msg));
    }
}