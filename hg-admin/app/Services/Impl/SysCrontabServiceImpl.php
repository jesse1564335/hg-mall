<?php

namespace App\Services\Impl;

use App\Common\Log\Logger;
use App\Constants\CommonCode;
use App\Constants\ErrorCode;
use App\Domain\Dao\SysCrontabDao;
use App\Domain\Response\SysCrontabRsp;
use App\Exception\BusinessException;
use App\Model\SysCrontab;
use App\Services\SysCrontabService;
use App\Services\UserAuth;
use Hyperf\Crontab\Crontab;
use Hyperf\Crontab\CrontabManager;
use Hyperf\Crontab\Scheduler;
use Hyperf\Crontab\Strategy\StrategyInterface;
use Hyperf\Di\Annotation\Inject;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;

/**
 * Class SysCrontabServiceImpl
 * created by: lhw at 2022/8/25 19:52
 */
class SysCrontabServiceImpl implements SysCrontabService
{

    /**
     * @Inject()
     * @var Logger
     */
    private Logger $logger;

    /**
     * @Inject()
     * @var SysCrontabDao
     */
    private SysCrontabDao $crontabDao;

    /**
     * @Inject()
     * @var SysCrontabRsp
     */
    private SysCrontabRsp $crontabRsp;

    /**
     * @Inject()
     * @var Scheduler
     */
    protected $scheduler;

    /**
     * @Inject()
     * @var StrategyInterface
     */
    protected $strategy;

    /**
     * @Inject()
     * @var CrontabManager
     */
    protected CrontabManager $crontabManager;


    public function selectAllCrontab(array $params = null): array {
        if (!isset($params["sortColumns"])) {
            $params["sortColumns"] = [
                [
                    "sortColumn" => "job_id",
                    "sort" => CommonCode::MODEL_SORT_DESC
                ],
                [
                    "sortColumn" => "update_time",
                    "sort" => CommonCode::MODEL_SORT_DESC
                ]
            ];
        }
        $params["all"] = true;
        return $this->crontabRsp->modelFormatter(
            $this->crontabDao->fetchData($params)
        );
    }

    public function addCrontab(array $params): bool {
        $crontab = $this->crontabDao->getModel();
        $crontab->setJobName($params["jobName"]);
        $crontab->setJobCallback(json_encode([$params["jobCallback"]], true));
        $crontab->setJobRule($params["jobRule"]);
        $crontab->setJobEnable($params["jobEnable"]);
        $crontab->setCreateBy(UserAuth::instance()->getSysUser()->getName());
        $crontab->setCreateTime(date("Y-m-d H:i:s"));
        $this->processCrontab($crontab);
        var_dump(json_decode($crontab->job_callback, true));
        return $crontab->save();
    }

    public function updateCrontab(array $params): bool {
        $crontab = $this->crontabDao->getModel()->find($params["jobId"]);
        $crontab->setJobName($params["jobName"]);
        $crontab->setJobCallback(json_encode($params["jobCallback"], true));
        $crontab->setJobRule($params["jobRule"]);
        $crontab->setJobEnable($params["jobEnable"]);
        $crontab->setUpdateBy(UserAuth::instance()->getSysUser()->getName());
        $crontab->setUpdateTime(date("Y-m-d H:i:s"));
        $this->processCrontab($crontab);
        var_dump(json_decode($crontab->job_callback, true));
        return $crontab->save();
    }

    public function setEnable($cronId, $enable): void {
        $model = $this->crontabDao->getModel()->find($cronId);
        if (empty($model)) {
            throw new BusinessException(ErrorCode::SERVER_ERROR, "任务不存在");
        }
        $model->setJobEnable($enable);
        $model->save();
        $this->processCrontab($model);
    }

    private function processCrontab(SysCrontab $model) {
        $crontab = $this->buildCrontabByAnnotation($model);
        if ($crontab instanceof Crontab && $this->crontabManager->register($crontab)) {
            $this->logger->info('Crontab %s have been registered.', [$crontab->getName()]);
        } else {
            $this->logger->info('Crontab %s 取消注册.', [$crontab->getName()]);
        }
        $crontabs = $this->scheduler->schedule();
        while (! $crontabs->isEmpty()) {
            $crontab = $crontabs->dequeue();
            $this->strategy->dispatch($crontab);
        }
        system(" ps -ef | grep " . config("app_name") .".crontab-dispatcher.0 | grep -v grep | awk '{print $1}' | xargs -r kill -9");
        $this->logger->info(config("app_name") .".crontab-dispatcher.0 进程重启", []);
    }

    private function buildCrontabByAnnotation(SysCrontab $annotation): Crontab {
        $crontab = new Crontab();
        isset($annotation->job_name) && $crontab->setName($annotation->job_name);
        isset($annotation->job_type) && $crontab->setType($annotation->job_type);
        isset($annotation->job_rule) && $crontab->setRule($annotation->job_rule);
        isset($annotation->singleton) && $crontab->setSingleton($annotation->singleton);
        isset($annotation->mutexPool) && $crontab->setMutexPool($annotation->mutexPool);
        isset($annotation->mutexExpires) && $crontab->setMutexExpires($annotation->mutexExpires);
        isset($annotation->onOneServer) && $crontab->setOnOneServer($annotation->onOneServer);
        isset($annotation->job_callback) && $crontab->setCallback(json_decode($annotation->job_callback, true));
        isset($annotation->job_memo) && $crontab->setMemo($annotation->job_memo);
        isset($annotation->job_enable) && $crontab->setEnable($annotation->job_enable == 1);
        return $crontab;
    }

    public function selectList(array $params) {
        if (!isset($params["sortColumns"])) {
            $params["sortColumns"] = [
                [
                    "sortColumn" => "job_id",
                    "sort" => CommonCode::MODEL_SORT_DESC
                ],
                [
                    "sortColumn" => "update_time",
                    "sort" => CommonCode::MODEL_SORT_DESC
                ]
            ];
        }
        if (isset($params["pageNum"])) {
            $params["pageNum"] = 1;
        }
        if (isset($params["pageSize"])) {
            $params["pageSize"] = 10;
        }
        return $this->crontabDao->pageList($params);
    }

    public function run(int $cronId): void {
        $job = $this->crontabDao->fetchData(["first"=>true, "jobId"=>$cronId], ["job_callback"])->toArray();
        if (!empty($job)) {
            $callback = json_decode($job["job_callback"], true);
            $class = make($callback[0]);
            $class->{$callback[1]}();
            $this->logger->info("the job start run one time: %s", ["jobId"=>$cronId, "cmd"=>$callback]);
        } else {
            $this->logger->info("the job run fail: job not exist", []);
        }
    }

    public function detail(int $cronId) {
        $conditions = [
          "first" => true,
          "jobId" => $cronId
        ];
        return $this->crontabDao->fetchData($conditions);
    }

}