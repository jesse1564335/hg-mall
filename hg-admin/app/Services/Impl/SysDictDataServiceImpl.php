<?php

namespace App\Services\Impl;

use App\Constants\CommonCode;
use App\Domain\Dao\SysDictDataDao;
use App\Domain\Utils\CacheUtil;
use App\Services\SysDictDataService;
use Hyperf\Di\Annotation\Inject;

class SysDictDataServiceImpl implements SysDictDataService
{
    /**
     * @Inject()
     * @var SysDictDataDao
     */
    private SysDictDataDao $sysDictDataDao;


    public function getPageList(array $conditions) {
        if (!isset($conditions['pageSize'])) {
            $conditions['pageNum'] = 1;
            $conditions['pageSize'] = 10;
        }
        if (!isset($conditions['sortColumns'])) {
            $conditions['sortColumns'] = [
                [
                    'sortColumn' => 'dict_sort',
                    'sort' => CommonCode::MODEL_SORT_ASC
                ],
                [
                    'sortColumn' => 'update_time',
                    'sort' => CommonCode::MODEL_SORT_DESC
                ]
            ];
        }
        return $this->sysDictDataDao->getListByPage($conditions);
    }

    public function getInfo(int $dictCode) {
        return $this->sysDictDataDao->getInfo($dictCode);
    }

    public function updateInfo(array $params): bool {
        return $this->sysDictDataDao->update($params);
    }

    public function add(array $params): bool {
       return $this->sysDictDataDao->add($params);
    }

    public function delete(string $dictCode): bool {
        $res = $this->sysDictDataDao->delete($dictCode);
        if ($res) {
            CacheUtil::clearDictCache();
        }
        return $res;
    }

    public function countDictDataByType(string $dictType): int {
        return $this->sysDictDataDao->countDictDataByType($dictType);
    }
}