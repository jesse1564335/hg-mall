<?php

namespace App\Services\Impl;

use App\Constants\ErrorCode;
use App\Constants\UserConstant;
use App\Domain\Dao\SysRoleDao;
use App\Domain\Dao\SysRoleHasMenusDao;
use App\Domain\Dao\SysUserRoleDao;
use App\Exception\BusinessException;
use App\Model\SysRole;
use App\Services\SysRoleService;
use App\Services\UserAuth;
use Hyperf\DbConnection\Annotation\Transactional;
use Hyperf\Di\Annotation\Inject;

class SysRoleServiceImpl implements SysRoleService
{

    /**
     * @Inject()
     * @var SysRoleDao
     */
    private SysRoleDao $sysRoleDao;

    /**
     * @Inject()
     * @var SysUserRoleDao
     */
    private SysUserRoleDao $sysUserRoleDao;

    /**
     * @Inject()
     * @var SysRoleHasMenusDao
     */
    private SysRoleHasMenusDao $roleHasMenusDao;


    public function selectRolePermissionByUserId(int $userId): array {
       $roles = $this->sysRoleDao->selectRolePermissionByUserId($userId);
       $nameSets = [];
       $idSets = [];
       foreach ($roles as $obj) {
           if (!empty($obj)) {
               array_push($nameSets, $obj->roleName);
               array_push($idSets, $obj->id);
           }
       }
       return [$nameSets, $idSets];
    }

    public function selectRoleAll() {
        return $this->sysRoleDao->getRoleAll();
    }

    public function selectRoleListByUserId(int $userId): array {
      $roleIds = $this->sysRoleDao->selectRoleListByUserId($userId)->toArray();
      if (empty($roleIds)) {
          return [];
      }
      return array_column($roleIds, "roleId");
    }

    public function selectRoleList(array $params) {
        return $this->sysRoleDao->selectRoleList($params);
    }

    public function getRoleInfo(int $roleId) {
        $condition = [
          "roleId" => $roleId,
          "first" => true
        ];
        return $this->sysRoleDao->fetchData($condition);
    }

    /**
     * @Transactional()
     */
    public function deleteRole(string $roleIds): bool {
        $roleIdsArr = explode(",", $roleIds);
        foreach ($roleIdsArr as $roleId) {
            $role = $this->getRoleInfo($roleId);
            $this->checkRoleAllowed($role);
            if ($this->countUserRoleByRoleId($roleId) >0) {
                throw new BusinessException(ErrorCode::SERVER_ERROR, $role->getRoleName()."已分配，不能删除");
            }
        }
        $this->roleHasMenusDao->deleteByRoleIds($roleIdsArr);
        return $this->sysRoleDao->deleteRole($roleIdsArr);
    }

    public function countUserRoleByRoleId(int $roleId): int {
        return $this->sysUserRoleDao->countUserRoleByRoleId($roleId);
    }

    public function checkRoleAllowed(SysRole $role): void {
        if (!empty($role->getId()) && $role->isAdmin()) {
            throw new BusinessException(ErrorCode::SERVER_ERROR, "不允许操作超级管理员角色");
        }
    }

    public function updateRoleStatus(array $params): bool {
        $model = $this->sysRoleDao->getModel()->find($params["roleId"]);
        if (empty($model)) {
            throw new BusinessException(ErrorCode::SERVER_ERROR, "角色ID：".$params["roleId"] . "不存在");
        }
        $this->checkRoleAllowed($model);
        $model->setStatus($params["status"]);
        return $model->save();
    }

    /**
     * @Transactional()
     * @param array $params
     * created by: lhw at 2022/8/21 13:45
     * @return bool
     */
    public function addRole(array $params): bool {
        $model = $this->sysRoleDao->getModel();
        return $this->operatorRoleInfo($model, $params, true);
    }

    /**
     * @Transactional()
     * @param array $params
     * created by: lhw at 2022/8/21 13:48
     * @return bool
     */
    public function updateRole(array $params): bool {
        $model = $this->sysRoleDao->getModel()->find($params["roleId"]);
        $this->checkRoleAllowed($model);
        return $this->operatorRoleInfo($model, $params, false);
    }

    /**
     * desc:
     * @param SysRole $model
     * @param array $params
     * @param bool $isAddOrUpdate true:新增 false:更新
     * created by: lhw at 2022/8/21 13:55
     * @return bool
     */
    private function operatorRoleInfo(SysRole $model, array $params, bool $isAddOrUpdate): bool {
        if ($isAddOrUpdate) {
            $message = "新增角色";
            $checkModel = null;
            $checkParams = $params;
        } else {
            if (empty($model)) {
                throw new BusinessException(ErrorCode::SERVER_ERROR, "角色" . $params["roleName"] . "不存在");
            }
            $checkModel = $model;
            $checkParams = null;
            $message = "修改角色";
        }
        if (UserConstant::NOT_UNIQUE == $this->checkRoleNameUnique($checkModel, $checkParams)) {
            throw new BusinessException(ErrorCode::SERVER_ERROR, $message . $model->getRoleName() . "失败，角色名称已存在");
        }
        if (UserConstant::NOT_UNIQUE == $this->checkRoleKeyUnique($checkModel, $checkParams)) {
            throw new BusinessException(ErrorCode::SERVER_ERROR, $message . $model->getRoleName() . "失败，权限字符已存在");
        }

        $model->setStatus($params["status"]);
        $model->setRoleName($params["roleName"]);
        $model->setRoleKey($params["roleKey"]);
        $model->setDescription($params["description"] ?? "");
        $model->setRoleSort($params["roleSort"] ?? 0);
        $model->setMenuCheckStrictly($params["menuCheckStrictly"] ?? 0);
        $operator = UserAuth::instance()->getSysUser()->getName();
        $operatorDate = date("Y-m-d H:i:s");
        if ($isAddOrUpdate) {
            $model->setCreateTime($operatorDate);
            $model->setCreateBy($operator);
        } else {
            $model->setUpdateTime($operatorDate);
            $model->setUpdateBy($operator);
        }
        $model->save();
        if ($isAddOrUpdate) {
            $info =  $this->sysRoleDao->fetchData(["roleName"=>$params["roleName"], "first"=>true]);
            $roleId = $info->id;
        } else {
            $roleId = $model->getId();
            $this->roleHasMenusDao->deleteByRoleIds([$model->getId()]);
        }
        return  $this->roleHasMenusDao->addRoleMenus($roleId, $params["menuIds"]);
    }

    /**
     * desc: 校验角色名是否唯一
     * @param SysRole|null $role
     * created by: lhw at 2022/8/21 10:57
     * @param array|null $params
     * @return bool
     */
    private function checkRoleNameUnique(?SysRole $role, ?array $params): bool {
        if (!empty($role)) {
            $roleId = $role->getId() ?? -1;
            $info = $this->sysRoleDao->fetchData(["first"=>true, "roleNameEq"=>$role->getRoleName()]);
            if (!empty($info) && $info->id != $roleId) {
                return UserConstant::NOT_UNIQUE;
            }
            return UserConstant::UNIQUE;
        }
        $info = $this->sysRoleDao->fetchData(["first"=>true, "roleNameEq"=>$params["roleName"]]);
        if (!empty($info)) {
            return UserConstant::NOT_UNIQUE;
        }
        return UserConstant::UNIQUE;
    }

    /**
     * desc: 校验角色权限字符是否唯一
     * @param SysRole|null $role
     * created by: lhw at 2022/8/21 11:03
     * @param array|null $params
     * @return bool
     */
    private function checkRoleKeyUnique(?SysRole $role, ?array $params): bool {
        if (!empty($role)) {
            $roleId = $role->getId() ?? -1;
            $info = $this->sysRoleDao->fetchData(["first"=>true, "roleKeyEq"=>$role->getRoleKey()]);
            if (!empty($info) && $info->id != $roleId) {
                return UserConstant::NOT_UNIQUE;
            }
            return UserConstant::UNIQUE;
        }
        $info = $this->sysRoleDao->fetchData(["first"=>true, "roleKeyEq"=>$params["roleKey"]]);
        if (!empty($info)) {
            return UserConstant::NOT_UNIQUE;
        }
        return UserConstant::UNIQUE;
    }

}