<?php

namespace App\Services\Impl;

use App\Constants\CommonCode;
use App\Constants\ErrorCode;
use App\Domain\Dao\SysDictDataDao;
use App\Domain\Dao\SysDictTypeDao;
use App\Domain\Utils\CacheUtil;
use App\Exception\BusinessException;
use App\Services\SysDictTypeService;
use Hyperf\Di\Annotation\Inject;


class SysDictTypeServiceImpl implements SysDictTypeService
{
    /**
     * @Inject()
     * @var SysDictTypeDao
     */
    private SysDictTypeDao $sysDictTypeDao;

    /**
     * @Inject()
     * @var SysDictDataDao
     */
    private SysDictDataDao $sysDictDataDao;


    public function getPageList(array $conditions) {
        if (!isset($conditions['pageSize'])) {
            $conditions['pageNum'] = 1;
            $conditions['pageSize'] = 10;
        }
        if (!isset($conditions['sortColumns'])) {
            $conditions['sortColumns'] = [
                [
                    'sortColumn' => 'update_time',
                    'sort' => CommonCode::MODEL_SORT_DESC
                ]
            ];
        }
        return $this->sysDictTypeDao->getListByPage($conditions);
    }

    public function selectDictTypeById(int $dictId) {
        return $this->sysDictTypeDao->getInfoById($dictId);
    }

    public function selectDictDataByType(string $dictType) {
        $cache = CacheUtil::getDictCache($dictType);

        if (!empty($cache)) {
            return $cache;
        }
        $info =  $this->sysDictDataDao->getInfoByType($dictType)->toArray();
        if (!empty($info)) {
            CacheUtil::setDictCache($dictType, $info);
            return $info;
        }
        return [];
    }

    public function add(array $params): bool {
        return $this->sysDictTypeDao->addDictType($params);
    }

    public function update(array $params): bool {
        return  $this->sysDictTypeDao->updateDictType($params);
    }

    public function delete(string $dictId): bool {
        $ids = explode(",", $dictId);
        foreach ($ids as $id) {
            $dictTypeInfo = $this->selectDictTypeById($id);
            if ($this->sysDictDataDao->countDictDataByType($dictTypeInfo->getDictType()) > 0) {
                throw new BusinessException(ErrorCode::SERVER_ERROR, $dictTypeInfo->getDictName() . "已分配，不能删除");
            }
        }
        return $this->sysDictTypeDao->deleteDictType($dictId);
    }
}