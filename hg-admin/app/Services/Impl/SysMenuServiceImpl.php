<?php


namespace App\Services\Impl;


use App\Constants\UserConstant;
use App\Domain\Dao\SysMenuDao;
use App\Domain\Dao\SysRoleHasMenusDao;
use App\Domain\Response\MenuRsp;
use App\Domain\Response\MetaDTO;
use App\Domain\Response\RouterDTO;
use App\Domain\Utils\TreeSelect;
use App\Model\SysMenu;
use App\Model\SysUser;
use App\Services\SysMenuService;
use Hyperf\Di\Annotation\Inject;


class SysMenuServiceImpl implements SysMenuService
{
    /**
     * @Inject
     * @var SysMenuDao
     */
    private SysMenuDao $sysMenuDao;

    /**
     * @Inject
     * @var SysRoleHasMenusDao
     */
    private SysRoleHasMenusDao $roleHasMenusDao;

    /**
     * @Inject
     * @var MenuRsp
     */
    private MenuRsp $menuRsp;


    public function insertMenu(array $params): bool
    {
        $params['createBy'] = "admin";
        $params['updateBy'] = "admin";
        return $this->sysMenuDao->addMenu($params);
    }

    public function updateMenu(array $params): bool
    {
        $params['updateBy'] = "admin";
        return $this->sysMenuDao->updateMenu($params);
    }

    public function deleteMenuById(int $menuId): int
    {
        return $this->sysMenuDao->deleteMenuById($menuId);
    }

    public function selectMenuList(int $userId, ?array $params, bool $isFormat = true) {
        //管理员显示所有菜单
        if (SysUser::isSuperAdmin($userId)) {
            $params['all'] = true;
             $menuList = $this->sysMenuDao->getMenuList($params);
        } else {
            $params["userId"] = $userId;
            $menuList = $this->sysMenuDao->getMenuListByUserId($params);
        }
        if ($isFormat) {
            $menuList = $this->menuRsp->formatList($menuList);
        }
        return $menuList;
    }

    public function selectMenuById(int $menuId): array {
        $condition = [
          'first' => true,
          'menuId' => $menuId
        ];
        return $this->menuRsp->stdoutFormatter($this->sysMenuDao->getMenuById($condition));
    }

    public function hasChildByMenuId(int $menuId): bool {
        $condition = [
            'first' => true,
            'parentId' => $menuId
        ];
        $menu = $this->sysMenuDao->getMenuList($condition);
        if (empty($menu)) {
            return false;
        }
        return true;
    }

    public function checkMenuExistRole(int $menuId): bool {
        $condition = [
            'menuId' => $menuId,
            'count' => true
        ];
        $count = $this->roleHasMenusDao->checkMenuHasRole($condition);
        return $count > 0;
    }

    public function selectMenuListByRoleId(int $roleId): array {
        $roleArr = $this->sysMenuDao->getMenuListByRoleId($roleId);
        $idList = [];
        foreach ($roleArr as $obj) {
            array_push($idList, $obj->menu_id);
        }
        return $idList;
    }

    public function buildMenuTreeSelect($params): array {
        return TreeSelect::menuTree($this->buildMenuTree($params));
    }

    public function buildMenuTree($menus): array {
         $returnList = [];
         $tempList = [];
        foreach ($menus as  $menu) {
            array_push($tempList, $menu->getMenuId());
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!in_array($menu->getParentId(), $tempList)) {
                $this->recursionFn($menus, $menu);
                array_push($returnList, $menu);
            }
        }
        if (empty($returnList)) {
            $returnList = $menus;
        }
        return $returnList;
    }

    public function buildMenus($menus): array {
        $routers = [];
        foreach ($menus as $menu) {
            $router = new RouterDTO();
            $router->setHidden($menu->getIsVisible() == 0);
            $router->setName($this->getRouteName($menu));
            $router->setPath($this->getRouterPath($menu));
            $router->setComponent($this->getComponent($menu));
            $router->setMeta(new MetaDTO($menu->getMenuName(), $menu->getIcon(), true));
            $cMenus = $menu->getChildren();
            if (!empty($cMenus) && count($cMenus) > 0 && UserConstant::TYPE_DIR == $menu->getMenuType()) {
                $router->setAlwaysShow(true);
                $router->setRedirect("noRedirect");
                $router->setChildren($this->buildMenus($menu->getChildren()));
            } elseif ($this->isMenuCFrame($menu)) {
                $childrenList = [];
                $children = new RouterDTO();
                $children->setPath($menu->getPath());
                $children->setComponent($menu->getComponent());
                $children->setName(ucfirst($menu->getPath()));
                $children->setMeta(new MetaDTO($menu->getMenuName(), $menu->getIcon(), true));
                array_push($childrenList, $children);
                $router->setChildren($childrenList);
            }
            array_push($routers, $router->convertToArray());
        }
        return $routers;
    }

    public function selectMenuTreeByUserId(int $userId): array {
        if (SysUser::isSuperAdmin($userId)) {
            $menus = $this->sysMenuDao->selectMenuTreeAll();
        } else {
            $menus = $this->sysMenuDao->selectMenuTreeByUserId($userId);
        }
        return $this->getChildPerms($menus, 0);
    }

    public function selectMenuPermsByUserId(int $userId): array {
        $objs = $this->sysMenuDao->selectMenuPermsByUserId($userId);
        $permsArr = [];
        foreach ($objs as $obj) {
            if (!empty($obj->perms)) {
                array_push($permsArr, $obj->perms);
            }
        }
        return $permsArr;
    }


    /**
     * desc: 根据父节点的ID获取所有子节点
     * @param array|object $menus
     * @param int $parentId
     * created by: lhw at 2022/7/28 15:29
     * @return array
     */
    private function getChildPerms($menus, int $parentId): array {
        $returnList = [];
        foreach ($menus as $menu) {
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if ($menu->parent_id == $parentId)
            {
                $this->recursionFn($menus, $menu);
                array_push($returnList, $menu);
            }
        }
        return $returnList;
    }

    /**
     * desc:递归列表
     * * @param $list
     * @param SysMenu $t
     * created by: lhw at 2022/7/26 13:47
     */
    private function recursionFn($list, SysMenu $t) {
        // 得到子节点列表
        $childList = $this->getChildList($list, $t);
        $t->setChildren($childList);
        foreach ($childList as $obj) {
            if ($this->hasChild($list, $obj)) {
                $this->recursionFn($list, $obj);
            }
        }
    }

    /**
     * desc: 子节点列表
     * * @param $list
     * @param SysMenu $t
     * created by: lhw at 2022/7/26 13:41
     * @return array
     */
    private function getChildList($list, SysMenu $t): array {
        $childrenList = [];
        foreach ($list as $menu) {
            if ($menu->getParentId() == $t->getMenuId()) {
                array_push($childrenList, $menu);
            }
        }
        return $childrenList;
    }

    /**
     * 判断是否有子节点
     * @param $list
     * @param SysMenu $t
     * @return bool
     */
    private function hasChild($list, SysMenu $t): bool {
        return count($this->getChildList($list, $t)) > 0;
    }

    /**
     * desc:是否为菜单内部跳转
     * @param SysMenu $menu
     * created by: lhw at 2022/7/28 17:51
     * @return bool
     */
    public function isMenuCFrame(SysMenu $menu): bool {
        return  $menu->getParentId() == 0
                && $menu->getMenuType() == UserConstant::TYPE_MENU
                && $menu->getIsFrame() == UserConstant::NO_FRAME;
    }

    /**
     * desc: 是否为目录内部跳转
     * @param SysMenu $menu
     * created by: lhw at 28/7/2022 11:28 下午
     * @return bool
     */
    public function isMenuMFrame(SysMenu $menu): bool {
        return  $menu->getParentId() == 0
            && $menu->getMenuType() == UserConstant::TYPE_DIR
            && $menu->getIsFrame() == UserConstant::NO_FRAME;
    }

    /**
     * desc:获取路由名称
     * @param SysMenu $menu
     * created by: lhw at 2022/7/28 22:52
     * @return string
     */
    public function getRouteName(SysMenu $menu): string {
        $routerName = ucfirst($menu->getPath());
        // 非外链并且是一级目录（类型为菜单）
        if ($this->isMenuCFrame($menu)) {
            $routerName = "";
        }
        return $routerName;
    }

    /**
     * desc:获取路由地址
     * @param SysMenu $menu
     * created by: lhw at 28/7/2022 11:30 下午
     * @return string
     */
    public function getRouterPath(SysMenu $menu): string {
         $routerPath = $menu->getPath();
        if ($this->isMenuMFrame($menu)) {
            // 非外链并且是一级目录（类型为目录）
            $routerPath = "/" . $menu->getPath();
        } elseif ($this->isMenuCFrame($menu)) {
            // 非外链并且是一级目录（类型为菜单）
            $routerPath = "/";
        }
        return $routerPath;
    }

    /**
     * desc:获取组件信息
     * @param SysMenu $menu
     * created by: lhw at 28/7/2022 11:38 下午
     * @return string
     */
    public function getComponent(SysMenu $menu): string {
        $component = UserConstant::LAYOUT;
        if (!empty($menu->getComponent()) && !$this->isMenuCFrame($menu)) {
            $component = $menu->getComponent();
        }
        else if (empty($menu->getComponent()) && $this->isParentView($menu))
        {
            $component = UserConstant::PARENT_VIEW;
        }
        return $component;
    }

    /**
     * desc:是否为parent_view组件
     * @param SysMenu $menu
     * created by: lhw at 28/7/2022 11:37 下午
     * @return bool
     */
    public function isParentView(SysMenu $menu): bool {
        return $menu->getParentId() > 0 && (UserConstant::TYPE_DIR == $menu->getMenuType());
    }
}
