<?php
/**
 * Class SysUserServiceImpl
 * @package App\Services\Impl
 * Desc:
 * created by: lhw at 2022/8/2 15:00
 */

namespace App\Services\Impl;

use App\Common\Helper;
use App\Constants\ErrorCode;
use App\Domain\Dao\SysUserDao;
use App\Domain\Dao\SysUserRoleDao;
use App\Exception\BusinessException;
use App\Services\SysUserService;
use Hyperf\DbConnection\Annotation\Transactional;
use Hyperf\Di\Annotation\Inject;

class SysUserServiceImpl implements SysUserService
{

    /**
     * @Inject()
     * @var SysUserDao
     */
    private SysUserDao $sysUserDao;

    /**
     * @Inject()
     * @var SysUserRoleDao
     */
    private SysUserRoleDao $userRoleDao;


    public function getUserList(array $condition) {
        return $this->sysUserDao->getUserList($condition);
    }

    public function getUserById(int $userId) {
        $cols = $this->sysUserDao->getUserById($userId);
        if (empty($cols)) {
            return [];
        }
        $res = $cols->toArray();
        $roleIds = array_column($res, "roleId");
        $roleNames = array_column($res, "roleName");
        foreach ($res as &$col) {
            $col["sex"] = strval($col["sex"]);
            $col["status"] = strval($col["status"]);
            $col["roleIds"] = $roleIds;
            $col["roleNames"] = implode(",", $roleNames);
            unset($col);
        }
        return $res[0];
    }

    public function setIsDisable(int $userId, int $status): int {
        return $this->sysUserDao->isDisableUser($userId, $status);
    }

    /**
     * @Transactional()
     */
    public function add(array $condition): bool {
        if (!empty($condition["phone"])) {
            $exist = $this->sysUserDao->getUserInfoByPhone($condition["phone"]);
            if (!empty($exist)) {
                throw new BusinessException(ErrorCode::PARAMS_ERROR, "手机号已被绑定其他用户");
            }
        }
        if (!empty($condition["userName"])) {
            $exist = $this->sysUserDao->getUserInfoByName($condition["userName"]);
            if (!empty($exist)) {
                throw new BusinessException(ErrorCode::PARAMS_ERROR, "用户名称已存在");
            }
        }
        if (!empty($condition["email"])) {
            $exist = $this->sysUserDao->getUserInfoByEmail($condition["email"]);
            if (!empty($exist)) {
                throw new BusinessException(ErrorCode::PARAMS_ERROR, "邮箱已被绑定其他用户");
            }
        }
        $res = $this->sysUserDao->addUser($condition);
        $info = $this->sysUserDao->getUserInfoByName($condition["userName"]);
        $this->userRoleDao->batchAddUserRole(["userId"=>$info->id, "roleIds"=>$condition["roleIds"]]);
        return $res;
    }

    /**
     * @Transactional()
     */
    public function update(array $condition):bool {
        if (!empty($condition["phone"])) {
            $exist = $this->sysUserDao->getUserInfoByPhone($condition["phone"]);
            if (!empty($exist) && ($exist->id != $condition["userId"])) {
                throw new BusinessException(ErrorCode::PARAMS_ERROR, "手机号已被绑定其他用户");
            }
        }
        if (!empty($condition["email"])) {
            $exist = $this->sysUserDao->getUserInfoByEmail($condition["email"]);
            if (!empty($exist) && ($exist->id != $condition["userId"])) {
                throw new BusinessException(ErrorCode::PARAMS_ERROR, "邮箱已被绑定其他用户");
            }
        }
        $res = $this->sysUserDao->updateUser($condition);
        $this->userRoleDao->batchUpdateUserRole($condition);
        return $res;
    }

    /**
     * @Transactional()
     */
    public function delete(string $userIds): bool {
        $ids = explode(",", $userIds);
        $condition = [
          "batchDel" => true,
          "userIds" => $ids
        ];
        $res = $this->sysUserDao->batchDeleteUser($condition);
        $res1 = $this->userRoleDao->deleteByUserId($ids);
        return $res && $res1;
    }

    public function resetPassword(int $userId, string $password): bool {
        $model = $this->sysUserDao->getModel()->find($userId);
        $model->setPassword(Helper::instance()->md5Pwd($password, $model->getSalt()));
        return $model->save();
    }

    public function updateUserProfile(array $condition): bool {
        if (!empty($condition["phone"])) {
            $exist = $this->sysUserDao->getUserInfoByPhone($condition["phone"]);
            if (!empty($exist) && ($exist->id != $condition["userId"])) {
                throw new BusinessException(ErrorCode::PARAMS_ERROR, "手机号已被绑定其他用户");
            }
        }
        if (!empty($condition["email"])) {
            $exist = $this->sysUserDao->getUserInfoByEmail($condition["email"]);
            if (!empty($exist) && ($exist->id != $condition["userId"])) {
                throw new BusinessException(ErrorCode::PARAMS_ERROR, "邮箱已被绑定其他用户");
            }
        }
        $model = $this->sysUserDao->getModel()->find($condition["userId"]);
        $model->setPhone($condition["phone"] ?? "");
        $model->setEmail($condition["email"] ?? "");
        $model->setGender($condition["sex"] ?? 0);
        return $model->save();
    }

    public function setAvatar(int $userId, string $url): bool {
        $model = $this->sysUserDao->getModel()->find($userId);
        $model->setAvatar($url);
        return $model->save();
    }
}