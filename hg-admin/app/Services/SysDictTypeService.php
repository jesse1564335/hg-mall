<?php

namespace App\Services;

interface SysDictTypeService
{

    /**
     * desc: 分页
     * @param array $conditions
     * created by: lhw at 2022/8/6 16:36
     * @return mixed
     */
    public function getPageList(array $conditions);

    /**
     * desc: 根据主键ID查询字典类型详情
     * @param int $dictId
     * created by: lhw at 2022/8/6 17:06
     * @return mixed
     */
    public function selectDictTypeById(int $dictId);

    /**
     * desc:根据字典类型查询字典类型详情
     * @param string $dictType
     * created by: lhw at 2022/8/6 17:06
     * @return mixed
     */
    public function selectDictDataByType(string $dictType);

    /**
     * desc: 新增
     * @param array $params
     * @return bool
     */
    public function add(array $params): bool;

    /**
     * desc: 修改
     * @param array $params
     * created by: lhw at 2022/8/6 22:19
     * @return bool
     */
    public function update(array $params): bool;

    /**
     * desc: 删除
     * @param string $dictId
     * created by: lhw at 2022/8/7 13:26
     * @return bool
     */
    public function delete(string $dictId): bool;
}