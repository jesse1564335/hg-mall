<?php

namespace App\Services;

interface SysCrontabService
{

    /**
     * desc: 查找全部任务
     * @param array|null $params
     * created by: lhw at 2022/8/21 20:23
     * @return array
     */
    public function selectAllCrontab(array $params = null): array;

    /**
     * desc: 添加任务
     * @param array $params
     * created by: lhw at 2022/8/22 22:14
     * @return bool
     */
    public function addCrontab(array $params): bool;

    /**
     * desc: 更新任务
     * @param array $params
     * created by: lhw at 2022/9/3 10:33
     * @return bool
     */
    public function updateCrontab(array $params): bool;

    /**
     * desc:
     * @param $cronId
     * @param $enable
     * created by: lhw at 2022/8/25 19:38
     */
    public function setEnable($cronId, $enable): void;

    /**
     * desc: 分页查询
     * @param array $params
     * created by: lhw at 2022/8/25 20:34
     * @return mixed
     */
    public function selectList(array $params);

    /**
     * desc: 立即执行一次
     * @param int $cronId
     * created by: lhw at 2022/8/25 22:25
     */
    public function run(int $cronId): void;

    /**
     * desc: 详情
     * @param int $cronId
     * created by: lhw at 2022/8/26 19:49
     * @return mixed
     */
    public function detail(int $cronId);
}