<?php
/**
 * Class SysConfigService
 * @package App\Services
 * Desc:
 * created by: lhw at 2022/8/8 14:58
 */

namespace App\Services;


interface SysConfigService
{

    /**
     * desc: 分页查询列表
     * @param array $params
     * created by: lhw at 2022/8/8 15:49
     * @return mixed
     */
    public function list(array $params);

    /**
     * desc:新增配置
     * @param array $params
     * created by: lhw at 2022/8/8 17:59
     * @return bool
     */
    public function add(array $params): bool;

    /**
     * desc: 查询配置
     * @param int $configId
     * created by: lhw at 2022/8/9 10:23
     * @return mixed
     */
    public function getInfo(int $configId);

    /**
     * desc: 更新配置
     * @param array $params
     * created by: lhw at 2022/8/10 14:52
     * @return bool
     */
    public function update(array $params): bool;

    /**
     * desc: 删除配置
     * created by: lhw at 2022/8/10 15:12
     * @param string $configIds
     * @return bool
     */
    public function delete(string $configIds): bool;

    /**
     * desc:根据键名查询参数配置信息
     * @param string $configKey
     * created by: lhw at 2022/8/10 16:40
     * @return string
     */
    public function selectConfigByKey(string $configKey): string;

    /**
     * desc: 校验键名(config_key)是否唯一
     * @param string $configKey
     * created by: lhw at 2022/8/10 16:12
     * @return int
     */
    public function checkConfigKeyUnique(string $configKey): int;

    /**
     * desc: 清空缓存
     * created by: lhw at 2022/8/10 21:59
     */
    public function clearCache(): void;

}