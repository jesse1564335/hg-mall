<?php
/**
 * Class SysPermissionService
 * @package App\Services
 * Desc:
 * created by: lhw at 2022/8/2 10:25
 */

namespace App\Services;


use App\Model\SysUser;
use App\Services\Impl\SysMenuServiceImpl;
use App\Services\Impl\SysRoleServiceImpl;
use Hyperf\Di\Annotation\Inject;

class SysPermissionService
{
    /**
     * @Inject()
     * @var SysRoleServiceImpl
     */
    private SysRoleServiceImpl $sysRoleService;

    /**
     * @Inject()
     * @var SysMenuServiceImpl
     */
    private SysMenuServiceImpl $sysMenuService;

    /**
     * desc:根据用户查找角色
     * @param SysUser $sysUser
     * created by: lhw at 2022/8/1 23:08
     * @return array[]
     */
    public function getRolePermission(SysUser $sysUser): array {
        $rolesArr = [
            "roleId" => [],
            "roleName" => []
        ];
        if ($sysUser->isAdmin()) {
            array_push($rolesArr["roleId"], $sysUser->id);
            array_push($rolesArr["roleName"], "admin");
        } else {
            [$rolesId, $rolesName] = $this->sysRoleService->selectRolePermissionByUserId($sysUser->id);
            array_push($rolesArr["roleId"], $rolesId);
            array_push($rolesArr["roleName"], $rolesName);
        }
//        $rolesArr["roleName"] = [];
        return $rolesArr;
    }

    /**
     * desc: 获取菜单数据权限
     * @param SysUser $sysUser
     * created by: lhw at 2022/8/2 10:34
     * @return array
     */
    public function getMenuPermission(SysUser $sysUser): array {
        $perms = [];
        if ($sysUser->isAdmin()) {
            array_push($perms, "*:*:*");
        } else {
            $roles = $this->getRolePermission($sysUser);
            if (in_array("admin", $roles["roleName"])) {
                array_push($perms, "*:*:*");
            } else {
                $perms = $this->sysMenuService->selectMenuPermsByUserId($sysUser->getId());

            }
        }
        return $perms;
    }
}