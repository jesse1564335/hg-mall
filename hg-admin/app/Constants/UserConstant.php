<?php

namespace App\Constants;

class UserConstant
{
    /**
     * 平台内系统用户的唯一标志
     */
    public const SYS_USER = "SYS_USER";

    /** 正常状态 */
    public const NORMAL = "1";

    /** 异常状态 */
    public const EXCEPTION = "0";

    /** 用户封禁状态 */
    public const USER_DISABLE = "0";

    /** 角色封禁状态 */
    public const ROLE_DISABLE = "0";

    /** 部门正常状态 */
    public const DEPT_NORMAL = "1";

    /** 部门停用状态 */
    public const DEPT_DISABLE = "0";

    /** 字典正常状态 */
    public const DICT_NORMAL = "1";

    /** 是否为系统默认（是） */
    public const YES = "Y";

    /** 是否菜单外链（是） */
    public const YES_FRAME = "1";

    /** 是否菜单外链（否） */
    public const NO_FRAME = "0";

    /** 菜单类型（目录） */
    public const TYPE_DIR = "M";

    /** 菜单类型（菜单） */
    public const TYPE_MENU = "C";

    /** 菜单类型（按钮） */
    public const TYPE_BUTTON = "F";

    /** Layout组件标识 */
    public const LAYOUT = "Layout";

    /** ParentView组件标识 */
    public const PARENT_VIEW = "ParentView";

    /** 校验返回结果码 */
    public const UNIQUE = 1;
    public const NOT_UNIQUE = 0;
}