<?php

namespace App\Constants;

use Hyperf\Constants\AbstractConstants;

class CacheConstant extends AbstractConstants
{
    public const USER_AUTH_KEY = 'Auth:';
    public const USER_AUTH_TTL = 21800;
    public const CAPTCHA_KEY = "captcha_codes:";
    public const CAPTCHA_TTL = 120;

    public const OPENSSL_CBC_KEY = 'Hg*88g*sign';
    public const OPENSSL_CBC_IV_KEY = 'Hg*88g4Gnsi';

    /**
     * 字典管理 cache key
     */
    public const SYS_DICT_KEY = "sys_dict:";

    /**
     * 参数配置 cache key
     */
    public const SYS_CONFIG_KEY = "sys_config:";



}
