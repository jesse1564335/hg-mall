<?php


namespace App\Controller;

use App\Common\Annotation\PreAuthorization;
use Hyperf\HttpServer\Annotation\AutoController;
use Hyperf\HttpServer\Annotation\GetMapping;


/**
 * Class IndexController
 * @package App\Controller
 * @AutoController()
 */
class IndexController extends AbstractController
{
    /**
     * @GetMapping(path="test")
     */
    public function test() {
//        return $this->response->success(config('nacos_config'));
    }


    /**
     * @GetMapping("show")
     * @PreAuthorization(value="index:show")
     */
    public function show() {
        return "show";
    }


}