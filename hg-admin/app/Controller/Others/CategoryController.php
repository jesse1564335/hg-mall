<?php

namespace App\Controller\Others;

use App\Common\Annotation\PreAuthorization;
use App\Constants\ErrorCode;
use App\Controller\AbstractController;
use App\Domain\Request\QueryCategoryReq;
use App\Domain\Request\SaveCategoryReq;
use App\Domain\Utils\TreeSelect;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Psr\Http\Message\ResponseInterface;
use repository\rpc\service\ProductService;

/**
 * @Controller(prefix="/product/category")
 * Class CategoryController
 * @package App\Controller\Others
 * created by: lhw at 2022/9/13 16:12
 */
class CategoryController extends AbstractController
{
    /**
     * @Inject()
     * @var ProductService
     */
    private ProductService $productService;


    /**
     * @PostMapping(path="list")
     * desc: 分类列表
     * @param QueryCategoryReq $req
     * created by: lhw at 2022/9/13 16:33
     * @return ResponseInterface
     */
    public function list(QueryCategoryReq $req): ResponseInterface
    {
        $this->logger->info("get the category list req . %s", $req->all());
        $list = $this->productService->categoryList($req->all());
        return $this->response->success($list);
    }

    /**
     * @DeleteMapping(path="{categoryId}")
     * @PreAuthorization(value="product:category:del")
     * desc: 删除分类
     * @param int $categoryId
     * created by: lhw at 2022/9/13 22:36
     * @return ResponseInterface
     */
    public function delete(int $categoryId): ResponseInterface {
        $this->logger->info("delete category req . %s", ["categoryId"=>$categoryId]);
        $res = $this->productService->deleteCategory($categoryId);
        if ($res) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR);
    }

    /**
     * @PostMapping(path="add")
     * @PreAuthorization(value="product:category:add")
     * desc: 添加分类
     * @param SaveCategoryReq $req
     * created by: lhw at 2022/9/14 13:23
     * @return ResponseInterface
     */
    public function add(SaveCategoryReq $req): ResponseInterface {
        $this->logger->info("add category req . %s", $req->all());
        $req->scene("add")->validateResolved();
        $res = $this->productService->addCategory($req->all());
        if (isset($res) && $res["code"] == ErrorCode::SUCCESS) {
            return $this->response->success();
        }
        $message = $res["message"] ?? "失败";
        return $this->response->fail(ErrorCode::SERVER_ERROR, $message);
    }

    /**
     * @PostMapping(path="update")
     * @PreAuthorization(value="product:category:update")
     * desc: 更新分类
     * @param SaveCategoryReq $req
     * created by: lhw at 2022/9/14 13:57
     * @return ResponseInterface
     */
    public function update(SaveCategoryReq $req): ResponseInterface {
        $this->logger->info("update category req. %s", $req->all());
        $req->scene("update")->validateResolved();
       $res = $this->productService->updateCategory($req->all());
       if (isset($res) && $res["code"] == ErrorCode::SUCCESS) {
           return $this->response->success();
       }
       $message = $res["message"] ?? "失败";
       return $this->response->fail(ErrorCode::SERVER_ERROR, $message);
    }

    /**
     * @GetMapping(path="info/{categoryId}")
     * desc: 分类信息
     * @param int $categoryId
     * created by: lhw at 2022/9/14 14:11
     * @return ResponseInterface
     */
    public function info(int $categoryId): ResponseInterface {
        $this->logger->info("get category detail req . %s", ["categoryId"=>$categoryId]);
        return $this->response->success($this->productService->getCategoryDetail($categoryId));
    }

    /**
     * @GetMapping(path="categoryTree")
     * desc: 分类树结构
     * created by: lhw at 2022/9/16 14:51
     * @return ResponseInterface
     */
    public function getCategoryTree(): ResponseInterface {
        return $this->response->success(
            TreeSelect::categoryTree($this->productService->buildCategoryTree())
        );
    }
}