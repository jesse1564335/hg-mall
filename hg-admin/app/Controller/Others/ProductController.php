<?php


namespace App\Controller\Others;

use App\Common\Annotation\PreAuthorization;
use App\Constants\ErrorCode;
use App\Controller\AbstractController;
use App\Domain\Request\SaveProductReq;
use App\Domain\Request\GetProductListReq;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Psr\Http\Message\ResponseInterface;
use repository\rpc\service\ProductService;

/**
 * @Controller(prefix="/product")
 * Class ProductController
 * created by: lhw at 2022/9/4 18:23
 */
class ProductController extends AbstractController
{

    /**
     * @Inject()
     * @var ProductService
     */
    private ProductService $productService;


    /**
     * @GetMapping(path="page")
     * @PreAuthorization(value="product:page")
     * desc: 分页查询
     * @param GetProductListReq $req
     * created by: lhw at 2022/9/9 11:28
     * @return ResponseInterface
     */
    public function list(GetProductListReq $req): ResponseInterface {
        $this->logger->info("get product list req. %s", $req->all());
        $list = $this->productService->list($req->all());
        return $this->response->success($list);
    }

    /**
     * @PostMapping(path="add")
     * desc:
     * created by: lhw at 2022/9/4 18:24
     */
    public function add(SaveProductReq $req): ResponseInterface {
        $this->logger->info("add product req. %s", $req->all());
        $req->scene("add")->validateResolved();
        $res = $this->productService->addProduct($req->all());
        if ($res) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "添加失败");
    }

    /**
     * @PostMapping(path="update")
     * desc: 更新产品
     * @param SaveProductReq $req
     * created by: lhw at 2022/9/9 13:58
     * @return ResponseInterface
     */
    public function update(SaveProductReq $req): ResponseInterface {
        $this->logger->info("update product req. %s", $req->all());
        $req->scene("update")->validateResolved();
        $res = $this->productService->updateProduct($req->all());
        if ($res) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "更新失败");
    }

    /**
     * @DeleteMapping(path="{productId}")
     * desc: 删除
     * @param int $productId
     * created by: lhw at 2022/9/12 18:21
     * @return ResponseInterface
     */
    public function delete(int $productId): ResponseInterface {
        $this->logger->info("delete product req . %s", ["productId"=>$productId]);

        return $this->response->success();
    }


}