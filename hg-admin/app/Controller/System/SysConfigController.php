<?php
/**
 * Class SysConfigController
 * @package App\Controller
 * Desc:
 * created by: lhw at 2022/8/8 15:51
 */

namespace App\Controller\System;


use App\Common\Annotation\PreAuthorization;
use App\Constants\ErrorCode;
use App\Constants\UserConstant;
use App\Controller\AbstractController;
use App\Domain\Request\AddSysConfigReq;
use App\Domain\Request\ModifySysConfigReq;
use App\Domain\Request\SysConfigReq;
use App\Domain\Response\SysConfigRsp;
use App\Services\SysConfigService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Psr\Http\Message\ResponseInterface;

/**
 * @Controller(prefix="/system/config")
 * Class SysConfigController
 * @package App\Controller
 * created by: lhw at 2022/8/8 16:02
 */
class SysConfigController extends AbstractController
{

    /**
     * @Inject()
     * @var SysConfigRsp
     */
    private SysConfigRsp $configRsp;

    /**
     * @Inject()
     * @var SysConfigService
     */
    private SysConfigService $configService;


    /**
     * @PreAuthorization(value="sys:config:list")
     * @GetMapping(path="list")
     * desc: 分页列表
     * @param SysConfigReq $req
     * created by: lhw at 2022/8/8 16:01
     * @return ResponseInterface
     */
    public function list(SysConfigReq $req): ResponseInterface {
        $this->logger->info('sys config list req: %s', $req->all());
        $list = $this->configService->list($req->all());
        return  $this->response->success(
            $this->configRsp->buildPage($list)
        );
    }

    /**
     * @PreAuthorization(value="sys:config:add")
     * @PostMapping(path="add")
     * desc:添加
     * @param AddSysConfigReq $req
     * created by: lhw at 2022/8/8 17:09
     * @return ResponseInterface
     */
    public function add(AddSysConfigReq $req): ResponseInterface {
        $this->logger->info("add sys config req: %s", $req->all());
        $req->validateResolved();
        if (UserConstant::NOT_UNIQUE != $this->configService->checkConfigKeyUnique($req->all()['configKey'])) {
            return $this->response->fail(ErrorCode::SERVER_ERROR, "参数键名不唯一");
        }
        $res = $this->configService->add($req->all());
        if ($res) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "添加失败");
    }

    /**
     * @PreAuthorization(value="sys:config:query")
     * @GetMapping(path="{configId}")
     * desc: 查询配置信息
     * @param int $configId
     * created by: lhw at 2022/8/9 10:19
     * @return ResponseInterface
     */
    public function getInfo(int $configId): ResponseInterface {
        $this->logger->info("get sys config info req: %s", ["configId"=>$configId]);
        $info = $this->configService->getInfo($configId);
        return $this->response->success(
            $this->configRsp->stdoutFormatter($info)
        );
    }

    /**
     * @PreAuthorization(value="sys:config:update")
     * @PostMapping(path="update")
     * desc: 更新配置
     * @param ModifySysConfigReq $req
     * created by: lhw at 2022/8/10 14:50
     * @return ResponseInterface
     */
    public function update(ModifySysConfigReq $req): ResponseInterface {
        $this->logger->info("update sys config req: %s", $req->all());
        $req->validateResolved();
        $res = $this->configService->update($req->all());
        if ($res) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "更新失败");
    }

    /**
     * @PreAuthorization(value="sys:config:del")
     * @DeleteMapping(path="del/{configIds}")
     * desc: 删除配置
     * @param string $configIds
     * created by: lhw at 2022/8/10 15:19
     * @return ResponseInterface
     */
    public function delete(string $configIds): ResponseInterface {
        $this->logger->info("delete sys config by ids: %s", ["configIds"=>$configIds]);
        $res = $this->configService->delete($configIds);
         if ($res) {
             return $this->response->success();
         }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "删除失败");
    }

    /**
     * @PreAuthorization(value="sys:config:remove")
     * @DeleteMapping(path="clearCache")
     * desc: 清空缓存数据
     * created by: lhw at 2022/8/10 22:01
     * @return ResponseInterface
     */
    public function clearCache(): ResponseInterface {
        $this->configService->clearCache();
        return $this->response->success();
    }

    /**
     * @GetMapping(path="configKey/{configKey}")
     * desc:根据参数键名查询参数值
     * @param string $configKey
     * created by: lhw at 2022/8/10 22:34
     * @return ResponseInterface
     */
    public function getConfigValue(string $configKey): ResponseInterface {
        return $this->response->success([
            $this->configService->selectConfigByKey($configKey)
        ]);
    }
}