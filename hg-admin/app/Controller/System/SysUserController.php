<?php

namespace App\Controller\System;

use App\Common\Annotation\PreAuthorization;
use App\Constants\ErrorCode;
use App\Controller\AbstractController;
use App\Domain\Request\AddSysUserReq;
use App\Domain\Request\ModifySysUserReq;
use App\Domain\Request\SysUserReq;
use App\Domain\Response\SysUserRsp;
use App\Model\SysRole;
use App\Model\SysUser;
use App\Services\SysRoleService;
use App\Services\SysUserService;
use App\Services\UserAuth;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Psr\Http\Message\ResponseInterface;


/**
 * @Controller(prefix="/system/user")
 * Class SysUserController
 * @package App\Controller
 * created by: lhw at 2022/7/31 21:09
 */
class SysUserController extends AbstractController
{

    /**
     * @Inject()
     * @var SysUserRsp
     */
    private SysUserRsp $sysUserRsp;

    /**
     * @Inject()
     * @var SysUserService
     */
    private SysUserService $sysUserService;

    /**
     * @Inject()
     * @var SysRoleService
     */
    private SysRoleService $sysRoleService;


    /**
     * @PreAuthorization(value="sys:user:list")
     * @GetMapping(path="list")
     * desc: 用户列表
     * @param SysUserReq $req
     * created by: lhw at 2022/8/2 16:05
     * @return ResponseInterface
     */
    public function list(SysUserReq $req): ResponseInterface {
        $this->logger->info("get system user list req: %s", $req->all());
        $req->validateResolved();
        $list = $this->sysUserService->getUserList($req->all());
        return $this->response->success(
            $this->sysUserRsp->buildPage($list)
        );
    }

    /**
     * @PreAuthorization(value="sys:user:query")
     * @PostMapping(path="info")
     * desc: 根据用户ID查询详细信息
     * @return ResponseInterface
     */
    public function getInfo(): ResponseInterface {
        $allRoles = $this->sysRoleService->selectRoleAll()->toArray();
        $req = $this->request->all();
        $userId = $req["userId"];
        $result = [];
        if (SysUser::isSuperAdmin((int)$userId)) {
            $result["roles"] = $allRoles;
        } else {
            $roles = array_filter($allRoles, function ($item) {
                if (!SysRole::isSuperAdmin($item["roleId"])) {
                    return true;
                }
            });
            array_multisort($roles, SORT_ASC);
            $result["roles"] = $roles;
        }
        if (!empty($userId)) {
            $result["user"] = $this->sysUserService->getUserById($userId);
            $result["roleIds"] = $this->sysRoleService->selectRoleListByUserId($userId);
        }
        return $this->response->success($result);
    }

    /**
     * @GetMapping(path="isDisable/{userId}/{status}")
     * @PreAuthorization(value="sys:user:disable")
     * desc: 是否禁用用户
     * @param int $userId
     * @param int $status
     * created by: lhw at 2022/8/4 15:14
     * @return ResponseInterface
     */
    public function disableUser(int $userId, int $status): ResponseInterface {
        $this->logger->info("set system user status: %s", ["userId"=>$userId, "status"=>$status]);
        $this->sysUserService->setIsDisable($userId, $status);
        return $this->response->success();
    }

    /**
     * @PostMapping(path="add")
     * @PreAuthorization(value="sys:user:add")
     * desc: 新增用户
     * @param AddSysUserReq $req
     * created by: lhw at 2022/8/14 0:08
     * @return ResponseInterface
     */
    public function add(AddSysUserReq $req): ResponseInterface {
        $this->logger->info("add system user req: %s", $req->all());
        $req->validateResolved();
        $this->sysUserService->add($req->all());
        return $this->response->success();
    }

    /**
     * @PostMapping(path="update")
     * @PreAuthorization(value="sys:user:update")
     * desc: 更新用户
     * @param ModifySysUserReq $req
     * created by: lhw at 2022/8/14 10:38
     * @return ResponseInterface
     */
    public function update(ModifySysUserReq $req): ResponseInterface {
        $this->logger->info("update system user req: %s", $req->all());
        $req->validateResolved();
        $this->sysUserService->update($req->all());
        return $this->response->success();
    }

    /**
     * @DeleteMapping(path="{userId}")
     * @PreAuthorization(value="sys:user:del")
     * desc: 删除用户
     * @param string $userId
     * created by: lhw at 2022/8/14 11:19
     * @return ResponseInterface
     */
    public function delete(string $userId): ResponseInterface {
        $this->logger->info("delete system user req: %s", ["ids"=>$userId]);
        $res = $this->sysUserService->delete($userId);
        if ($res) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "删除失败");
    }

    /**
     * @GetMapping(path="resetPwd/{userId}/{password}")
     * @PreAuthorization(value="sys:user:resetPwd")
     * desc: 重置密码
     * @param int $userId
     * @param string $password
     * created by: lhw at 2022/8/14 11:55
     * @return ResponseInterface
     */
    public function resetPassword(int $userId, string $password): ResponseInterface {
        $res = $this->sysUserService->resetPassword($userId, $password);
        if ($res) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "重置密码失败");
    }

    /**
     * @GetMapping(path="profile")
     * desc: 个人信息
     * created by: lhw at 2022/8/14 13:42
     * @return ResponseInterface
     */
    public function profile(): ResponseInterface {
        $userId =  UserAuth::instance()->getUserId();
        $info = $this->sysUserService->getUserById($userId);
        return $this->response->success($info);
    }

    /**
     * @PostMapping(path="updateProfile")
     * desc: 更新个人信息
     * created by: lhw at 2022/8/14 14:07
     * @return ResponseInterface
     */
    public function updateProfile(): ResponseInterface {
        $res = $this->sysUserService->updateUserProfile($this->request->all());
        if ($res) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "保存失败");
    }

    /**
     * @PostMapping(path="avatar")
     * desc:设置头像
     * created by: lhw at 2022/8/17 16:23
     * @return ResponseInterface
     */
    public function setAvatar(): ResponseInterface {
        $userId = UserAuth::instance()->getUserId();
        $avatarUrl = $this->request->post("url", "");
        if ($avatarUrl == "") {
            return $this->response->fail(ErrorCode::SERVER_ERROR, "缺少图片地址");
        }
        return $this->response->success(
            $this->sysUserService->setAvatar($userId, $avatarUrl[0])
        );
    }
}