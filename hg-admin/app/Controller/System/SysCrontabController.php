<?php

namespace App\Controller\System;

use App\Common\Annotation\PreAuthorization;
use App\Constants\ErrorCode;
use App\Controller\AbstractController;
use App\Domain\Request\SaveCrontabReq;
use App\Domain\Request\SysCrontabReq;
use App\Domain\Response\SysCrontabRsp;
use App\Services\SysCrontabService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Psr\Http\Message\ResponseInterface;

/**
 * @Controller(prefix="/monitor/crontab")
 * Class SysCrontabController
 * created by: lhw at 2022/8/24 21:52
 */
class SysCrontabController extends AbstractController
{

    /**
     * @Inject()
     * @var SysCrontabService
     */
    private SysCrontabService $crontabService;

    /**
     * @Inject()
     * @var SysCrontabRsp
     */
    private SysCrontabRsp $crontabRsp;



    /**
     * @GetMapping(path="list")
     * @PreAuthorization(value="monitor:crontab:list")
     * desc: 分页查询
     * @param SysCrontabReq $req
     * created by: lhw at 2022/8/25 20:58
     * @return ResponseInterface
     */
    public function page(SysCrontabReq $req): ResponseInterface {
        $this->logger->info("select crontab list by req: %s", $req->all());
        $req->validateResolved();
        $list = $this->crontabService->selectList($req->all());
        return $this->response->success(
           $this->crontabRsp->buildPage($list)
        );
    }

    /**
     * @GetMapping(path="enable/{cronId}/{enable}")
     * @PreAuthorization(value="monitor:crontab:enable")
     * desc:设置任务状态
     * created by: lhw at 2022/8/24 21:53
     */
    public function setEnable(int $cronId, int $enable): ResponseInterface {
        $this->logger->info("set crontab enable req: %s", ["cronId"=>$cronId, "enable"=>$enable]);
        $this->crontabService->setEnable($cronId, $enable);
        return $this->response->success();
    }

    /**
     * @GetMapping(path="run/{cronId}")
     * @PreAuthorization(value="monitor:crontab:run")
     * desc: 立即执行一次
     * @param int $cronId
     * created by: lhw at 2022/8/25 22:23
     * @return ResponseInterface
     */
    public function run(int $cronId): ResponseInterface {
        $this->crontabService->run($cronId);
        return $this->response->success();
    }

    /**
     * @GetMapping(path="info/{cronId}")
     * desc: 详情
     * @param int $cronId
     * created by: lhw at 2022/8/26 19:47
     * @return ResponseInterface
     */
    public function info(int $cronId): ResponseInterface {
        $this->logger->info("get the crontab job detail by req:%s", [$cronId]);
        $detail = $this->crontabService->detail($cronId);
        return $this->response->success(
            $this->crontabRsp->stdoutFormatter($detail)
        );
    }

    /**
     * @PostMapping(path="add")
     * @PreAuthorization(value="monitor:crontab:add")
     * desc: 添加任务
     * @param SaveCrontabReq $req
     * created by: lhw at 2022/9/3 10:28
     * @return ResponseInterface
     */
    public function add(SaveCrontabReq $req): ResponseInterface {
        $this->logger->info("add crontab job req: %s", $req->all());
        $req->scene("insert")->validateResolved();
        $result = $this->crontabService->addCrontab($req->all());
        if ($result) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "添加任务失败");
    }

    /**
     * @PostMapping(path="update")
     * @PreAuthorization(value="monitor:crontab:update")
     * desc: 更新任务
     * @param SaveCrontabReq $req
     * created by: lhw at 2022/9/3 10:38
     * @return ResponseInterface
     */
    public function update(SaveCrontabReq $req): ResponseInterface {
        $this->logger->info("update crontab job req: %s", $req->all());
        $req->scene("update")->validateResolved();
        $result = $this->crontabService->updateCrontab($req->all());
        if ($result) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "任务更新失败");
    }
}