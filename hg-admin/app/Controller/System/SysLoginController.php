<?php

namespace App\Controller\System;

use App\Controller\AbstractController;
use App\Domain\Request\LoginReq;
use App\Domain\Response\SysUserRsp;
use App\Services\LoginService;
use App\Services\SysMenuService;
use App\Services\SysPermissionService;
use App\Services\UserAuth;
use Hyperf\Config\Annotation\Value;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use \Psr\Http\Message\ResponseInterface;

/**
 * Class SysLoginController
 * @package App\Controller
 * created by: lhw at 2022/7/28 14:07
 * @Controller(prefix="/sys")
 */
class SysLoginController extends AbstractController {

    /**
     * @Inject()
     * @var LoginService
     */
    private LoginService $loginService;

    /**
     * @Inject()
     * @var SysPermissionService
     */
    private SysPermissionService $sysPermissionService;

    /**
     * @Inject()
     * @var SysMenuService
     */
    private SysMenuService $menuService;


    /**
     * @Inject()
     * @var SysUserRsp
     */
    private SysUserRsp $sysUserRsp;

    /**
     * @Value("nacos_config.captchaEnabled")
     * @var bool
     */
    private bool $captchaEnabled;


    /**
     * @GetMapping(path="captchaImg")
     * desc: 图形验证码信息
     * created by: lhw at 2022/7/29 15:13
     * @return ResponseInterface
     */
    public function getCaptchaCode(): ResponseInterface {
        return $this->response->success($this->loginService->makeCaptchaImg());
    }


    /**
     * @PostMapping(path="login")
     * desc: 登录
     * @param LoginReq $req
     * created by: lhw at 2022/7/29 11:18
     * @return ResponseInterface
     */
    public function login(LoginReq $req): ResponseInterface {
        $this->logger->info("user login req: {%s}", $req->all());
        $token = $this->loginService->login($req, $this->captchaEnabled);
        return $this->response->success(["token" => $token]);
    }

    /**
     * @GetMapping(path="userInfo")
     * desc:获取用户信息
     * created by: lhw at 2022/7/31 21:12
     *
     *
     * @return ResponseInterface
     */
    public function getUserInfo(): ResponseInterface {
        $user = UserAuth::instance()->getSysUser();
        //角色
        $roles = $this->sysPermissionService->getRolePermission($user);
        //权限
        $permissions = $this->sysPermissionService->getMenuPermission($user);
        return $this->response->success([
            "user" => $this->sysUserRsp->stdoutFormatter($user),
            "roles" => $roles,
            "permissions" => $permissions
        ]);
    }

    /**
     * @GetMapping(path="getRouters")
     * desc: 获取路由信息
     * created by: lhw at 2022/7/28 14:20
     * @return ResponseInterface
     */
    public function getRouters(): ResponseInterface {
        $menus = $this->menuService->selectMenuTreeByUserId(get_user_id());
        return $this->response->success($this->menuService->buildMenus($menus));
    }

    /**
     * @GetMapping(path="logout")
     * desc: 退出登录
     * created by: lhw at 2022/7/31 21:53
     * @return ResponseInterface
     */
    public function logout(): ResponseInterface {
        UserAuth::instance()->clearCache($this->request->getHeaderLine(UserAuth::AUTH_TOKEN));
        return $this->response->success();
    }
}
