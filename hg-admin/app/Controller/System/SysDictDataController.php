<?php

namespace App\Controller\System;

use App\Common\Annotation\PreAuthorization;
use App\Constants\ErrorCode;
use App\Controller\AbstractController;
use App\Domain\Request\AddDictDataReq;
use App\Domain\Request\ModifyDictDataReq;
use App\Domain\Request\SysDictDataReq;
use App\Domain\Response\SysDictDataRsp;
use App\Services\SysDictDataService;
use App\Services\SysDictTypeService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Psr\Http\Message\ResponseInterface;

/**
 * @Controller(prefix="/system/dict/data")
 * Class SysDictDataController
 * @package App\Controller
 * created by: lhq at 2022/8/6 16:05
 */
class SysDictDataController extends AbstractController
{

    /**
     * @Inject()
     * @var SysDictDataService
     */
    private SysDictDataService $dictDataService;

    /**
     * @Inject()
     * @var SysDictTypeService
     */
    private SysDictTypeService $dictTypeService;

    /**
     * @Inject()
     * @var SysDictDataRsp
     */
    private SysDictDataRsp $sysDictDataRsp;



    /**
     * @PreAuthorization(value="sys:dict:data:list")
     * @GetMapping(path="list")
     * desc: 分页
     * @param SysDictDataReq $req
     * created by: lhw at 2022/8/6 16:04
     * @return ResponseInterface
     */
    public function page(SysDictDataReq $req): ResponseInterface {
        $this->logger->info("sys dict data page list: %s", $req->all());
        $req->validateResolved();
        $list = $this->dictDataService->getPageList($req->all());
        return $this->response->success(
            $this->sysDictDataRsp->buildPage($list)
        );
    }

    /**
     * @GetMapping(path="type/{dictType}")
     * desc:根据字典类型查询字典数据信息
     * @param string $dictType
     * created by: lhw at 2022/8/6 17:02
     * @return ResponseInterface
     */
    public function dictType(string $dictType): ResponseInterface {
        $this->logger->info("sys dict data query by dictType: %s", ["dictType"=>$dictType]);
        $info = $this->dictTypeService->selectDictDataByType($dictType);
        return $this->response->success(
            $this->sysDictDataRsp->formatList($info, false)
        );
    }

    /**
     * @PreAuthorization(value="sys:dict:data:query")
     * @GetMapping(path="{dictCode}")
     * desc: 字典数据详情
     * @param int $dictCode
     * created by: lhw at 2022/8/7 15:11
     * @return ResponseInterface
     */
    public function getInfo(int $dictCode): ResponseInterface {
        $this->logger->info("get sys dict data info by dicCode: %s", ["dictCode"=>$dictCode]);
        $info = $this->dictDataService->getInfo($dictCode);
        return $this->response->success(
            $this->sysDictDataRsp->stdoutFormatter($info)
        );
    }

    /**
     * @PreAuthorization(value="sys:dict:data:update")
     * @PostMapping(path="update")
     * desc:更新字典数据
     * @param ModifyDictDataReq $req
     * created by: lhw at 2022/8/7 15:32
     * @return ResponseInterface
     */
    public function update(ModifyDictDataReq $req): ResponseInterface {
        $this->logger->info("update dict data req: %s", $req->all());
        $req->validateResolved();
        $res = $this->dictDataService->updateInfo($req->all());
        if ($res) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "更新失败");
    }

    /**
     * @PreAuthorization(value="sys:dict:data:del")
     * @DeleteMapping(path="del/{dictCode}")
     * desc: 删除
     * @param string $dictCode
     * created by: lhw at 2022/8/7 16:03
     * @return ResponseInterface
     */
    public function delete(string $dictCode): ResponseInterface {
        $this->logger->info("delete dict data by dict code : %s", ["dictCode"=>$dictCode]);
        $res = $this->dictDataService->delete($dictCode);
        if ($res) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "删除失败");
    }

    /**
     * @PreAuthorization(value="sys:dict:data:add")
     * @PostMapping(path="add")
     * desc: 新增
     * @param AddDictDataReq $req
     * created by: lhw at 2022/8/7 16:39
     * @return ResponseInterface
     */
    public function add(AddDictDataReq $req): ResponseInterface {
        $this->logger->info("add dict data req: %s", $req->all());
        $req->validateResolved();
        $res = $this->dictDataService->add($req->all());
        if ($res) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "新增失败");
    }
}