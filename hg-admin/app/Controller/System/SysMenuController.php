<?php


namespace App\Controller\System;

use App\Common\Annotation\PreAuthorization;
use App\Constants\ErrorCode;
use App\Controller\AbstractController;
use App\Domain\Request\AddMenuRequest;
use App\Domain\Request\QueryMenuReq;
use App\Services\SysMenuService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\Di\Annotation\Inject;
use \Psr\Http\Message\ResponseInterface;

/**
 * Class SysMenuController
 * @package App\Controller
 * created by: lhw at 21/7/2022 9:34 下午
 * @Controller(prefix="/system/menu")
 */
class SysMenuController extends AbstractController
{
    /**
     * @Inject
     * @var SysMenuService
     */
    private SysMenuService $sysMenuService;



    /**
     * @PreAuthorization(value="sys:menu:add")
     * @PostMapping(path="add")
     * desc: 添加菜单
     * @param AddMenuRequest $addMenuRequest
     * created by: lhw at 21/7/2022 10:24 下午
     * @return ResponseInterface
     */
    public function addMenu(AddMenuRequest $addMenuRequest): ResponseInterface {
        $this->logger->info("add menu req: %s", $addMenuRequest->all());
        $addMenuRequest->scene($addMenuRequest->all()["menuType"])->validateResolved();
        $res = $this->sysMenuService->insertMenu($addMenuRequest->all());
        return $res > 0
            ? $this->response->success()
            : $this->response->fail(ErrorCode::SERVER_ERROR, "添加失败");
    }

    /**
     * @PreAuthorization(value="sys:menu:update")
     * @PostMapping(path="update")
     * desc: 更新菜单
     * @param AddMenuRequest $addMenuRequest
     * created by: lhw at 21/7/2022 10:26 下午
     * @return ResponseInterface
     */
    public function updateMenu(AddMenuRequest $addMenuRequest): ResponseInterface {
        $this->logger->info("update menu req: %s", $addMenuRequest->all());
        $addMenuRequest->scene($addMenuRequest->all()["menuType"])->validateResolved();
        $res = $this->sysMenuService->updateMenu($addMenuRequest->all());
        return $res > 0
            ? $this->response->success()
            : $this->response->fail(ErrorCode::SERVER_ERROR, "更新失败");
    }

    /**
     * @PreAuthorization(value="sys:menu:del")
     * @DeleteMapping(path="del/{id}")
     * desc:删除菜单
     * @param int $id
     * created by: lhw at 21/7/2022 10:32 下午
     * @return ResponseInterface
     */
    public function deleteMenu(int $id): ResponseInterface {
        $this->logger->info("delete menu by id req:%s", ["id"=>$id]);
        if ($this->sysMenuService->hasChildByMenuId($id)) {
            return $this->response->fail(ErrorCode::SERVER_ERROR, "存在子菜单,不允许删除");
        }
        if ($this->sysMenuService->checkMenuExistRole($id)) {
            return $this->response->fail(ErrorCode::SERVER_ERROR, "菜单已分配,不允许删除");
        }
        $res = $this->sysMenuService->deleteMenuById($id);
        return $res > 0
            ? $this->response->success()
            : $this->response->fail(ErrorCode::SERVER_ERROR, "删除失败");
    }

    /**
     * @PreAuthorization(value="sys:menu:list")
     * @GetMapping(path="list")
     * desc: 菜单列表
     * created by: lhw at 2022/7/22 14:41
     * @param QueryMenuReq $req
     * @return ResponseInterface
     */
    public function getMenuList(QueryMenuReq $req): ResponseInterface {
        $this->logger->info("get menu list req:%s", $req->all());
        $userId = get_user_id();
        return $this->response->success($this->sysMenuService->selectMenuList($userId, $req->all()));
    }

    /**
     * @PreAuthorization(value="sys:menu:query")
     * @GetMapping(path="menuInfo/{id}")
     * desc: 获取菜单详情
     * @param int $id
     * created by: lhw at 2022/7/24 20:46
     * @return ResponseInterface
     */
    public function getMenuInfo(int $id): ResponseInterface {
        $this->logger->info("get menu info req:%s", ["id"=>$id]);
        return $this->response->success(
            $this->sysMenuService->selectMenuById($id)
        );
    }

    /**
     * @GetMapping(path="roleMenuTree/{roleId}")
     * desc:对应角色菜单列表树
     * @param int $roleId
     * created by: lhw at 2022/7/25 17:24
     * @return ResponseInterface
     */
    public function getMenuListByRoleId(int $roleId): ResponseInterface {
        $this->logger->info("get menu list by role id req:%s", ["roleId"=>$roleId]);
        $userId = get_user_id();
        $menus = $this->sysMenuService->selectMenuList($userId, null, false);
        $checkedKey = $this->sysMenuService->selectMenuListByRoleId($roleId);
        return $this->response->success(
            [
                "menus" => $this->sysMenuService->buildMenuTreeSelect($menus),
                "checkedKeys" => $checkedKey
            ]
        );
    }

    /**
     * @GetMapping(path="treeSelect")
     * desc: 菜单下拉树列表
     * created by: lhw at 2022/7/26 13:53
     * @return ResponseInterface
     */
    public function menuTreeList(): ResponseInterface {
        $this->logger->info("get menu tree list by role id req:%s", []);
        $userId = get_user_id();
        $menus = $this->sysMenuService->selectMenuList($userId, null, false);
        return $this->response->success(
            $this->sysMenuService->buildMenuTreeSelect($menus)
        );
    }

}
