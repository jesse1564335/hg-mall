<?php

namespace App\Controller\System;

use App\Common\Annotation\PreAuthorization;
use App\Constants\ErrorCode;
use App\Controller\AbstractController;
use App\Domain\Request\AddDictTypeReq;
use App\Domain\Request\ModifyDictTypeReq;
use App\Domain\Request\SysDictTypeReq;
use App\Domain\Response\SysDictTypeRsp;
use App\Services\SysDictTypeService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Psr\Http\Message\ResponseInterface;

/**
 * @Controller(prefix="/system/dict/type")
 * Class SysDictTypeController
 * @package App\Controller
 * created by: lhq at 2022/8/6 16:39
 */
class SysDictTypeController extends AbstractController
{

    /**
     * @Inject()
     * @var SysDictTypeService
     */
    private SysDictTypeService $dictTypeService;

    /**
     * @Inject()
     * @var SysDictTypeRsp
     */
    private SysDictTypeRsp $sysDictTypeRsp;


    /**
     * @PreAuthorization(value="sys:dict:type:list")
     * @GetMapping(path="list")
     * desc: 分页
     * @param SysDictTypeReq $req
     * created by: lhw at 2022/8/6 16:39
     * @return ResponseInterface
     */
    public function page(SysDictTypeReq $req): ResponseInterface {
        $this->logger->info("sys dict type page list: %s", $req->all());
        $req->validateResolved();
        $list = $this->dictTypeService->getPageList($req->all());
        return $this->response->success(
            $this->sysDictTypeRsp->buildPage($list)
        );
    }

    /**
     * @GetMapping(path="{dictId}")
     * desc: 查询字典类型详细
     * @param int $dictId
     * created by: lhw at 2022/8/6 16:50
     * @return ResponseInterface
     */
    public function getInfoById(int $dictId): ResponseInterface {
        $this->logger->info("sys dict type query by id: %s", ["dictId"=>$dictId]);
        $info = $this->dictTypeService->selectDictTypeById($dictId);
        return $this->response->success(
           $this->sysDictTypeRsp->stdoutFormatter($info)
        );
    }

    /**
     * @PreAuthorization(value="sys:dict:type:add")
     * @PostMapping(path="add")
     * desc: 新增字典
     * @param AddDictTypeReq $req
     * created by: lhw at 2022/8/6 21:55
     * @return ResponseInterface
     */
    public function add(AddDictTypeReq $req): ResponseInterface {
        $this->logger->info("add sys dict type: %s", $req->all());
        $req->validateResolved();
        $bool = $this->dictTypeService->add($req->all());
        if ($bool) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "新增失败");
    }

    /**
     * @PreAuthorization(value="sys:dict:type:update")
     * @PostMapping(path="update")
     * desc: 修改
     * @param ModifyDictTypeReq $req
     * created by: lhw at 2022/8/6 22:27
     * @return ResponseInterface
     */
    public function update(ModifyDictTypeReq $req): ResponseInterface {
        $this->logger->info("modify sys dict type: %s", $req->all());
        $req->validateResolved();
        $bool = $this->dictTypeService->update($req->all());
        if ($bool) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "新增失败");
    }

    /**
     * @PreAuthorization(value="sys:dict:type:del")
     * @DeleteMapping(path="del/{dictId}")
     * desc: 删除
     * @param string $dictId
     * created by: lhw at 2022/8/7 13:29
     * @return ResponseInterface
     */
    public function del(string $dictId): ResponseInterface {
        $this->logger->info("delete sys dict type: %s", ['id'=>$dictId]);
        $bool = $this->dictTypeService->delete($dictId);
        if ($bool) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "删除失败");
    }

 }