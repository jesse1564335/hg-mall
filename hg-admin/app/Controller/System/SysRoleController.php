<?php

namespace App\Controller\System;

use App\Common\Annotation\PreAuthorization;
use App\Constants\ErrorCode;
use App\Controller\AbstractController;
use App\Domain\Request\ModifyRoleReq;
use App\Domain\Request\QueryRoleReq;
use App\Domain\Response\SysRoleRsp;
use App\Services\SysRoleService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Psr\Http\Message\ResponseInterface;

/**
 * @Controller(prefix="/system/role")
 * Class SysRoleController
 * @package App\Controller
 * created by: lhq at 2022/8/17 21:55
 */
class SysRoleController extends AbstractController
{

    /**
     * @Inject()
     * @var SysRoleRsp
     */
    private SysRoleRsp $sysRoleRsp;

    /**
     * @Inject()
     * @var SysRoleService
     */
    private SysRoleService $roleService;


    /**
     * @GetMapping(path="list")
     * @PreAuthorization(value="sys:role:list")
     * desc: 分页列表
     * @param QueryRoleReq $req
     * created by: lhw at 2022/8/17 22:26
     * @return ResponseInterface
     */
    public function list(QueryRoleReq $req): ResponseInterface {
        $this->logger->info("query role list req: %s", $req->all());
        $req->validateResolved();
        $list = $this->roleService->selectRoleList($req->all());
        return $this->response->success(
            $this->sysRoleRsp->buildPage($list)
        );
    }

    /**
     * @PreAuthorization(value="sys:role:query")
     * @GetMapping(path="{roleId}")
     * desc:根据角色ID获取信息
     * @param int $roleId
     * created by: lhw at 2022/8/18 10:21
     * @return ResponseInterface
     */
    public function getInfo(int $roleId): ResponseInterface {
        $this->logger->info("query role info by id: %s", ["id"=>$roleId]);
        $info = $this->roleService->getRoleInfo($roleId);
        return $this->response->success(
            $this->sysRoleRsp->stdoutFormatter($info)
        );
    }

    /**
     * @DeleteMapping(path="{roleIds}")
     * @PreAuthorization(value="sys:role:remove")
     * desc: 删除
     * @param string $roleIds
     * created by: lhw at 2022/8/19 15:12
     * @return ResponseInterface
     */
    public function delete(string $roleIds): ResponseInterface {
        $this->logger->info("delete role by ids: %s", ["ids"=>$roleIds]);
        $res = $this->roleService->deleteRole($roleIds);
        if ($res) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "删除失败");
    }

    /**
     * @PutMapping(path="changeStatus")
     * @PreAuthorization(value="sys:role:update")
     * desc: 修改状态
     * @param ModifyRoleReq $req
     * created by: lhw at 2022/8/19 16:49
     * @return ResponseInterface
     */
    public function changeStatus(ModifyRoleReq $req): ResponseInterface {
        $this->logger->info("change role status by: %s", $req->all());
        $req->scene("changeStatus")->validateResolved();
        $res = $this->roleService->updateRoleStatus($req->all());
        if ($res) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "修改失败");
    }

    /**
     * @PutMapping(path="")
     * @PreAuthorization(value="sys:role:update")
     * desc: 编辑更新角色信息
     * @param ModifyRoleReq $req
     * created by: lhw at 2022/8/20 14:21
     * @return ResponseInterface
     */
    public function update(ModifyRoleReq $req): ResponseInterface {
        $this->logger->info("update role info . req: %s", $req->all());
        $req->scene("update")->validateResolved();
        $res = $this->roleService->updateRole($req->all());
        if ($res) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "更新失败");
    }

    /**
     * @PostMapping(path="")
     * @PreAuthorization(value="sys:role:add")
     * desc: 新增角色
     * @param ModifyRoleReq $req
     * created by: lhw at 2022/8/21 11:41
     * @return ResponseInterface
     */
    public function add(ModifyRoleReq $req): ResponseInterface {
        $this->logger->info("add role req: %s", $req->all());
        $req->scene("add")->validateResolved();
        $res = $this->roleService->addRole($req->all());
        if ($res) {
            return $this->response->success();
        }
        return $this->response->fail(ErrorCode::SERVER_ERROR, "新增失败");
    }
}