<?php

declare(strict_types=1);

namespace App\Job;

use App\Domain\Request\LoginReq;
use App\Services\LogService;
use Hyperf\AsyncQueue\Job;

class RecordLogJob extends Job
{

    /**
     * 任务执行失败后的重试次数，即最大执行次数为 $maxAttempts+1 次
     * @var int
     */
    protected $maxAttempts = 3;

    /**
     * 信息
     * @var array
     */
    public array $params;

    /**
     * @var int|null
     */
    public ?int $status;

    /**
     * @var string|null
     */
    public ?string $msg;

    public function __construct(?array $params, ?int $status, ?string $msg)
    {
        $this->params = $params;
        $this->status = $status;
        $this->msg = $msg;
    }

    public function handle()
    {
        LogService::recordLoginInfo($this->params, $this->status, $this->msg);
    }
}
