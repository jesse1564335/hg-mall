<?php


namespace App\Common\Annotation;


use Hyperf\Di\Annotation\AbstractAnnotation;
use Hyperf\Di\Annotation\AnnotationCollector;

/**
 * Class PreAuthorization
 * @package app\Common\Annotation
 * @Annotation
 * @Target({"METHOD"})
 */
class PreAuthorization extends AbstractAnnotation
{
    /**
     * 权限标识
     * @var string
     */
    public string $value;


    public function __construct($value="")
    {
        parent::__construct($value);
        $this->bindMainProperty("value", $value);
    }
}