<?php

declare(strict_types=1);

namespace App\Common\Aspect;

use App\Common\Annotation\PreAuthorization;
use App\Common\Log\Logger;
use App\Constants\CacheConstant;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Model\SysUser;
use App\Services\UserAuth;
use Hyperf\Di\Annotation\AnnotationCollector;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Exception\AnnotationException;
use Hyperf\Di\Exception\Exception;
use Hyperf\Redis\Redis;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\HttpServer\Contract\RequestInterface;
use Psr\Container\NotFoundExceptionInterface;

/**
 * @Aspect
 */
#[Aspect]
class PreAuthorizationAspect extends AbstractAspect
{
    protected ContainerInterface $container;

    protected RequestInterface $request;

    protected Logger $logger;

    public $annotations = [
      PreAuthorization::class
    ];

    public function __construct(ContainerInterface $container, RequestInterface $request, Logger $logger)
    {
        $this->container = $container;
        $this->request = $request;
        $this->logger = $logger;
    }

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        try {
            $authorization = $this->getAuthorizationAnnotation($proceedingJoinPoint->className, $proceedingJoinPoint->methodName);
            if (!$this->checkPermission($authorization->value)) {
                throw new BusinessException(ErrorCode::AUTH_INVALID, ErrorCode::getMessage(ErrorCode::AUTH_INVALID));
            }
        } catch (AnnotationException | NotFoundExceptionInterface | ContainerExceptionInterface $e) {
            $this->logger->info("PreAuthorizationAspect 执行过程异常：%s", [
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
        }
        try {
            return $proceedingJoinPoint->process();
        } catch (Exception $e) {
            $this->logger->info("PreAuthorizationAspect 执行过程异常：%s", [
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
        }
    }

    /**
     * desc: 获取注解类
     * @param string $className
     * @param string $method
     * created by: lhw at 2022/8/18 17:40
     * @return PreAuthorization
     * @throws AnnotationException
     */
    protected function getAuthorizationAnnotation(string $className, string $method): PreAuthorization {
        $annotation = AnnotationCollector::getClassMethodAnnotation($className, $method)[PreAuthorization::class] ?? null;
        if (!$annotation instanceof PreAuthorization) {
            throw new AnnotationException("Annotation PreAuthorization couldn't be collected successfully.");
        }
        return $annotation;
    }

    /**
     * desc: 校验操作权限
     * @param string $annotationValue
     * created by: lhw at 2022/8/18 17:59
     * @return bool
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function checkPermission(string $annotationValue): bool {
        $token = UserAuth::instance()->getToken();
        $cache = di()->get(Redis::class)->get(CacheConstant::USER_AUTH_KEY.$token);
        $cache = json_decode($cache, true);
        if (SysUser::isSuperAdmin($cache["userId"])) {
            return true;
        }
        $hasPerms = $cache["perms"];
        if (in_array($annotationValue, $hasPerms)) {
            return true;
        }
        return false;
    }
}
