<?php


namespace App\Common\Log;
use Hyperf\Logger\LoggerFactory;

class Logger
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    public function __construct(LoggerFactory $loggerFactory)
    {
        $this->logger = $loggerFactory->get('log', 'default');
    }

    public function info(string $message, array $contents) {
        $this->logger->info(sprintf($message, json_encode($contents)));
    }
}
