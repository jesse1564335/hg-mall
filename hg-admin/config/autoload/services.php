<?php

use repository\rpc\service\ActivityRpcService;
use repository\rpc\service\OrderRpcService;
use repository\rpc\service\ProductService;
use repository\rpc\service\FileUploadRpcService;
use repository\rpc\service\UserRpcService;

$registry = [
    'protocol' => 'nacos',
    'address' => 'http://'.env("NACOS_HOST").":".env("NACOS_PORT"),
];

return [
    'enable' => [
        // 开启服务发现
        'discovery' => true,
        // 开启服务注册
        'register' => true,
    ],
    // 服务消费者相关配置
    'consumers'  => value(function () use($registry) {
        $consumers = [];
        $services = [
            'OrderRpcService' => OrderRpcService::class,
            'ProductService' => ProductService::class,
            'UserRpcService' => UserRpcService::class,
            'ActivityRpcService' => ActivityRpcService::class,
            'FileUploadRpcService' => FileUploadRpcService::class
        ];
        foreach ($services as $name => $interface) {
            $consumers[] = [
                'protocol' => 'jsonrpc-http',
                'name' => $name,
                'load_balancer' => 'random',
                'service' => $interface,
                'registry' => $registry
            ];
        }
        return $consumers;
    }),
//    // 服务提供者相关配置
    'providers' => [],
    // 服务驱动相关配置
    'drivers' => [
//        'consul' => [
//            'uri' => 'http://127.0.0.1:8500',
//            'token' => '',
//            'check' => [
//                'deregister_critical_service_after' => '90m',
//                'interval' => '1s',
//            ],
//        ],
        'nacos' => [
            // nacos server url like https://nacos.hyperf.io, Priority is higher than host:port
            // 'url' => '',
            // The nacos host info
            'host' => env("NACOS_HOST", '127.0.0.1'),
            'port' => env("NACOS_PORT", 8848),
            // The nacos account info
            'username' => env("NACOS_USERNAME", "nacos"),
            'password' => env("NACOS_PASSWORD", "nacos"),
            'guzzle' => [
                'config' => null,
            ],
            'group_name' => "DEFAULT_GROUP",
            'namespace_id' => env("NACOS_CONFIG_DATA_TENANT"),
            'heartbeat' => 5,
            'ephemeral' => false, // 是否注册临时实例
        ],
    ],
];
