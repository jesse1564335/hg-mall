<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

use App\Services\Impl\LoginServiceImpl;
use App\Services\Impl\SysConfigServiceImpl;
use App\Services\Impl\SysCrontabServiceImpl;
use App\Services\Impl\SysDictDataServiceImpl;
use App\Services\Impl\SysDictTypeServiceImpl;
use App\Services\Impl\SysMenuServiceImpl;
use App\Services\Impl\SysRoleServiceImpl;
use App\Services\Impl\SysUserServiceImpl;
use App\Services\LoginService;
use App\Services\SysConfigService;
use App\Services\SysCrontabService;
use App\Services\SysDictDataService;
use App\Services\SysDictTypeService;
use App\Services\SysMenuService;
use App\Services\SysRoleService;
use App\Services\SysUserService;

return [
    Hyperf\Contract\StdoutLoggerInterface::class => App\Common\Log\LoggerFactory::class,
    Hyperf\Server\Listener\AfterWorkerStartListener::class => App\Common\Http\WorkerStartListener::class,
    'HttpServer' => Hyperf\HttpServer\Server::class,
    LoginService::class => LoginServiceImpl::class,
    SysMenuService::class => SysMenuServiceImpl::class,
    SysRoleService::class => SysRoleServiceImpl::class,
    SysUserService::class => SysUserServiceImpl::class,
    SysDictTypeService::class => SysDictTypeServiceImpl::class,
    SysDictDataService::class => SysDictDataServiceImpl::class,
    SysConfigService::class => SysConfigServiceImpl::class,
    SysCrontabService::class => SysCrontabServiceImpl::class,
];
