<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

use App\Task\SignStatusTask;
use Hyperf\Crontab\Crontab;
return [
    'enable' => true,
    'crontab' => [
//        (new Crontab())->setName('update-meeting-status')->setRule('*/10 * * * * *')->setCallback([SignStatusTask::class,'exec'])->setMemo('更新活动状态'),
    ],
];
