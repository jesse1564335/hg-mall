#关闭当前进程，重启
port=9501
pid=$(netstat -nlp | grep :$port | awk '{print $7}' | awk -F"/" '{ print $1 }');

if [  -n  "$pid"  ];  then
    kill  -9  $pid;
fi

/usr/local/php/bin/php bin/hyperf.php start > /dev/null 2>1&
