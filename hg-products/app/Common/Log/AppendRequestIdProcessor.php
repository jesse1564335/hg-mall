<?php


namespace App\Common\Log;

use Hyperf\Utils\Context;
use Hyperf\Utils\Coroutine;
use Monolog\Logger;
use \Monolog\Processor\ProcessorInterface;

class AppendRequestIdProcessor implements ProcessorInterface
{

    const REQUEST_ID = 'log.request.id';

    /**
     * @inheritDoc
     */
    public function __invoke(array $record)
    {
        $record['context']['request_id'] = Context::getOrSet(self::REQUEST_ID, uniqid());
        $record['context']['coroutine_id'] = Coroutine::id();
        return $record;
    }
}
