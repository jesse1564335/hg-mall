<?php

namespace App\Common;


use Hyperf\Config\Annotation\Value;
use Hyperf\Utils\Traits\StaticInstance;
use repository\tools\StringUtil;

class Helper
{
    use StaticInstance;

    /**
     * @Value("nacos_config.productCodePrefix")
     * @var string
     */
    private string $productCodePrefix;


    public  function generatorCode(): string
    {
        return $this->productCodePrefix . StringUtil::getMillisecond() . StringUtil::getRandomNumbers(4);
    }
}
