<?php


namespace App\Controller;

use App\Domain\Response\ProductRsp;
use App\Service\ProductService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\GetMapping;

/**
 * @Controller(prefix="/what")
 * Class TestController
 * created by: lhw at 2022/9/4 20:08
 */
class TestController extends AbstractController
{
    /**
     * @Inject()
     * @var ProductService
     */
    private ProductService $productsService;

    /**
     * @Inject()
     * @var ProductRsp
     */
    private ProductRsp $productRsp;


    /**
     * @GetMapping(path="/say")
     * desc:
     * created by: lhw at 2022/9/4 20:09
     * @return mixed
     */
    public function say() {
        return $this->response->success(
            $this->productsService->list([])
        );
    }
}