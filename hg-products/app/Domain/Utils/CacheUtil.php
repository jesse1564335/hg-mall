<?php

namespace App\Domain\Utils;

use App\Constants\CacheConstant;
use Hyperf\Redis\Redis;

class CacheUtil
{

    public static function getDictCache(string $key) {
        $redis  = di()->get(Redis::class);
        if (empty($key)) {
            return null;
        }
        $key = CacheConstant::SYS_DICT_KEY.$key;
        if ($redis->exists($key)) {
            return json_decode($redis->get($key), true);
        }
        return null;
    }

    public static function setDictCache(string $key, $data): bool {
        $key = CacheConstant::SYS_DICT_KEY.$key;
        return di()->get(Redis::class)->set($key, json_encode($data, true));
    }

    public static function clearDictCache() {
        di()->get(Redis::class)->del(CacheConstant::SYS_DICT_KEY."*");
    }
}