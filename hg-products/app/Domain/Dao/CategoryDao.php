<?php
/**
 * Class CategoryDao
 * @package App\Domain\Dao
 * Desc:
 * created by: lhw at 2022/9/13 14:11
 */

namespace App\Domain\Dao;


use App\Model\Category;

class CategoryDao extends BaseDao
{
    public function getModel(): Category
    {
        return new Category();
    }

    public function getCategoryList(array $conditions) {
        if (!isset($conditions["all"])) {
            $conditions["all"] = true;
        }
        return $this->fetchData($conditions);
    }


    public function filterConditions(Category $model, array $condition = []) {
        if (!empty($condition['categoryName'])) {
            $model = $model->where('category_name', 'like', "%" . $condition['categoryName'] . "%");
        }
        if (!empty($condition['categoryNameEq'])) {
            $model = $model->where('category_name', '=', $condition['categoryNameEq']);
        }
        if (!empty($condition['categoryValue'])) {
            $model = $model->where('category_value', '=', $condition['categoryValue']);
        }
        if (isset($condition['parentId'])) {
            $model = $model->where('parent_id', '=', $condition['parentId']);
        }
        if (isset($condition['status'])) {
            $model = $model->where('status', '=', $condition['status']);
        }
       return $model;
    }
}