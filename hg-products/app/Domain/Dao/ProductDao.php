<?php
/**
 * Class ProductDao
 * @package App\Domain\Dao
 * Desc:
 * created by: lhw at 2022/9/9 10:09
 */

namespace App\Domain\Dao;


use App\Model\Products;
use Hyperf\Database\Model\Builder;

class ProductDao extends BaseDao
{
    public function getModel(): Products {
        return new Products();
    }

    /**
     * desc: 添加产品
     * @param array $params
     * created by: lhw at 2022/9/9 11:01
     * @return bool
     */
    public function addProduct(array $params): bool {
        $model = $this->getModel();
        $model->setProductName($params["productName"]);
        $model->setProductCode($params["productCode"]);
        $model->setCategoryId($params["categoryId"]);
        $model->setCategoryName($params["categoryName"]);
        $model->setPrice($params["price"]);
        $model->setOnSale($params["onSale"] ?? 0);
        $model->setIsActivity($params["isActivity"] ?? 0);
        $model->setTimeOn($params["timeOn"] ?? null);
        $model->setTimeOff($params["timeOff"] ?? null);
        $model->setDescription($params["description"] ?? "");
        $model->setContent($params["content"] ?? "");
        $model->setFrontImage($params["frontImage"] ?? "");
        $model->setImages($params["images"] ?? "");
        $model->setCreateBy($params["currentUser"] ?? "");
        return $model->save();
    }

    /**
     * desc: 更新产品
     * @param array $params
     * created by: lhw at 2022/9/9 13:43
     * @return bool
     */
    public function updateProduct(array $params): bool {
        $model = $this->getModel()->find($params["id"]);
        $model->setProductName($params["productName"]);
        $model->setCategoryId($params["categoryId"]);
        $model->setCategoryName($params["categoryName"]);
        $model->setPrice($params["price"]);
        $model->setOnSale($params["onSale"] ?? 0);
        $model->setIsActivity($params["isActivity"] ?? 0);
        $model->setTimeOff($params["timeOff"] ?? null);
        $model->setDescription($params["description"] ?? "");
        $model->setContent($params["content"] ?? "");
        $model->setFrontImage($params["frontImage"] ?? "");
        $model->setImages($params["images"] ?? "");
        $model->setUpdateBy($params["currentUser"] ?? "");
        $model->setUpdateTime(date("Y-m-d H:i:s"));
        return $model->save();
    }

    /**
     * desc:
     * @param Products $model
     * @param array $condition
     * created by: lhw at 2022/9/18 21:42
     * @return Products|Builder
     */
    public function filterConditions(Products $model, array $condition = []) {
        if (!empty($condition["productName"])) {
            $model = $model->where("product_name", "like", "%".$condition["productName"]."%");
        }
        if (!empty($condition["productCode"])) {
            $model = $model->where("product_code", "=", $condition["productCode"]);
        }
        if (isset($condition["onSale"])) {
            $model = $model->where("on_sale", "=", $condition["onSale"]);
        }
        if (!empty($condition["beginTime"])) {
            $model = $model->where("create_time", ">=", $condition["beginTime"]);
        }
        if (!empty($condition["endTime"])) {
            $model = $model->where("create_time", "<=", $condition["endTime"]);
        }
        return $model;
    }
}