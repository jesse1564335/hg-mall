<?php
/**
 * Class ProductRsp
 * @package App\Domain\Response
 * Desc:
 * created by: lhw at 2022/9/9 13:18
 */

namespace App\Domain\Response;


use App\Model\Products;

class ProductRsp extends BaseRsp
{
    public function stdoutFormatter(Products $model): array
    {
        return [
            "productId" => $model->getId(),
            'productName' => $model->getProductName(),
            'productCode' => $model->getProductCode(),
            'price' => $model->getPrice(),
            'onSale' => "".$model->getOnSale(),
            'isActivity' => "".$model->getIsActivity(),
            'createTime' => $model->getCreateTime()
        ];
    }


    public function formatList($models): array
    {
        $result = [];
        foreach ($models as $model) {
            $result[] = $this->stdoutFormatter($model);
        }
        return $result;
    }
}