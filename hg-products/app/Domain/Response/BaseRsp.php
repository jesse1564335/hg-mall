<?php

namespace App\Domain\Response;

use Hyperf\Paginator\LengthAwarePaginator;

class BaseRsp
{
    public function buildPage(LengthAwarePaginator $paginator): array {
        $formatter = [
            "total" => $paginator->total(),
            "currentPage" => $paginator->currentPage(),
            "lastPage" => $paginator->lastPage(),
            "rows" => []
        ];
        $items = $paginator->items();
        $formatter["rows"] = $this->formatList($items);
        return $formatter;
    }
}