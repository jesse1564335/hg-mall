<?php
/**
 * Class CategoryRsp
 * @package App\Domain\Response
 * Desc:
 * created by: lhw at 2022/9/13 14:40
 */

namespace App\Domain\Response;


use App\Model\Category;

class CategoryRsp
{
    public function stdoutFormatter(Category $model): array
    {
        return [
            "categoryId" => $model->getId(),
            'categoryValue' => $model->getCategoryValue(),
            'categoryName' =>$model->getCategoryName(),
            'parentId' => $model->getParentId(),
            'status' => "".$model->getStatus(),
            'sort' => $model->getSort(),
            'remark' => $model->getRemark(),
            'iconUrl' => $model->getIconUrl(),
            'children' => [],
            'createTime' => $model->getCreateTime()
        ];
    }


    public function formatList($models): array
    {
        $result = [];
        foreach ($models as $model) {
            $result[] = $this->stdoutFormatter($model);
        }
        return $result;
    }
}