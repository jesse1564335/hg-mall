<?php

namespace App\Constants;

use Hyperf\Config\Annotation\Value;
use Hyperf\Constants\AbstractConstants;


class CommonCode extends AbstractConstants
{
    public const MODEL_SORT_ASC = 1; //升序

    public const MODEL_SORT_DESC = -1; //降序
}