<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class ErrorCode extends AbstractConstants
{
    /**
     * @Message("success")
     */
    public const SUCCESS = 200;

    /**
     * @Message("Server Error！")
     */
    public const SERVER_ERROR = 500;

    /**
     * @Message("参数校验错误")
     */
    public const VALIDATOR_ERROR = 4001;

    /**
     * @Message("参数错误")
     */
    public const PARAMS_ERROR = 4002;

    /**
     * @Message("缺少token")
     */
    public const INVALID_TOKEN= 1000;

    /**
     * @Message("token未正常初始化")
     */
    public const INVALID_TOKEN_INIT = 1001;

    /**
     * @Message("token过期")
     */
    public const TOKEN_INVALID = 1002;

    /**
     * @Message("越权操作")
     */
    public const AUTH_INVALID = 1003;

    /**
     * @Message("验证码无效")
     */
    public const MSG_CODE_ERROR = 1004;

    /**
     * @Message("验证码过期")
     */
    public const MSG_CODE_EXPIRED = 1005;

    /**
     * @Message("用户不存在")
     */
    public const USER_NOT_EXIST = 1101;

    /**
     * @Message("用户已存在")
     */
    public const USER_IS_EXIST = 1102;

    /**
     * @Message("密码错误")
     */
    public const USER_PWD_ERROR = 1103;

    /**
     * @Message("模板不存在")
     */
    public const MEETING_TPL_NOT_EXIST = 1201;

    /**
     * @Message("活动不存在")
     */
    public const MEETING_NOT_EXIST = 1301;

    /**
     * @Message("活动尚未结束")
     */
    public const MEETING_NOT_END = 1302;

    /**
     * @Message("活动已经结束")
     */
    public const MEETING_HAD_END = 1303;

    /**
     * @Message("活动已经开始")
     */
    public const MEETING_IS_BEGIN = 1304;

    /**
     * @Message("不在时间范围内")
     */
    public const SIGN_TIME_INVALID = 1401;

    /**
     * @Message("方法不存在")
     */
    public const CALL_METHOD_NOT_EXIST = 7001;

}
