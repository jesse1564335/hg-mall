<?php
namespace App\Service;


interface ProductService
{
    /**
     * desc: 新增产品
     * created by: lhw at 2022/9/4 17:58
     * @param array $params
     * @return bool
     */
    public function addProduct(array $params): bool;

    /**
     * desc: 更新产品
     * @param array $params
     * created by: lhw at 2022/9/9 13:49
     * @return bool
     */
    public function updateProduct(array $params): bool;

    /**
     * desc: 产品列表:分页
     * @param array $params
     * created by: lhw at 2022/9/9 11:05
     * @return mixed
     */
    public function list(array $params);


    /**
     * desc: 是否上下架
     * @param int $productId
     * @param int $onSale
     * created by: lhw at 10/9/2022 10:35 下午
     * @return bool
     */
    public function setOnSale(int $productId, int $onSale): bool;

    /**
     * desc:产品分类
     * @param array $params
     * created by: lhw at 2022/9/13 17:10
     * @return mixed
     */
    public function categoryList(array $params);

    /**
     * desc: 删除产品分类
     * @param int $categoryId
     * created by: lhw at 2022/9/13 22:46
     * @return bool
     */
    public function deleteCategory(int $categoryId): bool;

    /**
     * desc: 新增分类
     * @param array $params
     * created by: lhw at 2022/9/14 11:23
     * @return array
     */
    public function addCategory(array $params): array;

    /**
     * desc: 更新分类
     * @param array $params
     * created by: lhw at 2022/9/14 13:58
     * @return array
     */
    public function updateCategory(array $params): array;

    /**
     * desc: 查询分类详情
     * @param int $categoryId
     * created by: lhw at 2022/9/14 14:14
     * @return array
     */
    public function getCategoryDetail(int $categoryId): array;

    /**
     * desc: 构建分类树结构
     * created by: lhw at 2022/9/16 14:43
     * @return mixed
     */
    public function buildCategoryTree();

}
