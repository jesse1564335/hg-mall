<?php
/**
 * Class CategoryServiceImpl
 * @package App\Service\Impl
 * Desc:
 * created by: lhw at 2022/9/13 14:08
 */

namespace App\Service\Impl;


use App\Common\Http\Response;
use App\Constants\ErrorCode;
use App\Domain\Dao\CategoryDao;
use App\Domain\Response\CategoryRsp;
use App\Model\Category;
use App\Service\CategoryService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Psr\Http\Message\ResponseInterface;

class CategoryServiceImpl implements CategoryService
{
    /**
     * @Inject()
     * @var CategoryDao
     */
    private CategoryDao $categoryDao;

    /**
     * @Inject()
     * @var CategoryRsp
     */
    private CategoryRsp $categoryRsp;


    /**
     * @Inject
     * @var Response
     */
    protected Response $response;


    public function selectCategoryList(array $params) {
        $list = $this->categoryDao->getCategoryList($params);
        return $this->categoryRsp->formatList($list);
    }

    public function deleteCategory(int $categoryId): bool {
       $model = $this->categoryDao->getModel()->find($categoryId);
       return $model->forceDelete();
    }

    public function addCategory(array $params): array {
        $result = $this->checkCategoryNameAndValueUnique($params);
        if (is_array($result)) {
            return $result;
        }
        $model = $this->categoryDao->getModel();
        $model->setCategoryName($params["categoryName"]);
        $model->setCategoryValue($params["categoryValue"]);
        $model->setParentId($params["parentId"] ?? 0);
        $model->setStatus($params["status"] ?? 0);
        $model->setRemark($params["remark"] ?? "");
        $model->setIconUrl($params["iconUrl"] ?? "");
        $model->setCreateBy($params["currentUser"] ?? "");
        $model->setCreateTime(date("Y-m-d H:i:s"));
        $save = $model->save();
        if ($save) {
            return ["code"=>ErrorCode::SUCCESS, "message"=>"添加成功"];
        }
        return ["code"=>ErrorCode::SERVER_ERROR, "message"=>"添加失败"];
    }

    public function updateCategory(array $params): array {
        $result = $this->checkCategoryNameAndValueUnique($params);
        if (is_array($result)) {
            return $result;
        }
        $model = $this->categoryDao->getModel()->find($params["categoryId"]);
        $model->setCategoryName($params["categoryName"]);
        $model->setCategoryValue($params["categoryValue"]);
        $model->setParentId($params["parentId"] ?? 0);
        $model->setStatus($params["status"] ?? 0);
        $model->setRemark($params["remark"] ?? "");
        $model->setIconUrl($params["iconUrl"] ?? "");
        $model->setUpdateBy($params["currentUser"] ?? "");
        $model->setUpdateTime(date("Y-m-d H:i:s"));
        $save = $model->save();
        if ($save) {
            return ["code"=>ErrorCode::SUCCESS, "message"=>"更新成功"];
        }
        return ["code"=>ErrorCode::SERVER_ERROR, "message"=>"更新失败"];
    }

    public function getCategoryInfo(int $categoryId): array {
        $model = $this->categoryDao->getModel()->find($categoryId);
        return $this->categoryRsp->stdoutFormatter($model);
    }

    public function checkCategoryNameAndValueUnique(array $condition) {
        $checkName = $this->categoryDao->fetchData([
            "categoryNameEq" => $condition["categoryName"],
            "parentId" => $condition["parentId"],
            "first" => true
        ]);
        if (!empty($checkName)) {
            if (isset($condition["categoryId"]) && ($checkName["id"] != $condition["categoryId"])) {
                return ["code"=>ErrorCode::SERVER_ERROR, "message"=>"分类名称不唯一"];
            } elseif (!isset($condition["categoryId"])) {
                return ["code"=>ErrorCode::SERVER_ERROR, "message"=>"分类名称不唯一"];
            }
        }

        $checkValue = $this->categoryDao->fetchData([
            "categoryValue" => $condition["categoryValue"],
            "first" => true
        ]);
        if (!empty($checkValue)) {
            if (isset($condition["categoryId"]) && ($checkValue["id"] != $condition["categoryId"])) {
                return ["code"=>ErrorCode::SERVER_ERROR, "message"=>"分类键值不唯一"];
            }
        }
        return true;
    }

    public function buildCategoryTree(): array {
        $list = $this->categoryDao->getCategoryList([]);
        $returnList = [];
        $tempList = [];
        foreach ($list as  $category) {
            array_push($tempList, $category->getId());
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!in_array($category->getParentId(), $tempList)) {
                $this->recursionFn($list, $category);
                array_push($returnList, $category);
            }
        }
        if (empty($returnList)) {
            $returnList = $list;
        }
        return $returnList;
    }

    /**
     * desc: 递归处理
     * @param $list
     * @param Category $t
     * created by: lhw at 2022/9/16 14:36
     */
    private function recursionFn($list, Category $t) {
        // 得到子节点列表
        $childList = $this->getChildList($list, $t);
        $t->setChildren($childList);
        foreach ($childList as $obj) {
            if ($this->hasChild($list, $obj)) {
                $this->recursionFn($list, $obj);
            }
        }
    }

    /**
     * desc: 子分类列表
     * @param $list
     * @param Category $t
     * created by: lhw at 2022/9/16 14:37
     * @return array
     */
    private function getChildList($list, Category $t): array {
        $childrenList = [];
        foreach ($list as $category) {
            if ($category->getParentId() == $t->getId()) {
                array_push($childrenList, $category);
            }
        }
        return $childrenList;
    }

    /**
     * desc: 是否子分类
     * @param $list
     * @param Category $t
     * created by: lhw at 2022/9/16 14:38
     * @return bool
     */
    private function hasChild($list, Category $t): bool {
        return count($this->getChildList($list, $t)) > 0;
    }
}