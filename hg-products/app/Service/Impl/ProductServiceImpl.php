<?php


namespace App\Service\Impl;


use App\Common\Helper;
use App\Constants\CommonCode;
use App\Domain\Dao\ProductDao;
use App\Domain\Response\ProductRsp;
use App\Service\CategoryService;
use App\Service\ProductService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\RpcServer\Annotation\RpcService;

/**
 * @RpcService(name="ProductService", protocol="jsonrpc-http", server="jsonrpc-http", publishTo="nacos")
 * Class ProductServiceImpl
 * created by: lhw at 2022/9/4 18:03
 */
class ProductServiceImpl implements ProductService
{

    /**
     * @Inject()
     * @var ProductDao
     */
    private ProductDao $productDao;

    /**
     * @Inject()
     * @var ProductRsp
     */
    private ProductRsp $productRsp;

    /**
     * @Inject()
     * @var CategoryService
     */
    private CategoryService $categoryService;


    public function addProduct(array $params): bool {
        $params["productCode"] = Helper::instance()->generatorCode();
        return $this->productDao->addProduct($params);
    }

    public function updateProduct(array $params): bool {
        return $this->productDao->updateProduct($params);
    }

    public function list(array $params) {
        if (!isset($params['sortColumns'])) {
            $params['sortColumns'] = [
                [
                    'sortColumn' => 'create_time',
                    'sort' => CommonCode::MODEL_SORT_DESC
                ]
            ];
        }
        $result = $this->productDao->fetchData($params);
        return $this->productRsp->buildPage($result);
    }

    public function setOnSale(int $productId, int $onSale): bool {
        $model = $this->productDao->getModel()->find($productId);
        $model->setOnSale($onSale);
        return $model->save();
    }

    public function categoryList(array $params) {
       return $this->categoryService->selectCategoryList($params);
    }

    public function deleteCategory(int $categoryId): bool {
        return $this->categoryService->deleteCategory($categoryId);
    }

    public function addCategory(array $params): array {
        return $this->categoryService->addCategory($params);
    }

    public function updateCategory(array $params): array {
        return $this->categoryService->updateCategory($params);
    }

    public function getCategoryDetail(int $categoryId): array {
        return $this->categoryService->getCategoryInfo($categoryId);
    }

    public function buildCategoryTree() {
        return $this->categoryService->buildCategoryTree();
    }

}
