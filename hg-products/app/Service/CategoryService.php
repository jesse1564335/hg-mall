<?php
/**
 * Class CategoryService
 * @package App\Service
 * Desc:
 * created by: lhw at 2022/9/13 14:07
 */

namespace App\Service;


interface CategoryService
{
    /**
     * desc: 分类列表
     * @param array $params
     * created by: lhw at 2022/9/13 14:10
     * @return mixed
     */
    public function selectCategoryList(array $params);

    /**
     * desc: 删除分类
     * @param int $categoryId
     * created by: lhw at 2022/9/13 22:40
     * @return bool
     */
    public function deleteCategory(int $categoryId): bool;

    /**
     * desc: 新增分类
     * @param array $params
     * created by: lhw at 2022/9/14 10:47
     * @return array
     */
    public function addCategory(array $params): array;

    /**
     * desc: 更新分类
     * @param array $params
     * created by: lhw at 2022/9/14 10:47
     * @return array
     */
    public function updateCategory(array $params): array;

    /**
     * desc: 查询分类信息
     * @param int $categoryId
     * created by: lhw at 2022/9/14 14:12
     * @return array
     */
    public function getCategoryInfo(int $categoryId): array;

    /**
     * desc: 检查分类是否在同级中唯一
     * created by: lhw at 2022/9/14 14:53
     * @param array $condition
     * @return mixed
     */
    public function checkCategoryNameAndValueUnique(array $condition);

    /**
     * desc: 构建分类树结构
     * created by: lhw at 2022/9/16 13:52
     * @return mixed
     */
    public function buildCategoryTree();
}