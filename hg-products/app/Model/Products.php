<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property integer $id 主键id
 * @property string $product_name 产品名称
 * @property string $product_code 产品编码
 * @property integer $price 产品价格(单位：分)==产品的原始价
 * @property integer $on_sale 产品是否在售 0-下架 1-在售 2-待上架
 * @property integer $is_activity 是否活动产品：0-否 1-是
 * @property ?string $time_on 定时上架的时间
 * @property ?string $time_off 定时下架的时间
 * @property string $description 产品简介
 * @property string $content 产品详情
 * @property string $front_image 封面图
 * @property string images 详情图-轮播图
 * @property string $create_by 创建人
 * @property string $create_time 创建时间
 * @property ?string $update_by 更新人
 * @property ?string $update_time 更新时间
 */
class Products extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getProductName(): string
    {
        return $this->product_name;
    }

    /**
     * @param string $product_name
     */
    public function setProductName(string $product_name): void
    {
        $this->product_name = $product_name;
    }

    /**
     * @return string
     */
    public function getProductCode(): string
    {
        return $this->product_code;
    }

    /**
     * @param string $product_code
     */
    public function setProductCode(string $product_code): void
    {
        $this->product_code = $product_code;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getOnSale(): int
    {
        return $this->on_sale;
    }

    /**
     * @param int $on_sale
     */
    public function setOnSale(int $on_sale): void
    {
        $this->on_sale = $on_sale;
    }

    /**
     * @return int
     */
    public function getIsActivity(): int
    {
        return $this->is_activity;
    }

    /**
     * @param int $is_activity
     */
    public function setIsActivity(int $is_activity): void
    {
        $this->is_activity = $is_activity;
    }

    /**
     * @return string|null
     */
    public function getTimeOn(): ?string
    {
        return $this->time_on;
    }

    /**
     * @param string|null $time_on
     */
    public function setTimeOn(?string $time_on): void
    {
        $this->time_on = $time_on;
    }

    /**
     * @return string|null
     */
    public function getTimeOff(): ?string
    {
        return $this->time_off;
    }

    /**
     * @param string|null $time_off
     */
    public function setTimeOff(?string $time_off): void
    {
        $this->time_off = $time_off;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getFrontImage(): string
    {
        return $this->front_image;
    }

    /**
     * @param string $front_image
     */
    public function setFrontImage(string $front_image): void
    {
        $this->front_image = $front_image;
    }

    /**
     * @return string
     */
    public function getImages(): string
    {
        return $this->images;
    }

    /**
     * @param string $images
     */
    public function setImages(string $images): void
    {
        $this->images = $images;
    }

    /**
     * @return string
     */
    public function getCreateBy(): string
    {
        return $this->create_by;
    }

    /**
     * @param string $create_by
     */
    public function setCreateBy(string $create_by): void
    {
        $this->create_by = $create_by;
    }

    /**
     * @return string
     */
    public function getCreateTime(): string
    {
        return $this->create_time;
    }

    /**
     * @param string $create_time
     */
    public function setCreateTime(string $create_time): void
    {
        $this->create_time = $create_time;
    }

    /**
     * @return string|null
     */
    public function getUpdateBy(): ?string
    {
        return $this->update_by;
    }

    /**
     * @param string|null $update_by
     */
    public function setUpdateBy(?string $update_by): void
    {
        $this->update_by = $update_by;
    }

    /**
     * @return string|null
     */
    public function getUpdateTime(): ?string
    {
        return $this->update_time;
    }

    /**
     * @param string|null $update_time
     */
    public function setUpdateTime(?string $update_time): void
    {
        $this->update_time = $update_time;
    }
}