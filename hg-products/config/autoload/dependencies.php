<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

use App\Service\CategoryService;
use App\Service\Impl\CategoryServiceImpl;
use App\Service\Impl\ProductServiceImpl;
use App\Service\ProductService;

return [
    Hyperf\Contract\StdoutLoggerInterface::class => App\Common\Log\LoggerFactory::class,
    Hyperf\Server\Listener\AfterWorkerStartListener::class => App\Common\Http\WorkerStartListener::class,
    ProductService::class => ProductServiceImpl::class,
    CategoryService::class => CategoryServiceImpl::class
];
