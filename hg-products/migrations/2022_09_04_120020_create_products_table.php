<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('category_id')->unsigned()->nullable(false)->comment("所属分类Id");
            $table->char('category_name', 100)->nullable(false)->comment("所属分类名称");
            $table->string('product_name', 100)->nullable(false)->comment("产品名称");
            $table->string('product_code', 100)->nullable(false)->comment("产品code");
            $table->integer('price')->unsigned()->nullable(false)->comment("产品价格(单位：分)==产品的原始价");
            $table->tinyInteger('on_sale')->unsigned()->default(0)->comment("是否在售： 0-下架 1-在售 2-待上架");
            $table->tinyInteger('is_activity')->unsigned()->default(0)->comment("是否活动产品：0-否 1-是");
            $table->timestamp('time_on')->nullable()->default(null)->comment("定时上架时间");
            $table->timestamp('time_off')->nullable()->default(null)->comment("定时下架时间");
            $table->string('description')->comment("产品简介");
            $table->longText('content')->comment("产品详情");
            $table->string('front_image')->nullable(false)->comment("产品封面图");
            $table->string('images')->nullable(false)->comment("产品详细图");
            $table->char('create_by', 100)->comment("创建人");
            $table->timestamp('create_time')->useCurrent()->comment("创建时间");
            $table->char('update_by', 100)->nullable()->comment("更新人");
            $table->timestamp('update_time')->nullable()->comment("更新时间");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
}
